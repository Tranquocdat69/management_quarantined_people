package com.project_iv.group_3.room.controller;

import java.util.List;

import com.project_iv.group_3.area.entity.Area;
import com.project_iv.group_3.area.service.AreaService;
import com.project_iv.group_3.room.dto.TotalPeopleInRoom;
import com.project_iv.group_3.room.entity.Room;
import com.project_iv.group_3.room.service.RoomService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RoomController {
    @Autowired
    private RoomService RoomService;
    
    @Autowired
    private AreaService areaService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @GetMapping("/rooms")
    public String list(){
        return "dashboard/room/index";
    }

    @GetMapping("/rooms/getAllRoom")
    @ResponseBody
    public List<TotalPeopleInRoom> getAllRoom(){
        List<TotalPeopleInRoom> listAll = RoomService.getAllRoomsByAreaIdInUserId();
        
        return listAll;
    }
    // @GetMapping("/getAllRoom")
    // @ResponseBody
    // public List<Room> getAllRoom(Model model){
    //     List<Room> allRoom = RoomRepo.findAll();
    //     model.addAttribute("allRoom", allRoom);
    //     return allRoom ;
    // }
    
    @GetMapping("/rooms/create-room")
    public String showNewRoom(Model model){
        List<Area> listArea = areaService.getAreaByUserId();
        model.addAttribute("listArea", listArea);
        model.addAttribute("room", new Room());
        if (listArea.size() != 0) {
            return "dashboard/room/create";
        }
        return "redirect:/areas/create-area";
    } 

    @PostMapping("/rooms/add-room")
    public String addRoom(@ModelAttribute("room") Room room){
        RoomService.addNewRoom(room);
        this.simpMessagingTemplate.convertAndSend("/topic/getRoom","created new room");

       return "redirect:/rooms";
    }

    @GetMapping("/rooms/edit/{room_id}")
    public String showEditForm(@PathVariable(name="room_id") Integer room_id,Model model){
        Room room =RoomService.getById(room_id);
        if (room != null) {
            Boolean checkRoomIdExsistInAreaUserId = RoomService.checkRoomIdExsistInAreaUserId(room_id);
            if (checkRoomIdExsistInAreaUserId) {
                List<Area> listArea = areaService.getAreaByUserId();
                model.addAttribute("room", room);
                model.addAttribute("listArea", listArea);
                return "dashboard/room/edit";
            }
        }
       return "redirect:/error/404.html";
    }
    
    @PostMapping("/rooms/edit-room")
    public String saveRoom(Integer room_id,@ModelAttribute("room") Room room){
        Room roomById =RoomService.getById(room_id);
        if (roomById != null) {
            RoomService.save(room);
            this.simpMessagingTemplate.convertAndSend("/topic/getRoom","edited room");
            return "redirect:/rooms";
        }
       return "redirect:/error/404.html";
    }
    
    @GetMapping("/rooms/SearchRoom")
    @ResponseBody
    public List<TotalPeopleInRoom> SearchRoom(@RequestParam String keyword){
        List<TotalPeopleInRoom> SearchRoom = RoomService.SearchRoom(keyword);
        return SearchRoom;
    }

    @GetMapping("/rooms/delete")
    @ResponseBody
    public Boolean delete(@RequestParam Integer room_id){
        Boolean isDelete = RoomService.delete(room_id);
        if (isDelete) {
            this.simpMessagingTemplate.convertAndSend("/topic/getRoom","deleted room");
        }
        return isDelete;
    }

    @GetMapping("/rooms/SearchExistRoom")
    @ResponseBody
    public Boolean searchExistRoom(@RequestParam String newRoomName, Integer areaId){
        return RoomService.SearchExistRoom(newRoomName, areaId);
    }

    @GetMapping("/rooms/SearchEditExistRoom")
    @ResponseBody
    public Boolean searchEditExistRoom(@RequestParam String currentEditRoomName,String newEditRoomName, Integer areaId){
       return RoomService.SearchEditExistRoom(currentEditRoomName, newEditRoomName, areaId);
    }
   
    @GetMapping("/rooms/getRoomsByAreaId")
    @ResponseBody
    public List<Room> getRoomsByAreaId(@RequestParam Integer areaId){
        return RoomService.getRoomsByAreaId(areaId);
    }

    @GetMapping("/rooms/searchEditExistRoomDiffArea")
    @ResponseBody
    public Boolean searchEditExistRoomDiffArea(@RequestParam Integer areaId ,String newRoomName){
        return RoomService.SearchEditExistRoomDiffArea(areaId, newRoomName);
    }

    @GetMapping("/rooms/checkLimitBedNumberInRoom")
    @ResponseBody
    public TotalPeopleInRoom checkLimitBedNumberInRoom(@RequestParam Integer roomId){
        return RoomService.checkLimitBedNumberInRoom(roomId);
    }
   
    @GetMapping("/rooms/checkLimitBedNumberInRoomWhenEdit")
    @ResponseBody
    public TotalPeopleInRoom checkLimitBedNumberInRoomWhenEdit(@RequestParam Integer newRoomId,@RequestParam Integer currentRoomId){
        return RoomService.checkLimitBedNumberInRoomWhenEdit(newRoomId, currentRoomId);
    }
}
