package com.project_iv.group_3.room.repository;

import java.util.List;

import javax.transaction.Transactional;

import com.project_iv.group_3.room.dto.TotalPeopleInRoom;
import com.project_iv.group_3.room.entity.Room;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface RoomRepository extends JpaRepository<Room,Integer>{

        @Query(nativeQuery = true,value = "SELECT DISTINCT r.*,a.area_name FROM room r inner join area a on r.area_id = a.area_id where a.area_id = ?1")
        public List<Room> getRoomsByAreaId(Integer areaId);
        
        @Query(nativeQuery = true,value = "SELECT r.bed_number as bedNumber, count(rqi.quarantined_information_id) as totalPeopleInRoom FROM room r inner join room_quarantined_information rqi on rqi.room_id = r.room_id where r.room_id = ?1 and rqi.status = 0")
        public TotalPeopleInRoom checkLimitBedNumberInRoom(Integer roomId);
  
        @Query(nativeQuery = true,value = "SELECT r.bed_number as bedNumber, count(rqi.quarantined_information_id) as totalPeopleInRoom FROM room r inner join room_quarantined_information rqi on rqi.room_id = r.room_id where r.room_id = ?1 and r.room_id != ?2 and rqi.status = 0")
        public TotalPeopleInRoom checkLimitBedNumberInRoomWhenEdit(Integer newRoomId, Integer currentRoomId);

        @Query(nativeQuery = true, value = "select * from room order by room_id desc")
        public List<Room> getAllRoomsIdDesc();

        @Query(nativeQuery = true, value = "select r.room_id as roomId,r.room_name as roomName, r.bed_number as bedNumber,r.note,r.area_id as areaId, a.area_name as areaName,IFNULL(r2.totalPeopleInRoom, 0) AS totalPeopleInRoom from room r inner join area a on a.area_id = r.area_id left outer join (select rqi.room_id,count(rqi.quarantined_information_id) as totalPeopleInRoom from room_quarantined_information rqi where rqi.status = 0 group by rqi.room_id) r2 on r.room_id = r2.room_id where r.area_id  in (select a.area_id from management_assignment m inner join area a on a.area_id = m.area_id where m.user_id = ?1) and concat(r.room_name,a.area_name) like %?2% order by r.room_id desc")
        public List<TotalPeopleInRoom> SearchRoom(Integer userId,String keyword);
        
        @Query(nativeQuery = true, value = "select exists (select r.* from room r inner join area a on r.area_id = a.area_id where r.room_name = ?1  and a.area_id= ?2 )")
        public Integer SearchExistRoom(String newRoomName, Integer areaId);
        
        @Query(nativeQuery = true, value = "select exists (select r.* from room r inner join area a on r.area_id = a.area_id where r.room_name != ?1 and a.area_id=?3 and r.room_name = ?2)")
        public Integer SearchEditExistRoom(String currentEditRoomName,String newEditRoomName, Integer areaId);
        
        @Query(nativeQuery = true, value = "select exists (select r.* from room r inner join area a on r.area_id = a.area_id where a.area_id = ?1 and r.room_name = ?2)")
        public Integer SearchEditExistRoomDiffArea(Integer areaId ,String newRoomName);
        
        // @Query(nativeQuery = true, value = "select exists (select r.* from room r inner join area a on r.area_id = a.area_id where a.area_id = ?1 and r.room_name = ?2)")
        // public Integer CountBedNumber(Integer areaId ,String newRoomName);
        
        // @Query(nativeQuery = true, value = "select * from room order by room_id desc")
        // public List<Room> getAllRoomsIdDesc();
        
        @Query(nativeQuery = true, value = "select exists (select * from room_quarantined_information where room_id = ?1)")
        public Integer checkRoomExistInRoomQuarantinedInformation(Integer room_id);
        
        // @Query(nativeQuery = true, value = "select exists (select * from quarantined_information where id = ?1)")
        // public Integer checkRQIExistInQI(Integer id);
        
        @Modifying
        @Transactional
        @Query(nativeQuery = true, value = "delete from room_quarantined_information where room_id = ?1")
        public void deleteRoomQuarantinedByRoomId(Integer room_id);
        
        @Query(nativeQuery = true, value = "select r.room_id as roomId,r.room_name as roomName, r.bed_number as bedNumber,r.note,r.area_id as areaId, a.area_name as areaName,IFNULL(r2.totalPeopleInRoom, 0) AS totalPeopleInRoom from room r inner join area a on a.area_id = r.area_id left outer join (select rqi.room_id,count(rqi.quarantined_information_id) as totalPeopleInRoom from room_quarantined_information rqi where rqi.status = 0 group by rqi.room_id) r2 on r.room_id = r2.room_id where r.area_id  in (select a.area_id from management_assignment m inner join area a on a.area_id = m.area_id where m.user_id = ?1) order by r.room_id desc")
        public List<TotalPeopleInRoom> getAllRoomsByAreaIdInUserId(Integer userId);

        @Query(nativeQuery = true, value = "select exists(select r.room_id from room r where r.room_id = ?1 and r.area_id in (select a.area_id from management_assignment m inner join area a on a.area_id = m.area_id where m.user_id = ?2))")
        public Integer checkRoomIdExsistInAreaUserId(Integer roomId, Integer userId);

        
        @Query(nativeQuery = true, value = "select room.* from room_quarantined_information inner join room on room_quarantined_information.room_id=room.room_id where quarantined_information_id = ?1 order by id desc limit 1")
        public Room getRoomQuarantinedInformationLast(Integer quarantinedInformationId);
  
    }
    
