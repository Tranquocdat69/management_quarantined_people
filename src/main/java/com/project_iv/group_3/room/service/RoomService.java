package com.project_iv.group_3.room.service;

import java.util.ArrayList;
import java.util.List;

import com.project_iv.group_3.room.dto.TotalPeopleInRoom;
import com.project_iv.group_3.room.entity.Room;
import com.project_iv.group_3.room.repository.RoomRepository;
import com.project_iv.group_3.security.user_detail.CustomUserDetail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class RoomService {
    @Autowired
    private RoomRepository RoomRepository;

    public List<Room> getRoomsByAreaId(Integer areaId){
        return RoomRepository.getRoomsByAreaId(areaId);
    }
    
    public TotalPeopleInRoom checkLimitBedNumberInRoom(Integer roomId){
        return RoomRepository.checkLimitBedNumberInRoom(roomId);
    }
    public TotalPeopleInRoom checkLimitBedNumberInRoomWhenEdit(Integer newRoomId, Integer currentRoomId){
        return RoomRepository.checkLimitBedNumberInRoomWhenEdit(newRoomId, currentRoomId);
    };


    public List<Room> listAll(){
        return RoomRepository.getAllRoomsIdDesc();
    }

    public void addNewRoom(Room room){
        RoomRepository.save(room);
    }

    public void save(Room room_id){
        RoomRepository.save(room_id);
    }

    public Room getById(Integer room_id){
        boolean existsById = RoomRepository.existsById(room_id);
        if (existsById) {
            return RoomRepository.findById(room_id).get();
        }
        return null;
    }
    
    public List<TotalPeopleInRoom> SearchRoom(String keyword){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetail customUser = (CustomUserDetail) authentication.getPrincipal();
            int userId = customUser.getUser().getId();

        if (keyword.trim().isEmpty()) {
            return new ArrayList<TotalPeopleInRoom>();
        }
        return RoomRepository.SearchRoom(userId,keyword);
    }
    public List<TotalPeopleInRoom> getAllRoomsByAreaIdInUserId(){
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetail customUser = (CustomUserDetail) authentication.getPrincipal();
            int userId = customUser.getUser().getId();

            return RoomRepository.getAllRoomsByAreaIdInUserId(userId);
        
    }

    // public void delete(Integer room_id){
    //     RoomRepository.deleteById(room_id);
    // }
    public Boolean delete(Integer room_id){
        Room roomToDelete= RoomRepository.getById(room_id);
        if(roomToDelete!= null){
            Integer checkRoomExistInRoomQuarantinedInformation = RoomRepository.checkRoomExistInRoomQuarantinedInformation(room_id);
            if (checkRoomExistInRoomQuarantinedInformation !=1) {
                RoomRepository.deleteById(room_id);
                return true;
            }
        }
        return false;
    }
    public Boolean SearchExistRoom(String newRoomName, Integer areaId) {
       Integer searchExistRoom = RoomRepository.SearchExistRoom(newRoomName, areaId);
        if (searchExistRoom == 1) {
            return true;
        }
        return false;
}
public Boolean SearchEditExistRoom(String currentEditRoomName,String newEditRoomName, Integer areaId) {
    Integer searchEditExistRoom = RoomRepository.SearchEditExistRoom(currentEditRoomName,newEditRoomName, areaId);
     if (searchEditExistRoom == 1) {
         return true;
     }

     return false;
 
}
public Boolean SearchEditExistRoomDiffArea(Integer areaId ,String newRoomName) {
    Integer searchEditExistRoomDiffArea = RoomRepository.SearchEditExistRoomDiffArea(areaId,newRoomName);
     if (searchEditExistRoomDiffArea == 1) {
         return true;
     }

     return false;
    }

    public Boolean checkRoomIdExsistInAreaUserId(Integer roomId) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetail customUser = (CustomUserDetail) authentication.getPrincipal();
            int userId = customUser.getUser().getId();
        Integer checkRoomIdExsistInAreaUserId = RoomRepository.checkRoomIdExsistInAreaUserId(roomId, userId);
        if (checkRoomIdExsistInAreaUserId == 1) {
            return true;
        }
            return false;
    }

    public Room getRoomQuarantinedInformationLast(Integer quarantinedInformationId){
     return   RoomRepository.getRoomQuarantinedInformationLast(quarantinedInformationId);
    }
}


