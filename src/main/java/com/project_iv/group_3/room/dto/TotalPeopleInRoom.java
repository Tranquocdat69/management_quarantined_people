package com.project_iv.group_3.room.dto;

public interface TotalPeopleInRoom {
    
    public Integer getRoomId();

    public String getRoomName();

    public String getNote();

    public Integer getAreaId();

    public String getAreaName();

    public Integer getTotalPeopleInRoom();

    public Integer getBedNumber();

}
