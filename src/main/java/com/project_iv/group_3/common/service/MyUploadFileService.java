package com.project_iv.group_3.common.service;

import java.io.File;

import org.springframework.stereotype.Service;

@Service
public class MyUploadFileService {
    public File getFolderUpload() {
        File folderUpload = new File(System.getProperty("user.dir")+"/src/main/resources/static/dashboard/plugins/images/uploads");
        if (!folderUpload.exists()) {
          folderUpload.mkdirs();
        }
        return folderUpload;
    }
    
    public void deleteUploadedFile(String fileName) {
        File filePath = new File(System.getProperty("user.dir")+"/src/main/resources/static/dashboard/plugins/images/uploads/"+fileName);
        if (filePath != null) {
          if (!fileName.trim().isEmpty()) {
              filePath.delete();
          }
        }
    }

    public File getFolderUploadFile() {
      File folderUpload = new File(System.getProperty("user.dir")+"/src/main/resources/static/dashboard/plugins/application_form/uploads");
      if (!folderUpload.exists()) {
        folderUpload.mkdirs();
      }
      return folderUpload;
    }
  
    public void deleteUploadedFileDocx(String fileName) {
      File filePath = new File(System.getProperty("user.dir")+"/src/main/resources/static/dashboard/plugins/application_form/uploads/"+fileName);
      if (filePath != null) {
        if (!fileName.trim().isEmpty()) {
            filePath.delete();
        }
      }
    }
}
