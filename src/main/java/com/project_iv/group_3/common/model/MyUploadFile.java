package com.project_iv.group_3.common.model;

import org.springframework.web.multipart.MultipartFile;

public class MyUploadFile {
    private MultipartFile multipartFile;

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
