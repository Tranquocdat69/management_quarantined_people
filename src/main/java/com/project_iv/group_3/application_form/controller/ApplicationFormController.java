package com.project_iv.group_3.application_form.controller;

import java.io.File;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.project_iv.group_3.application_form.entity.ApplicationForm;
import com.project_iv.group_3.application_form.service.ApplicationFormService;
import com.project_iv.group_3.common.model.MyUploadFile;
import com.project_iv.group_3.common.service.MyUploadFileService;
import com.project_iv.group_3.form_type.entity.FormType;
import com.project_iv.group_3.form_type.service.FormTypeService;
import com.project_iv.group_3.quarantined_information.entity.QuarantinedInformation;
import com.project_iv.group_3.quarantined_information.service.QuarantinedInformationService;
import com.project_iv.group_3.quarantined_people.service.QuarantinedPeopleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ApplicationFormController {
    
    @Autowired
    private ApplicationFormService applicationFormService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private FormTypeService formTypeService;

    @Autowired
    private MyUploadFileService uploadFileService;

    @Autowired
    private QuarantinedInformationService quarantinedInformationService;

    @Autowired
    private QuarantinedPeopleService quarantinedPeopleService;

    @GetMapping("/application-form/{id}")
    public String index(@PathVariable Integer id, Model model){
        Boolean checkUserHasAuthorAccessDetail = quarantinedPeopleService.checkUserHasAuthorAccessDetail(id);
        if (checkUserHasAuthorAccessDetail) {
            return "dashboard/application_form/index";
        }
        return "redirect:/error/404.html";
    }

    @GetMapping("/application-form/getAllApplicationForms")
    @ResponseBody
    public List<ApplicationForm> getAllApplicationFormsByquarantinedInformationId(@RequestParam Integer quarantinedInformationId){
        List<ApplicationForm> applicationForms = applicationFormService.getAllApplicationFormsByQuarantinedInformationId(quarantinedInformationId);
        return applicationForms;
    }

    @GetMapping("/application-form/searchApplicationForms")
    @ResponseBody
    public List<ApplicationForm> searchApplicationForms(@RequestParam String keyword){
        List<ApplicationForm> searchApplicationForms = applicationFormService.searchApplicationForms(keyword);
        return searchApplicationForms;
    }

    @GetMapping("/application-form/deleteApplicationForm")
    @ResponseBody
    public Boolean deleteApplicationForm(@RequestParam Integer id, @RequestParam Integer quarantinedInformationId){
        QuarantinedInformation quarantinedInformationyById = quarantinedInformationService.getQuarantinedInformationyById(quarantinedInformationId);
        if (quarantinedInformationyById != null) {
            Boolean checkUserHasAuthorAccessDetail = quarantinedPeopleService.checkUserHasAuthorAccessDetail(quarantinedInformationyById.getQuarantinedPeople().getId());
            if (checkUserHasAuthorAccessDetail) {
                this.simpMessagingTemplate.convertAndSend("/topic/getApplicationForm/" + quarantinedInformationId,"deleted");
                return applicationFormService.deleteApplicationForm(id);
            }
            return false;
        }
        return false;
    }

    @GetMapping("/application-form/create-application-form/{id}")
    public String createPage(@PathVariable Integer id, Model model){
        Boolean checkUserHasAuthorAccessDetail = quarantinedPeopleService.checkUserHasAuthorAccessDetail(id);
        if (checkUserHasAuthorAccessDetail) {
            List<FormType> formTypes = formTypeService.getAllFormTypes();
            if (formTypes.size() > 0) {
                QuarantinedInformation quarantinedInformation = quarantinedInformationService.getQuarantinedInformationNotCompleteByQuarantinedPeopleId(id);
                if (quarantinedInformation.getStatus() != null) {
                    model.addAttribute("applicationForm", new ApplicationForm());
                    model.addAttribute("myUploadFile", new MyUploadFile());
                    model.addAttribute("quarantinedInformation", quarantinedInformation);
                    return "dashboard/application_form/create";
                }else{
                    return "redirect:/error/404.html";
                }
            }
            return "redirect:/form-type/create-form-type";
        }
        return "redirect:/error/403.html";
    }

    @PostMapping("/application-form/addApplicationForm")
    public String addApplicationForm(ApplicationForm applicationForm, Principal principal, MyUploadFile myUploadFile, QuarantinedInformation quarantinedInformation) throws InterruptedException{
        QuarantinedInformation quarantinedInformationByQuarantinedPeople = quarantinedInformationService.getQuarantinedInformationNotCompleteByQuarantinedPeopleId(quarantinedInformation.getQuarantinedPeople().getId());
                if (quarantinedInformationByQuarantinedPeople.getStatus() != null){
                    try {
                        MultipartFile multipartFile = myUploadFile.getMultipartFile();
                        if (multipartFile != null) {
                            String tempFileName = multipartFile.getOriginalFilename();
                            if (tempFileName != "") {
                                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                                String extension = tempFileName.substring(tempFileName.indexOf(".")+1).toLowerCase();
                                String name = tempFileName.substring(0, tempFileName.lastIndexOf("."));
                                String fileName = name + timeStamp + "."+extension;
                                File file = new File(uploadFileService.getFolderUploadFile(), fileName);
            
                                multipartFile.transferTo(file);
                                Thread.sleep(200);
                                applicationForm.setContent(fileName);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            
                    ApplicationForm addNewApplicationForm = applicationFormService.addNewApplicationForm(applicationForm, principal, quarantinedInformation);
            
                    if (addNewApplicationForm != null) {
                        // Thread.sleep(800);
                        this.simpMessagingTemplate.convertAndSend("/topic/getApplicationForm/" + quarantinedInformation.getId(),"added");
                        return "redirect:/quarantined-information/detail-quarantined-information/" + quarantinedInformation.getId();
                    }else{
                        return "dashboard/application_form/create";
                    }
                }
        return "redirect:/error/404.html";
    }

    @GetMapping("/application-form/detail/{id}")
    public String editPage(@PathVariable Integer id, Model model) {
        ApplicationForm applicationForm = applicationFormService.getApplicationFormById(id);
        if (applicationForm != null && applicationForm.getVerificationResult() == 0) {
            Boolean checkUserHasAuthorAccessDetail = quarantinedPeopleService.checkUserHasAuthorAccessDetail(applicationForm.getQuarantinedInformation().getQuarantinedPeople().getId());
            if (checkUserHasAuthorAccessDetail) {
                
                QuarantinedInformation quarantinedInformationNotCompleteByQuarantinedPeopleId = quarantinedInformationService.getQuarantinedInformationNotCompleteByQuarantinedPeopleId(applicationForm.getQuarantinedInformation().getQuarantinedPeople().getId());
                if (quarantinedInformationNotCompleteByQuarantinedPeopleId.getStatus() != null) {
                    model.addAttribute("applicationForm", applicationForm);
                    model.addAttribute("myUploadFile", new MyUploadFile());
                    return "dashboard/application_form/detail";
                }else{
                    return "redirect:/error/404.html";
                }
            }
            return "redirect:/error/403.html";
        }
        return "redirect:/error/404.html";
    }

    @PostMapping("/application-form/editApplicationForm")
    public String doEditPage(Integer applicationFormId, ApplicationForm applicationForm, MyUploadFile myUploadFile, HttpServletRequest request, Principal principal) throws InterruptedException {
        QuarantinedInformation quarantinedInformationNotCompleteByQuarantinedPeopleId = quarantinedInformationService.getQuarantinedInformationNotCompleteByQuarantinedPeopleId(applicationForm.getQuarantinedInformation().getQuarantinedPeople().getId());
        if (quarantinedInformationNotCompleteByQuarantinedPeopleId.getStatus() != null) {
            ApplicationForm findById = applicationFormService.getApplicationFormById(applicationFormId);
            if (findById != null) {
                try {
                    MultipartFile multipartFile = myUploadFile.getMultipartFile();
                    if (multipartFile != null) {
                        String tempFileName = multipartFile.getOriginalFilename();
                        if (tempFileName != "") {
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                            String extension = tempFileName.substring(tempFileName.indexOf(".")+1).toLowerCase();
                            String name = tempFileName.substring(0, tempFileName.lastIndexOf("."));
                            String fileName = name + timeStamp + "."+extension;
                            File file = new File(uploadFileService.getFolderUploadFile(), fileName);
                            String currentFile = request.getParameter("nameOfFile");
        
                            if (currentFile != null && !currentFile.trim().isEmpty()) {
                                uploadFileService.deleteUploadedFileDocx(currentFile);
                            }
        
                            multipartFile.transferTo(file);
                            Thread.sleep(200);
                            applicationForm.setContent(fileName);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
    
                ApplicationForm updateApplicationForm = applicationFormService.updateApplicationForm(applicationFormId, applicationForm.getFormType(), applicationForm.getContent() != null ? applicationForm.getContent() : "");
    
                if (updateApplicationForm != null) {
                    // Thread.sleep(800);
                    this.simpMessagingTemplate.convertAndSend("/topic/getApplicationForm/" + updateApplicationForm.getQuarantinedInformation().getId(),"edited");
                    return "redirect:/quarantined-information/detail-quarantined-information/" + updateApplicationForm.getQuarantinedInformation().getId();
                }else{
                    return "redirect:/application-form/detail/" + applicationForm.getApplicationFormId();
                }
            }
        }
        return "redirect:/error/404.html";
    }

    @GetMapping("/application-form/verifyApplicationForm")
    @ResponseBody
    public Boolean verifyApplicationForm(@RequestParam Integer applicationFormId, @RequestParam Integer verificationResult, Principal principal){
        ApplicationForm applicationForm = applicationFormService.getApplicationFormById(applicationFormId);
        if (applicationForm != null) {
            QuarantinedInformation quarantinedInformationyById = quarantinedInformationService.getQuarantinedInformationyById(applicationForm.getQuarantinedInformation().getId());
            if (quarantinedInformationyById != null) {
                Boolean checkUserHasAuthorAccessDetail = quarantinedPeopleService.checkUserHasAuthorAccessDetail(quarantinedInformationyById.getQuarantinedPeople().getId());
                if (checkUserHasAuthorAccessDetail) {
                    Boolean checkApplicationFormVerified = applicationFormService.checkApplicationFormVerified(applicationFormId);
                    if (checkApplicationFormVerified) {
                        ApplicationForm verifyApplicationForm = applicationFormService.verifyApplicationForm(applicationFormId, verificationResult, principal);
                        if (verifyApplicationForm != null) {
                            this.simpMessagingTemplate.convertAndSend("/topic/getApplicationForm/" + quarantinedInformationyById.getId(),"deleted");
                            return true;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    @GetMapping("/application-form/information/{id}")
    public String informationPage(@PathVariable Integer id, Model model) {
        ApplicationForm applicationFormById = applicationFormService.getApplicationFormById(id);
        if (applicationFormById != null) {
            Boolean checkUserHasAuthorAccessDetail = quarantinedPeopleService.checkUserHasAuthorAccessDetail(applicationFormById.getQuarantinedInformation().getQuarantinedPeople().getId());
            if (checkUserHasAuthorAccessDetail) {
                model.addAttribute("id", id);
                return "dashboard/application_form/information";
            }
            return "redirect:/error/403.html";
        }
        return "redirect:/error/404.html";
    }

    @GetMapping("/application-form/informationApplicationForm")
    @ResponseBody
    public ApplicationForm informationApplicationForm(@RequestParam Integer id){
        return applicationFormService.getApplicationFormById(id);
    }
}
