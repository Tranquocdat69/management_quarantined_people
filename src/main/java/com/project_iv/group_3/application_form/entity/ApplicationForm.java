package com.project_iv.group_3.application_form.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project_iv.group_3.form_type.entity.FormType;
import com.project_iv.group_3.quarantined_information.entity.QuarantinedInformation;
import com.project_iv.group_3.user.entity.User;

@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "application_form")
public class ApplicationForm implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "application_form_id", nullable = false)
    private Integer applicationFormId;

    @Column(name = "verification_result", nullable = false)
    private Integer verificationResult;

    private String content;

    @Column(name = "created_date", nullable = false)
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy",timezone = "GMT+8")
    private Date createdDate;

    @Column(name = "verified_date", nullable = true)
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy",timezone = "GMT+8")
    private Date verifiedDate;

    @ManyToOne
    @JoinColumn(name = "form_type_id")
    private FormType formType;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private User creator;

    @ManyToOne
    @JoinColumn(name = "verifier_id")
    private User verifier;

    @ManyToOne
    @JoinColumn(name = "quarantined_information_id")
    private QuarantinedInformation quarantinedInformation;

    public ApplicationForm() {
    }

    public ApplicationForm(Integer applicationFormId, Integer verificationResult, String content, Date createdDate,
            Date verifiedDate, FormType formType, User creator, User verifier,
            QuarantinedInformation quarantinedInformation) {
        this.applicationFormId = applicationFormId;
        this.verificationResult = verificationResult;
        this.content = content;
        this.createdDate = createdDate;
        this.verifiedDate = verifiedDate;
        this.formType = formType;
        this.creator = creator;
        this.verifier = verifier;
        this.quarantinedInformation = quarantinedInformation;
    }

    public Integer getApplicationFormId() {
        return applicationFormId;
    }

    public void setApplicationFormId(Integer applicationFormId) {
        this.applicationFormId = applicationFormId;
    }

    public Integer getVerificationResult() {
        return verificationResult;
    }

    public void setVerificationResult(Integer verificationResult) {
        this.verificationResult = verificationResult;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getVerifiedDate() {
        return verifiedDate;
    }

    public void setVerifiedDate(Date verifiedDate) {
        this.verifiedDate = verifiedDate;
    }

    public FormType getFormType() {
        return formType;
    }

    public void setFormType(FormType formType) {
        this.formType = formType;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public User getVerifier() {
        return verifier;
    }

    public void setVerifier(User verifier) {
        this.verifier = verifier;
    }

    public QuarantinedInformation getQuarantinedInformation() {
        return quarantinedInformation;
    }

    public void setQuarantinedInformation(QuarantinedInformation quarantinedInformation) {
        this.quarantinedInformation = quarantinedInformation;
    }
}
