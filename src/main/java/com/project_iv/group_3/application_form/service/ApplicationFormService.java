package com.project_iv.group_3.application_form.service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.project_iv.group_3.application_form.entity.ApplicationForm;
import com.project_iv.group_3.application_form.repository.ApplicationFormRepository;
import com.project_iv.group_3.common.service.MyUploadFileService;
import com.project_iv.group_3.form_type.entity.FormType;
import com.project_iv.group_3.quarantined_information.entity.QuarantinedInformation;
import com.project_iv.group_3.user.entity.User;
import com.project_iv.group_3.user.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApplicationFormService {
    
    @Autowired
    private ApplicationFormRepository applicationFormRepository;

    @Autowired 
    private UserService userService;

    @Autowired 
    private MyUploadFileService uploadFileService;

    public List<ApplicationForm> getAllApplicationFormsByQuarantinedInformationId(Integer quarantinedInformationId){
        return applicationFormRepository.getAllApplicationFormsByQuarantinedInformationId(quarantinedInformationId);
    }

    public List<ApplicationForm> searchApplicationForms(String keyword){
        if (keyword.trim().isEmpty()) {
            return new ArrayList<ApplicationForm>();
        }
        return applicationFormRepository.searchApplicationForms(keyword);
    }

    public Boolean deleteApplicationForm(Integer id){
        ApplicationForm applicationForm = applicationFormRepository.getById(id);
        if (applicationForm!= null) {
            if (applicationForm.getContent() != null &&  !applicationForm.getContent().isEmpty()) {
                uploadFileService.deleteUploadedFileDocx(applicationForm.getContent());
            }
            applicationFormRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public ApplicationForm addNewApplicationForm(ApplicationForm applicationForm, Principal principal, QuarantinedInformation quarantinedInformation){
        User userCreate = userService.findByUsername(principal.getName());
        applicationForm.setCreator(userCreate);
        applicationForm.setCreatedDate(new Date());
        applicationForm.setVerificationResult(0);
        applicationForm.setQuarantinedInformation(quarantinedInformation);
        ApplicationForm savedApplicationForm = applicationFormRepository.save(applicationForm);
        return savedApplicationForm;
    }

    public ApplicationForm getApplicationFormById(Integer id){
        boolean existsById = applicationFormRepository.existsById(id);
        if (existsById) {
            return applicationFormRepository.getById(id);
        }
        return null;
    }

    public ApplicationForm updateApplicationForm(Integer applicationFormId, FormType formType, String content){
        ApplicationForm applicationForm = applicationFormRepository.getById(applicationFormId);
        if (applicationForm != null) {
            applicationForm.setFormType(formType);
            if (content != "") {
                applicationForm.setContent(content);
            }
            ApplicationForm savedApplicationForm = applicationFormRepository.save(applicationForm);
            return savedApplicationForm;
        }
        return new ApplicationForm();
    }

    public Boolean checkQuarantinedInformationExistInApplicationForm(Integer quarantinedInformationId){
        Integer checkQuarantinedInformationExistInApplicationForm = applicationFormRepository.checkQuarantinedInformationExistInApplicationForm(quarantinedInformationId);
        if (checkQuarantinedInformationExistInApplicationForm == 1) {
            return true;
        }
        return false;
    }

    public ApplicationForm verifyApplicationForm(Integer applicationFormId, Integer verificationResult, Principal principal){
        ApplicationForm applicationForm = applicationFormRepository.getById(applicationFormId);
        if (applicationForm != null) {
            User userVerify = userService.findByUsername(principal.getName());
            applicationForm.setVerificationResult(verificationResult);
            applicationForm.setVerifiedDate(new Date());
            applicationForm.setVerifier(userVerify);
            
            ApplicationForm savedApplicationForm = applicationFormRepository.save(applicationForm);
            return savedApplicationForm;
        }
        return new ApplicationForm();
    }

    public Boolean checkApplicationFormVerified(Integer applicationFormId){
        Integer checkApplicationFormVerified = applicationFormRepository.checkApplicationFormVerified(applicationFormId);
        if (checkApplicationFormVerified == 1) {
            return true;
        }
        return false;
    }
}
