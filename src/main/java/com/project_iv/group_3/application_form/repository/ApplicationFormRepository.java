package com.project_iv.group_3.application_form.repository;

import java.util.List;

import com.project_iv.group_3.application_form.entity.ApplicationForm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ApplicationFormRepository extends JpaRepository<ApplicationForm, Integer> {
    
    @Query(nativeQuery = true, value = "SELECT DISTINCT af.*, ft.form_type_name AS formType, u.fullname AS creator FROM application_form af INNER JOIN form_type ft ON af.form_type_id = ft.form_type_id INNER JOIN users u ON af.creator_id = u.id WHERE quarantined_information_id = ?1 ORDER BY application_form_id DESC")
    public List<ApplicationForm> getAllApplicationFormsByQuarantinedInformationId(Integer quarantinedInformationId);

    @Query(nativeQuery = true, value = "SELECT DISTINCT af.*, ft.form_type_name AS formType, u.fullname AS creator FROM application_form af INNER JOIN form_type ft ON af.form_type_id = ft.form_type_id INNER JOIN users u ON af.creator_id = u.id WHERE CONCAT(ft.form_type_name,af.content,u.fullname) LIKE %?1% ORDER BY application_form_id DESC")
    public List<ApplicationForm> searchApplicationForms(String keyword);

    @Query(nativeQuery = true, value = "select exists (select af.quarantined_information_id from application_form af where af.quarantined_information_id = ?1)")
    public Integer checkQuarantinedInformationExistInApplicationForm(Integer quarantinedInformationId);

    @Query(nativeQuery = true, value = "select exists (select * from application_form where application_form_id = ?1 and verification_result = 0)")
    public Integer checkApplicationFormVerified(Integer applicationFormId);
}
