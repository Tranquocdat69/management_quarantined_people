package com.project_iv.group_3.security.user_service;

import com.project_iv.group_3.security.user_detail.CustomUserDetail;
import com.project_iv.group_3.user.entity.User;
import com.project_iv.group_3.user.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class CustomUserDetailService implements UserDetailsService{

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userByUsername = userService.findByUsername(username);
        if (userByUsername == null){
            throw  new UsernameNotFoundException("User not found");
        }
        return new CustomUserDetail(userByUsername);
    }
    
}
