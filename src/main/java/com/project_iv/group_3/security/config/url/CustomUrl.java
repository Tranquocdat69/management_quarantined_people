package com.project_iv.group_3.security.config.url;

public class CustomUrl {

    public static String indexString = "/";
    public static String loginString = "/login";
    public static String allowChangePasswordFirstTimeString = "/change-password-first-time/**";
    public static String changePasswordFirstTimeString = "/change-password-first-time";
    public static String doLoginString = "/doLogin";
    public static String doLogoutString = "/doLogout";
    public static String forgotPasswordUrl = "/forgot-password/**";
    public static String resetPasswordUrl = "/reset-password/**";

    // static file
    public static String staticFileLogin = "/login/**";
    public static String staticFileCommon = "/common_dashboard_login/**";
    public static String staticFileDashboard = "/dashboard/**";
    public static String staticFileError = "/error/**";
    public static String error403 = "/error/403.html";
}
