package com.project_iv.group_3.security.config.url.room;

public class RoomUrl {
    //room
    public static String create = "/rooms/create-room";
    public static String edit = "/rooms/edit/**";
    public static String delete = "/rooms/delete/**";
}
