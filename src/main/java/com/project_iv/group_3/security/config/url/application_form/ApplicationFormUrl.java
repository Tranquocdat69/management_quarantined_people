package com.project_iv.group_3.security.config.url.application_form;

public class ApplicationFormUrl {
    public static String create = "/application-form/create-application-form/**"; 
    public static String edit = "/application-form/detail/**"; 
    public static String delete = "/application-form/deleteApplicationForm/**"; 
    public static String verify = "/application-form/verifyApplicationForm/**"; 
    public static String information = "/application-form/information/**";
}
