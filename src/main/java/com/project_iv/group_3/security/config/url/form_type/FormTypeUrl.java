package com.project_iv.group_3.security.config.url.form_type;

public class FormTypeUrl {
     //form-type
     public static String create = "/form-type/create-form-type";
     public static String detail = "/form-type/detail/**";
     public static String delete = "/form-type/deleteFormType/**";
}
