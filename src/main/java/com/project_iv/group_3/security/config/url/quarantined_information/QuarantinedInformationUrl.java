package com.project_iv.group_3.security.config.url.quarantined_information;

public class QuarantinedInformationUrl {
    public static String create = "/quarantined-information/create-quarantined-information/**"; 
    public static String edit = "/quarantined-information/edit-quarantined-information/**"; 
    public static String delete = "/quarantined-information/deleteQuanrantinedInformationById/**"; 
    public static String complete = "/quarantined-information/completeQuarantined/**";
}
