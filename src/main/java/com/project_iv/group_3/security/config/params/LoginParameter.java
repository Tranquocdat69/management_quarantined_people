package com.project_iv.group_3.security.config.params;

public class LoginParameter {
    public static String username = "username";
    public static String password = "password";
    public static String cookieName = "JSESSIONID";
    public static String keyRemember = "Abcxyz123456";
}
