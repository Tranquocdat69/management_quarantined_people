package com.project_iv.group_3.security.config.url.test_type;

public class TestTypeUrl {
    //test-type
    public static String create = "/test-type/create-testType";
    public static String edit = "/test-type/edit-testType/**";
    public static String delete = "/test-type/delete/**";
}
