package com.project_iv.group_3.security.config.url.user;

public class UserUrl {
    //user
    public static String usersUrl = "/users/**";
    public static String allowCheckMatchPassword = "/users/isCorrectPassword/**";
}
