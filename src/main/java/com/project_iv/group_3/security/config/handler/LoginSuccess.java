package com.project_iv.group_3.security.config.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.project_iv.group_3.security.config.url.CustomUrl;
import com.project_iv.group_3.user.entity.User;
import com.project_iv.group_3.user.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class LoginSuccess extends SimpleUrlAuthenticationSuccessHandler{

            @Autowired
            private UserService service;

            @Override
            public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
                    User findByUsername = service.findByUsername(authentication.getName());
                    System.out.println("Logged user: " + authentication.getName());
                    Boolean isFirstTimeLogin = findByUsername.getIsFirstTimeLogin();
                
                    if (isFirstTimeLogin) {
                        response.sendRedirect(CustomUrl.changePasswordFirstTimeString+"/"+findByUsername.getPasswordResetToken()); 
                    }else{
                        response.sendRedirect(CustomUrl.indexString); 
                    }
            }
}
