package com.project_iv.group_3.security.config;

import com.project_iv.group_3.role.model.ListRole;
import com.project_iv.group_3.security.config.handler.CustomAccessDeniedHandler;
import com.project_iv.group_3.security.config.handler.LoginFailure;
import com.project_iv.group_3.security.config.handler.LoginSuccess;
import com.project_iv.group_3.security.config.handler.LogoutSuccess;
import com.project_iv.group_3.security.config.params.LoginParameter;
import com.project_iv.group_3.security.config.url.CustomUrl;
import com.project_iv.group_3.security.config.url.application_form.ApplicationFormUrl;
import com.project_iv.group_3.security.config.url.area.AreaUrl;
import com.project_iv.group_3.security.config.url.form_type.FormTypeUrl;
import com.project_iv.group_3.security.config.url.quarantined_information.QuarantinedInformationUrl;
import com.project_iv.group_3.security.config.url.quarantined_people.QuarantinedPeopleUrl;
import com.project_iv.group_3.security.config.url.room.RoomUrl;
import com.project_iv.group_3.security.config.url.test.TestUrl;
import com.project_iv.group_3.security.config.url.test_type.TestTypeUrl;
import com.project_iv.group_3.security.config.url.user.UserUrl;
import com.project_iv.group_3.security.user_service.CustomUserDetailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private LoginFailure loginFailure;
    
    @Autowired
    private LoginSuccess loginSuccess;
    
    @Autowired
    private LogoutSuccess logoutSuccess;

    @Bean
    public UserDetailsService userDetailsService(){
        return new CustomUserDetailService();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider(){
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService());
        authenticationProvider.setPasswordEncoder(bCryptPasswordEncoder());

        return authenticationProvider;
    }

    @Bean
    public AccessDeniedHandler accessDeniedHandler(){
        return new CustomAccessDeniedHandler();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        .authorizeRequests()
        .antMatchers(CustomUrl.loginString).permitAll()
        .antMatchers(CustomUrl.allowChangePasswordFirstTimeString).permitAll()
        .antMatchers(CustomUrl.staticFileLogin).permitAll()
        .antMatchers(CustomUrl.staticFileCommon).permitAll()
        .antMatchers(UserUrl.allowCheckMatchPassword).permitAll()
        .antMatchers(CustomUrl.staticFileDashboard).permitAll()
        .antMatchers(CustomUrl.error403).authenticated()
        .antMatchers(CustomUrl.staticFileError).permitAll()
        .antMatchers(CustomUrl.forgotPasswordUrl).permitAll()
        .antMatchers(CustomUrl.resetPasswordUrl).permitAll()

        .antMatchers(UserUrl.usersUrl).hasAuthority(ListRole.ADMIN.toString())

        .antMatchers(AreaUrl.areasUrl).hasAuthority(ListRole.ADMIN.toString())
        
        .antMatchers(FormTypeUrl.create).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.HANDLER.toString())
        .antMatchers(FormTypeUrl.detail).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.HANDLER.toString())
        .antMatchers(FormTypeUrl.delete).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.HANDLER.toString())

        .antMatchers(ApplicationFormUrl.create).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.HANDLER.toString())
        .antMatchers(ApplicationFormUrl.edit).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.HANDLER.toString())
        .antMatchers(ApplicationFormUrl.delete).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.HANDLER.toString())
        .antMatchers(ApplicationFormUrl.verify).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.HANDLER.toString())
        .antMatchers(ApplicationFormUrl.information).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.HANDLER.toString())

        .antMatchers(TestTypeUrl.create).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.TESTER.toString())
        .antMatchers(TestTypeUrl.edit).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.TESTER.toString())
        .antMatchers(TestTypeUrl.delete).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.TESTER.toString())
        
        .antMatchers(TestUrl.create).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.TESTER.toString())
        .antMatchers(TestUrl.delete).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.TESTER.toString())
        .antMatchers(TestUrl.updatePositiveResult).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.TESTER.toString())

        .antMatchers(RoomUrl.create).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.ADDER.toString())
        .antMatchers(RoomUrl.edit).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.ADDER.toString())
        .antMatchers(RoomUrl.delete).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.ADDER.toString())

        .antMatchers(QuarantinedPeopleUrl.create).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.ADDER.toString())
        .antMatchers(QuarantinedPeopleUrl.edit).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.ADDER.toString())

        .antMatchers(QuarantinedInformationUrl.create).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.ADDER.toString())
        .antMatchers(QuarantinedInformationUrl.edit).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.ADDER.toString())
        .antMatchers(QuarantinedInformationUrl.delete).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.ADDER.toString())
        .antMatchers(QuarantinedInformationUrl.complete).hasAnyAuthority(ListRole.ADMIN.toString(),ListRole.ADDER.toString())
        
        .anyRequest().authenticated()
        .and()
        .formLogin()
        .loginPage(CustomUrl.loginString).permitAll()
        .usernameParameter(LoginParameter.username)
        .passwordParameter(LoginParameter.password)
        .loginProcessingUrl(CustomUrl.doLoginString)
        .successHandler(loginSuccess)
        .failureHandler(loginFailure)
        .failureUrl(CustomUrl.loginString)
        .and()
        .rememberMe()
        .key(LoginParameter.keyRemember)
        .tokenValiditySeconds(7 * 24 * 60 * 60)
        .and()
        .logout()
        .logoutUrl(CustomUrl.doLogoutString)
        .invalidateHttpSession(true)
        .deleteCookies(LoginParameter.cookieName)
        .permitAll()
        .logoutSuccessHandler(logoutSuccess)
        .and()
        .exceptionHandling().accessDeniedHandler(accessDeniedHandler());
    }
}
