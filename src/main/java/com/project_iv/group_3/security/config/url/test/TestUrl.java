package com.project_iv.group_3.security.config.url.test;

public class TestUrl {
    public static String create = "/test/create-test/**";
    public static String delete = "/test/deleteTestById/**";
    public static String updatePositiveResult = "/test/updatePositiveResultForQuarantinedInformation/**";
}
