package com.project_iv.group_3.area.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="area")
public class Area {
    
    private Integer area_id;
         @Column(nullable=false, length=100)

    private String area_name;
         @Column(nullable=false, length=100)

    private String area_address;
        @Column(nullable=false, length=15)

    private String contact;

    public Area() {
    }
    
    public Area(Integer area_id, String area_name, String area_address, String contact) {
        this.area_id = area_id;
        this.area_name = area_name;

        this.area_address = area_address;

        this.contact = contact;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getArea_id() {
        return area_id;
    }

    public void setArea_id(Integer area_id) {
        this.area_id = area_id;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getArea_address() {
        return area_address;
    }

    public void setArea_address(String area_address) {
        this.area_address = area_address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return  area_name ;
    }
   
    
}
