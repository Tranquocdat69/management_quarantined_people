package com.project_iv.group_3.area.repository;

import java.util.List;

import javax.transaction.Transactional;

import com.project_iv.group_3.area.entity.Area;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


public interface AreaRepository extends JpaRepository<Area,Integer> {
    @Query(nativeQuery = true, value = "select distinct a.* from area a left join room r on r.area_id = a.area_id inner join management_assignment m on m.area_id = a.area_id where r.area_id is not null and m.user_id = ?1")
    public List<Area> getAllAreasExcludeAreaNotExistInRoomByUserId(Integer userId);

    @Query(nativeQuery = true, value = "select * from area order by area_id desc")
        public List<Area> getAllAreasIdDesc();

    @Query(nativeQuery = true, value = "select DISTINCT * FROM area where area_name LIKE %?1% ORDER BY area_id DESC")
    public List<Area> SearchArea(String keyword);
   
    @Query(nativeQuery = true, value = "select exists ( select * from area where area_name = ?1)")
    public Integer CheckExsitArea(String areaName);

    @Query(nativeQuery = true, value = "select exists ( select * from area where area_name != ?1 and area_name = ?2)")
    public Integer CheckExsitEditArea(String currentAreaName, String newAreaName);

    @Query(nativeQuery = true, value = "select distinct a.* from area a left join room r on a.area_id = r.area_id where r.room_id = ?1")
    public Area getAreaByRoomId(Integer roomId);
    
    @Query(nativeQuery = true, value = "select exists ( select * from area where contact = ?1)")
    public Integer CheckExsitContact(String contact);
    
    @Query(nativeQuery = true, value = "select exists ( select * from area where contact != ?1 and contact = ?2)")
    public Integer CheckExsitDiffContact(String currentContact, String newContact);
   
    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "insert into management_assignment values (?1,?2)")
    public void InsertAreaRoleForAdmin(Integer user_id , Integer area_id);

    @Query(nativeQuery = true, value = "select a.* from management_assignment m inner join area a on a.area_id = m.area_id where m.user_id = ?1")
    public List<Area> getAreaByUserId(Integer userId);

    @Query(nativeQuery = true, value = "select exists (select * from management_assignment where area_id = ?1)")
    public Integer checkAreaExistInManagementAssignment(Integer area_id);

    @Query(nativeQuery = true, value = "select exists (select * from room where area_id = ?1)")
    public Integer checkRoomExistInArea(Integer id);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "delete from management_assignment where area_id = ?1")
    public void deleteManagementAssignmentByAreaId(Integer area_id);
}

