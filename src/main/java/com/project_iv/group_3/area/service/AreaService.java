package com.project_iv.group_3.area.service;

import java.util.ArrayList;
import java.util.List;

import com.project_iv.group_3.area.entity.Area;
import com.project_iv.group_3.area.repository.AreaRepository;
import com.project_iv.group_3.security.user_detail.CustomUserDetail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service    
public class AreaService {
    @Autowired
    private AreaRepository res;

    public List<Area> listAll(){
        return res.getAllAreasIdDesc();
    }

    public List<Area> getAllAreasExcludeAreaNotExistInRoomByUserId(){
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetail customUser = (CustomUserDetail) authentication.getPrincipal();
            int userId = customUser.getUser().getId();

        return res.getAllAreasExcludeAreaNotExistInRoomByUserId(userId);
    }

    public Area addNewArea(Area area){
        return res.save(area);
    }

    public void save(Area area_id){
        res.save(area_id);
    }

    public Area getById(Integer area_id){
        boolean existsById = res.existsById(area_id);
        if (existsById) {
            return res.findById(area_id).get();
        }
        return null;
    }

    public List<Area> SearchArea(String keyword){
        if (keyword.trim().isEmpty()) {
            return new ArrayList<Area>();
        }
        return res.SearchArea(keyword);
    }
    public Boolean delete(Integer area_id){
        Area areaToDelete= res.getById(area_id);
        if(areaToDelete!= null){
            Integer checkRoomExistInArea = res.checkRoomExistInArea(area_id);
            if (checkRoomExistInArea==0) {
                Integer checkAreaExistInManagementAssignment = res.checkAreaExistInManagementAssignment(area_id);
            if (checkAreaExistInManagementAssignment ==1) {
                res.deleteManagementAssignmentByAreaId(area_id);
            }
            res.deleteById(area_id);
            return true;
            }
            
        }
        return false;
    }

    public Boolean CheckExsitArea(String areaName) {
        Integer checkExsitArea = res.CheckExsitArea(areaName);
        if (checkExsitArea == 1) {
            return true;
        }

        return false;
    }
    public Boolean CheckExsitDiffContact(String currentContact, String newContact) {
        Integer checkExsitDiffContact = res.CheckExsitDiffContact(currentContact,newContact);
        if (checkExsitDiffContact == 1) {
            return true;
        }

        return false;
    }
    public Boolean CheckExsitContact(String contact) {
        Integer checkExsitContact = res.CheckExsitContact(contact);
        if (checkExsitContact == 1) {
            return true;
        }

        return false;
    }


    public Boolean CheckExsitEditArea(String currentAreaName, String newAreaName ){
        Integer checkExsitEditArea = res.CheckExsitEditArea(currentAreaName, newAreaName);
        if (checkExsitEditArea ==1) {
            return true;
        }
        return false;
    }

    public Area getAreaByRoomId(Integer roomId){
       return res.getAreaByRoomId(roomId);
    }
    public void InsertAreaRoleForAdmin(Integer user_id , Integer area_id){
        res.InsertAreaRoleForAdmin(user_id, area_id);        
    }

   
    
    public List<Area> getAreaByUserId(){
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetail customUser = (CustomUserDetail) authentication.getPrincipal();
            int userId = customUser.getUser().getId();
            
        return res.getAreaByUserId(userId);
    }

}
