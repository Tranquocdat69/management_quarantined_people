package com.project_iv.group_3.area.controller;

import java.util.List;

import com.project_iv.group_3.area.entity.Area;
import com.project_iv.group_3.area.service.AreaService;
import com.project_iv.group_3.user.entity.User;
import com.project_iv.group_3.user.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AreaController {
    @Autowired
    private AreaService service;

    @Autowired
    private UserService userService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    
    @GetMapping("/areas")
    public String list(){
        return "dashboard/area/index";
    }
    @GetMapping("/areas/getAllAreas")
    @ResponseBody
    public List<Area> getAllAreas(){
        List<Area> listAll = service.listAll();
        return listAll;
    }

    @GetMapping("/areas/create-area")
    public String showNewArea(Model model) {
        model.addAttribute("area", new Area());
         
        return "dashboard/area/create"; 
    }

    @PostMapping("/areas/add-area")
    public String addArea(@ModelAttribute("area") Area area){
        Area addNewArea = service.addNewArea(area);
        List<User> allUserAdmin = userService.getAllUserAdmin();
        for (User user : allUserAdmin) {
            service.InsertAreaRoleForAdmin(user.getId(), addNewArea.getArea_id());
        }
        this.simpMessagingTemplate.convertAndSend("/topic/getArea","created");
        return "redirect:/areas";
    }

    @GetMapping("/areas/edit/{area_id}")
    public String showEditForm(@PathVariable(name="area_id") Integer area_id,Model model){
        Area area =service.getById(area_id);
        if (area != null) {
            model.addAttribute("area", area);
            return "dashboard/area/edit";
        }
        return "redirect:/error/404.html";
    }

    @PostMapping("/areas/edit-area")
    public String saveArea(Integer area_id,@ModelAttribute("area") Area area){
        Area areaById =service.getById(area_id);
        if (areaById != null) {
            service.addNewArea(area);
            this.simpMessagingTemplate.convertAndSend("/topic/getArea","edited");
            return "redirect:/areas";
        }
        return "redirect:/error/404.html";
    }

    @GetMapping("/areas/SearchArea")
    @ResponseBody
    public List<Area> SearchArea(@RequestParam String keyword){
        List<Area> SearchArea = service.SearchArea(keyword);
        return SearchArea;
    }

    @GetMapping("/areas/CheckExsitArea")
    @ResponseBody
    public boolean CheckExsitArea(@RequestParam String areaName){
        return service.CheckExsitArea(areaName);
    }

    
    @GetMapping("/areas/CheckExsitContact")
    @ResponseBody
    public boolean CheckExsitContact(@RequestParam String contact){
        return service.CheckExsitContact(contact);
    }
    @GetMapping("/areas/CheckExsitDiffContact")
    @ResponseBody
    public boolean CheckExsitDiffContact(@RequestParam String currentContact, String newContact){
        return service.CheckExsitDiffContact(currentContact,newContact);
    }

    @GetMapping("/areas/CheckExsitEditArea")
    @ResponseBody
    public boolean CheckExsitEditArea(@RequestParam String currentAreaName, String newAreaName){
        return service.CheckExsitEditArea(currentAreaName,newAreaName);
    }
  
    @GetMapping("/areas/delete")
    @ResponseBody
    public Boolean delete(@RequestParam Integer area_id){
        Boolean isDelete = service.delete(area_id);
        if (isDelete) {
            this.simpMessagingTemplate.convertAndSend("/topic/getArea","deleted");
        }
        
        return isDelete;
    }

    @GetMapping("areasInQuarantinedInformation/getAreaByRoomId")
    @ResponseBody
    public Area getAreaByRoomId(@RequestParam Integer roomId){
        return service.getAreaByRoomId(roomId);
    }


    @GetMapping("/user-area/getAreaIdByUserId")
    @ResponseBody
    public List<Area> getAreaByUserId(){
        return service.getAreaByUserId();
    }
}
