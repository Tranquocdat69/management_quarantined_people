package com.project_iv.group_3.error;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {
    @GetMapping("error/404.html")
    public String Page404(){
        return "error/404";
    }
    
    @GetMapping("error/403.html")
    public String Page403(){
        return "error/403";
    }
}
