package com.project_iv.group_3.form_type.controller;

import java.util.List;

import com.project_iv.group_3.form_type.entity.FormType;
import com.project_iv.group_3.form_type.service.FormTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FormTypeController {
    
    @Autowired
    private FormTypeService formTypeService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @GetMapping("/form-type")
    public String index(){
        return "dashboard/form_type/index";
    }

    @GetMapping("/form-type/getAllFormTypes")
    @ResponseBody
    public List<FormType> getAllFormTypes(){
        List<FormType> formTypes = formTypeService.getAllFormTypes();
        return formTypes;
    }

    @GetMapping("/form-type/searchFormTypes")
    @ResponseBody
    public List<FormType> searchFormTypes(@RequestParam String keyword){
        List<FormType> searchFormTypes = formTypeService.searchFormTypes(keyword);
        return searchFormTypes;
    }

    @GetMapping("/form-type/deleteFormType")
    @ResponseBody
    public Boolean deleteFormType(@RequestParam Integer id){
        Boolean isDelete = formTypeService.deleteFormType(id);
        if (isDelete) {
            this.simpMessagingTemplate.convertAndSend("/topic/getFormType","deleted");
        }
        return isDelete;
    }

    @GetMapping("/form-type/create-form-type")
    public String showFormCreate(Model model){
        model.addAttribute("formType", new FormType());
        
        return "dashboard/form_type/create";
    }

    @PostMapping("/form-type/addFormType")
    public String addFormType(FormType formType) throws InterruptedException{
        FormType addNewFormType = formTypeService.addNewFormType(formType);

        if (addNewFormType != null) {
            // Thread.sleep(800);
            this.simpMessagingTemplate.convertAndSend("/topic/getFormType","added");
            return "redirect:/form-type";
        }else{
            return "dashboard/form_type/create";
        }
    }

    @GetMapping("/form-type/getByName")
    @ResponseBody
    public FormType getByName(@RequestParam String name){
        FormType formType = (FormType) formTypeService.findByName(name);
        if (formType != null) {
            return formType;
        }else{
            return new FormType();
        }
    }

    @GetMapping("/form-type/detail/{id}")
    public String getDetailFormType(@PathVariable Integer id, Model model){
        FormType formType = formTypeService.findById(id);
        if (formType != null) {
            model.addAttribute("formType", formType);
            model.addAttribute("currentFormTypeName", formType.getFormTypeName());
            return "dashboard/form_type/detail";
        }
        return "redirect:/error/404.html";
    }

    @PostMapping("/form-type/editFormType")
    public String editFormType(Integer formTypeId,FormType formType) throws InterruptedException{
        FormType formTypeById = formTypeService.findById(formTypeId);
        if (formTypeById != null) {
        FormType editFormType = formTypeService.editFormType(formType.getFormTypeId(), formType.getFormTypeName());
            if (editFormType != null) {
                // Thread.sleep(800);
                this.simpMessagingTemplate.convertAndSend("/topic/getFormType","edited");
                return "redirect:/form-type";
            }else{
                return "redirect:/form-type/detail/"+formType.getFormTypeId();
            }
        }
        return "redirect:/error/404.html";
    }

    @GetMapping("/form-type/checkDuplicateFormType")
    @ResponseBody
    public FormType checkDuplicateFormType(@RequestParam String currentFormType, @RequestParam String newFormType){
        FormType formType = formTypeService.checkDuplicateFormType(currentFormType, newFormType);
        if (formType != null) {
            return formType;
        }else{
            return new FormType();
        }
    }
}
