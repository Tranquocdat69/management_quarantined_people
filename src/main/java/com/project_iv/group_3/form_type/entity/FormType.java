package com.project_iv.group_3.form_type.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_type")
public class FormType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "form_type_id", nullable = false)
    private Integer formTypeId;

    @Column(name = "form_type_name", length = 255, nullable = false, unique = true)
    private String formTypeName;

    public FormType() {
    }

    public FormType(Integer formTypeId, String formTypeName) {
        this.formTypeId = formTypeId;
        this.formTypeName = formTypeName;
    }

    public Integer getFormTypeId() {
        return formTypeId;
    }

    public void setFormTypeId(Integer formTypeId) {
        this.formTypeId = formTypeId;
    }

    public String getFormTypeName() {
        return formTypeName;
    }

    public void setFormTypeName(String formTypeName) {
        this.formTypeName = formTypeName;
    }
}
