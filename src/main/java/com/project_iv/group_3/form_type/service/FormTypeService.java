package com.project_iv.group_3.form_type.service;

import java.util.ArrayList;
import java.util.List;

import com.project_iv.group_3.form_type.entity.FormType;
import com.project_iv.group_3.form_type.repository.FormTypeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FormTypeService {
    
    @Autowired
    private FormTypeRepository formTypeRepository;

    public List<FormType> getAllFormTypes(){
        return formTypeRepository.getAllFormTypes();
    }

    public List<FormType> searchFormTypes(String keyword){
        if (keyword.trim().isEmpty()) {
            return new ArrayList<FormType>();
        }
        return formTypeRepository.searchFormTypes(keyword);
    }

    public Boolean deleteFormType(Integer id){
        FormType formType = formTypeRepository.getById(id);
        if (formType != null) {
            Integer formTypeExistApplicationForm = formTypeRepository.checkFormTypeExistApplicationForm(id);
            if (formTypeExistApplicationForm == 0) {
                formTypeRepository.deleteById(id);
                return true;
            }
        }
        return false;
    }

    public FormType addNewFormType(FormType formType){
        FormType savedFormType = formTypeRepository.save(formType);
        return savedFormType;
    }

    public FormType findByName(String name){
        return formTypeRepository.findByName(name);
    }

    public FormType findById(Integer id){
        boolean existsById = formTypeRepository.existsById(id);
        if (existsById) {
            FormType formType = formTypeRepository.getById(id);
            if (formType != null) {
                return formType;
            }
        }

        return null;
    }

    public FormType editFormType(Integer id,String name){
        FormType formType = formTypeRepository.getById(id);
        if (formType != null) {
            formType.setFormTypeName(name);
    
            FormType savedFormType = formTypeRepository.save(formType);
            return savedFormType;
        }

        return new FormType();
    }

    public FormType checkDuplicateFormType(String currentFormType, String newFormType){
        return formTypeRepository.checkDuplicateFormType(currentFormType, newFormType);
    }
}
