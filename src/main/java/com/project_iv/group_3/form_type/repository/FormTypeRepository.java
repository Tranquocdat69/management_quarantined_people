package com.project_iv.group_3.form_type.repository;

import java.util.List;

import com.project_iv.group_3.form_type.entity.FormType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface FormTypeRepository extends JpaRepository<FormType, Integer> {
    
    @Query(nativeQuery = true, value = "SELECT DISTINCT * FROM form_type ORDER BY form_type_id DESC")
    public List<FormType> getAllFormTypes();

    @Query(nativeQuery = true, value = "SELECT DISTINCT * FROM form_type WHERE form_type_name LIKE %?1% ORDER BY form_type_id DESC")
    public List<FormType> searchFormTypes(String keyword);

    @Query(nativeQuery = true, value = "SELECT * FROM form_type WHERE form_type_name = ?1")
    public FormType findByName(String name);

    @Query(nativeQuery = true, value = "SELECT * FROM form_type WHERE form_type_name != ?1 AND form_type_name = ?2")
    public FormType checkDuplicateFormType(String currentFormType, String newFormType);

    @Query(nativeQuery = true, value = "SELECT EXISTS (SELECT ft.* FROM form_type ft INNER JOIN application_form af ON ft.form_type_id = af.form_type_id WHERE ft.form_type_id = ?1)")
    public Integer checkFormTypeExistApplicationForm(Integer id);
}
