package com.project_iv.group_3.quarantined_people.controller;

import java.io.File;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.project_iv.group_3.area.entity.Area;
import com.project_iv.group_3.area.service.AreaService;
import com.project_iv.group_3.common.model.MyUploadFile;
import com.project_iv.group_3.common.service.MyUploadFileService;
import com.project_iv.group_3.level_infection.entity.LevelInfection;
import com.project_iv.group_3.level_infection.service.LevelInfectionService;
import com.project_iv.group_3.quarantined_information.entity.QuarantinedInformation;
import com.project_iv.group_3.quarantined_information.service.QuarantinedInformationService;
import com.project_iv.group_3.quarantined_people.dto.QuarantinedPeopleDto;
import com.project_iv.group_3.quarantined_people.entity.QuarantinedPeople;
import com.project_iv.group_3.quarantined_people.service.QuarantinedPeopleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class QuarantinedPeopleController {

    @Autowired
    private QuarantinedPeopleService qPservice;
  
    @Autowired
    private AreaService areaService;

    @Autowired
    private LevelInfectionService serviceLevelInfection;

    @Autowired
    private MyUploadFileService uploadFileService;
  
    @Autowired
    private QuarantinedInformationService quarantinedInformationService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate; 
    
    @GetMapping("/quarantined-people")
    public String index(Model model){
        List<Area> listAreas = areaService.getAreaByUserId();

            if (listAreas.size() > 0) {
                model.addAttribute("listAreas", listAreas);
                return "dashboard/quarantined_people/index";
            }
            model.addAttribute("infoMessage", "Hiện chưa có khu vực nào trong hệ thống");
            return "dashboard/quarantined_people/index";
    }

    @GetMapping("/quarantined-people/listAll")
    @ResponseBody
    public List<QuarantinedPeople> listAll(){
        return qPservice.listAll();
    }
   
    @GetMapping("/quarantined-people/getQuarantinedPeopleByRoomId")
    @ResponseBody
    public List<QuarantinedPeopleDto> getQuarantinedPeopleByRoomId(@RequestParam Integer roomIdRQI, @RequestParam Integer statusQI){
        return qPservice.getQuarantinedPeopleByRoomId(roomIdRQI, statusQI);
    }

    @GetMapping("/quarantined-people/create-quarantined-people")
    public String createPage(Model model){
            List<Area> listAreas = areaService.getAllAreasExcludeAreaNotExistInRoomByUserId();

            if (listAreas.size() > 0) {
                List<LevelInfection> allLevelInfection = serviceLevelInfection.getAllLevelInfection();
                model.addAttribute("listAreas", listAreas);
                model.addAttribute("listLevels", allLevelInfection);

                model.addAttribute("quarantinedInformation", new QuarantinedInformation());
                model.addAttribute("quarantinedPeople", new QuarantinedPeople());

                model.addAttribute("myUploadFile", new MyUploadFile());

                return "dashboard/quarantined_people/create";
            }else{
                return "redirect:/rooms/create-room";
            }
            
    }
    
    @PostMapping("/quarantined-people/createQuarantinedPeople")
    public String doCreatePage(QuarantinedPeople quarantinedPeople,QuarantinedInformation quarantinedInformation,MyUploadFile myUploadFile, HttpServletRequest request, Principal principal) throws ParseException{
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date dateOfBirth = simpleDateFormat.parse(request.getParameter("dateOfBirthTemp").toString());
        Date arrivalDate = simpleDateFormat.parse(request.getParameter("arrivalDateTemp").toString());
        Integer newRoomId = Integer.parseInt(request.getParameter("room").toString());

        try {
            MultipartFile multipartFile = myUploadFile.getMultipartFile();
            if (multipartFile != null) {
                String tempFileName = multipartFile.getOriginalFilename();
                if (tempFileName != "") {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                    String extension = tempFileName.substring(tempFileName.indexOf(".")+1).toLowerCase();
                    String name = tempFileName.substring(0, tempFileName.lastIndexOf("."));
                    String fileName = name + timeStamp + "."+extension;
                    File file = new File(uploadFileService.getFolderUpload(), fileName);

                    multipartFile.transferTo(file);
                    Thread.sleep(200);
                    quarantinedPeople.setAvatar(fileName);
                }
            }
          } catch (Exception e) {
            e.printStackTrace();
          }

        QuarantinedPeople addNewQuarantinedPeople = qPservice.addNewQuarantinedPeople(quarantinedPeople,dateOfBirth);

        if (addNewQuarantinedPeople != null) {
        QuarantinedInformation addNewQuarantinedInformation = quarantinedInformationService.addNewQuarantinedInformation(quarantinedInformation,quarantinedPeople,arrivalDate,newRoomId);
            if (addNewQuarantinedInformation != null) {
                this.simpMessagingTemplate.convertAndSend("/topic/getQuarantinedPeople","created");
                this.simpMessagingTemplate.convertAndSend("/topic/getRoom","created new quarantined people");
                return "redirect:/quarantined-people";
            }
        }

        return "redirect:/error/404.html";
    }

    @GetMapping("/quarantined-people/checkIdentityCardExist")
    @ResponseBody
    public Boolean checkIdentityCardExist(@RequestParam String identityCard){
        return qPservice.checkIdentityCardExist(identityCard);
    }
    
    @GetMapping("/quarantined-people/searchQuarantinedPeople")
    @ResponseBody
    public List<QuarantinedPeopleDto> searchQuarantinedPeople(@RequestParam Integer roomIdRQI, @RequestParam Integer statusQI,@RequestParam String keyword){
        return qPservice.searchQuarantinedPeoples(roomIdRQI, statusQI, keyword);
    }

    @GetMapping("/quarantined-people/edit-quarantined-people/{id}")
    public String editPage(@PathVariable Integer id, Model model) throws ParseException{
        
        Boolean checkUserHasAuthorAccessDetail = qPservice.checkUserHasAuthorAccessDetail(id);
        if (checkUserHasAuthorAccessDetail) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            QuarantinedPeople quarantinedPeopleById = qPservice.getQuarantinedPeopleById(id);
            if (quarantinedPeopleById != null) {
                String dateOfBirthString = simpleDateFormat.format(quarantinedPeopleById.getDateOfBirth());
        
                model.addAttribute("quarantinedPeople", quarantinedPeopleById);
                model.addAttribute("dateOfBirthString", dateOfBirthString);
                model.addAttribute("myUploadFile", new MyUploadFile());
                return "dashboard/quarantined_people/edit";
            }
        }
        return "redirect:/error/404.html";
    }

    @PostMapping("/quarantined-people/editQuarantinedPeople")
    public String doEditPage(Integer id,QuarantinedPeople quarantinedPeople,HttpServletRequest request, MyUploadFile myUploadFile,Principal principal) throws ParseException{
        Boolean checkUserHasAuthorAccessDetail = qPservice.checkUserHasAuthorAccessDetail(id);
        QuarantinedPeople quarantinedPeopleById = qPservice.getQuarantinedPeopleById(id);
        if (checkUserHasAuthorAccessDetail) {
            if (quarantinedPeopleById != null) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date dateOfBirth = simpleDateFormat.parse(request.getParameter("dateOfBirthTemp").toString());
        
                try {
                    MultipartFile multipartFile = myUploadFile.getMultipartFile();
                    if (multipartFile != null) {
                        String tempFileName = multipartFile.getOriginalFilename();
                        if (tempFileName != "") {
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                            String extension = tempFileName.substring(tempFileName.indexOf(".")+1).toLowerCase();
                            String name = tempFileName.substring(0, tempFileName.lastIndexOf("."));
                            String fileName = name + timeStamp + "."+extension;
                            File file = new File(uploadFileService.getFolderUpload(), fileName);
                            String currentAvatar = request.getParameter("nameOfAvatar");
        
                            if (currentAvatar != null && !currentAvatar.trim().isEmpty()) {
                                uploadFileService.deleteUploadedFile(currentAvatar);
                            }
        
                            multipartFile.transferTo(file);
                            Thread.sleep(200);
                            quarantinedPeople.setAvatar(fileName);
                        }else{
                            quarantinedPeople.setAvatar(request.getParameter("nameOfAvatar").toString());
                        }
                    }
                  } catch (Exception e) {
                    e.printStackTrace();
                  }
        
                  QuarantinedPeople updateQuarantinedPeople = qPservice.updateQuarantinedPeople(quarantinedPeople,quarantinedPeople.getAvatar() != null ? quarantinedPeople.getAvatar() : "", dateOfBirth);
                  if (updateQuarantinedPeople != null) {
                      this.simpMessagingTemplate.convertAndSend("/topic/getQuarantinedPeople","edited");
                      this.simpMessagingTemplate.convertAndSend("/topic/getQuarantinedPeople/"+updateQuarantinedPeople.getId(),"edited "+updateQuarantinedPeople.getId());
                      return "redirect:/quarantined-people";
                  }
            }
        }
        return "redirect:/error/404.html";
    }

    @GetMapping("/quarantined-people/detailQuarantinedPeople")
    @ResponseBody
    public QuarantinedPeople detailQuarantinedPeopleAndQuarantinedInformation(@RequestParam Integer id){
        return qPservice.getQuarantinedPeopleById(id);
    }

    @GetMapping("/quarantined-people/detail-quarantined-people/{id}")
    public String detailPage(@PathVariable Integer id, Model model) throws ParseException{

        Boolean checkUserHasAuthorAccessDetail = qPservice.checkUserHasAuthorAccessDetail(id);
        if (checkUserHasAuthorAccessDetail) {
            model.addAttribute("id", id);
            return "dashboard/quarantined_people/detail";
        }
        return "redirect:/error/404.html";
    }

    @GetMapping("/quarantined-people/checkIdentityCardExistWhenEdit")
    @ResponseBody
    public Boolean checkIdentityCardExistWhenEdit(@RequestParam String currentIdentityCard, @RequestParam String newIdentityCard){
        return qPservice.checkIdentityCardExistWhenEdit(currentIdentityCard,newIdentityCard);
    }
 
    @GetMapping("/quarantined-people/deleteQuarantinedPeople")
    @ResponseBody
    public Boolean deleteQuarantinedPeople(@RequestParam Integer id){
        Boolean deleteQuarantinedPeople = qPservice.deleteQuarantinedPeople(id);
        if (deleteQuarantinedPeople) {
            this.simpMessagingTemplate.convertAndSend("/topic/getQuarantinedPeople","deleted");
            this.simpMessagingTemplate.convertAndSend("/topic/getRoom","deleted quarantined people");
            return true;
        }else{
            return false;
        }
    }
}

