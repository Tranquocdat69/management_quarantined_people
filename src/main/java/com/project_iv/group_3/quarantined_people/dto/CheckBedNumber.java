package com.project_iv.group_3.quarantined_people.dto;

public interface CheckBedNumber {
    public Integer getTotalPeople();

    public Integer getLimitBedNumber();
    
}
