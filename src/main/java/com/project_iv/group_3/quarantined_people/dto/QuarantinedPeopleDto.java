package com.project_iv.group_3.quarantined_people.dto;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public interface QuarantinedPeopleDto {
    
    public Integer getId();

    public String getFullname();

    public String getPhoneNumber();

    public Integer getGender();

    // public String getProvince();

    // public String getDistrict();

    // public String getWard();

    @JsonFormat(pattern = "dd/MM/yyyy",timezone = "GMT+8")
    public Date getDateOfBirth();

    public String getIdentityCard();

}
