package com.project_iv.group_3.quarantined_people.repository;

import java.util.List;

import com.project_iv.group_3.quarantined_people.dto.QuarantinedPeopleDto;
import com.project_iv.group_3.quarantined_people.entity.QuarantinedPeople;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface QuarantinedPeopleRepository extends JpaRepository<QuarantinedPeople,Integer>{

    @Query(nativeQuery = true, value = "SELECT q.id,q.fullname,q.phone_number as phoneNumber,q.gender,q.date_of_birth as dateOfBirth,q.identity_card as identityCard FROM db_covid.quarantined_people q inner join quarantined_information qi on q.id = qi.quarantined_people_id inner join room_quarantined_information rqi on rqi.quarantined_information_id = qi.id where rqi.room_id = ?1 and rqi.status = ?2 and qi.status = ?3 and concat(q.fullname,q.phone_number,q.identity_card) like %?4%")
    public List<QuarantinedPeopleDto> searchQuarantinedPeoples(Integer roomIdRQI, Integer statusRQI, Integer statusQI,String keyword);

    @Query(nativeQuery = true, value = "select * from quarantined_people order by id desc")
    public List<QuarantinedPeople> getAllQuarantinedPeopleIdDesc();

    @Query(nativeQuery = true, value = "select exists (select * from quarantined_people where email = ?1)")
    public Integer checkEmailExist(String email);
    
    @Query(nativeQuery = true, value = "select exists (select * from quarantined_people where phone_number = ?1)")
    public Integer checkPhoneNumberExist(String phoneNumber);
    
    @Query(nativeQuery = true, value = "select exists (select * from quarantined_people where identity_card = ?1)")
    public Integer checkIdentityCardExist(String identityCard);
   
    @Query(nativeQuery = true, value = "select exists (select qp.identity_card from quarantined_people qp inner join quarantined_information qi on qp.id = qi.quarantined_people_id where qp.id = ?1)")
    public Integer checkQuarantinedPeopleInQuarantinedInformation(Integer id);

    @Query(nativeQuery = true,value = "select exists (select * from quarantined_people q where q.identity_card != ?1 and q.identity_card = ?2)")
    public Integer checkIdentityCardExistWhenEdit(String currentIdentityCard,String newIdentityCard);
    
    @Query(nativeQuery = true,value = "SELECT distinct q.id,q.fullname,q.phone_number as phoneNumber,q.gender,q.date_of_birth as dateOfBirth,q.identity_card as identityCard FROM db_covid.quarantined_people q inner join quarantined_information qi on q.id = qi.quarantined_people_id inner join room_quarantined_information rqi on rqi.quarantined_information_id = qi.id where rqi.room_id = ?1 and rqi.status = ?2 and qi.status = ?3")
    public List<QuarantinedPeopleDto> getQuarantinedPeopleByRoomId(Integer roomIdRQI, Integer statusRQI, Integer statusQI);

    

    @Query(nativeQuery = true, value = "select exists(select m.area_id from (select a.area_id from area a inner join room r on r.area_id = a.area_id inner join room_quarantined_information rqi on rqi.room_id = r.room_id inner join quarantined_information qi on qi.id = rqi.quarantined_information_id where qi.quarantined_people_id = ?1) as userArea,management_assignment m where m.user_id = ?2 and m.area_id = userArea.area_id)")
    public Integer checkUserHasAuthorAccessDetail(Integer quanrantinedPeopleId, Integer currentUserId);

}
