package com.project_iv.group_3.quarantined_people.service;

import java.util.Date;
import java.util.List;

import com.project_iv.group_3.common.service.MyUploadFileService;
import com.project_iv.group_3.quarantined_people.dto.QuarantinedPeopleDto;
import com.project_iv.group_3.quarantined_people.entity.QuarantinedPeople;
import com.project_iv.group_3.quarantined_people.repository.QuarantinedPeopleRepository;
import com.project_iv.group_3.security.user_detail.CustomUserDetail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class QuarantinedPeopleService {
    @Autowired 
    private QuarantinedPeopleRepository repo;

    @Autowired
    private MyUploadFileService uploadFileService;

    public List<QuarantinedPeople> listAll(){
        return repo.getAllQuarantinedPeopleIdDesc();
    }
   
    public QuarantinedPeople getById(Integer id){
        boolean existsById = repo.existsById(id);
        if (existsById) {
            return repo.getById(id);
        }

        return null;
    }

    public QuarantinedPeople getQuarantinedPeopleById(Integer id){
        boolean existsById = repo.existsById(id);
        if (existsById) {
            return repo.findById(id).get();
        }
        return null;
    }

    public QuarantinedPeople addNewQuarantinedPeople(QuarantinedPeople quarantinedPeople, Date dateOfBirth){
        quarantinedPeople.setDateOfBirth(dateOfBirth);

        // User userCreate = userService.findByUsername(principal.getName());
        QuarantinedPeople savedPerson = repo.save(quarantinedPeople);
        return savedPerson;
    }
    
    public QuarantinedPeople updateQuarantinedPeople(QuarantinedPeople quarantinedPeople,String avatar,Date dateOfBirth){
        quarantinedPeople.setDateOfBirth(dateOfBirth);
        if (avatar != "") {
            quarantinedPeople.setAvatar(avatar);
        }
        QuarantinedPeople savedPerson = repo.save(quarantinedPeople);
        return savedPerson;
    }

    public Boolean checkIdentityCardExist(String identityCard){
        Integer checkIdentityCardExist = repo.checkIdentityCardExist(identityCard);
        if (checkIdentityCardExist == 1) {
            return true;
        }
        return false;
    }

    public List<QuarantinedPeopleDto> searchQuarantinedPeoples(Integer roomIdRQI, Integer statusQI,String keyword){
        int statusRQI = 0;

        if (statusQI != 0) {
            statusRQI = 1;
        }
        return repo.searchQuarantinedPeoples(roomIdRQI, statusRQI, statusQI, keyword);
    }

    public Boolean checkIdentityCardExistWhenEdit(String currentIdentityCard,String newIdentityCard){
        Integer checkIdentityCardExistWhenEdit = repo.checkIdentityCardExistWhenEdit(currentIdentityCard,newIdentityCard);
        if (checkIdentityCardExistWhenEdit == 1) {
            return true;
        }
        return false;
    }
  
    public Boolean deleteQuarantinedPeople(Integer id){
        Integer checkQuarantinedPeopleInQuarantinedInformation = repo.checkQuarantinedPeopleInQuarantinedInformation(id);
        if (checkQuarantinedPeopleInQuarantinedInformation == 0) {
            QuarantinedPeople quarantinedPeopleNeedDeleting = repo.getById(id);
            if (quarantinedPeopleNeedDeleting.getAvatar() != null &&  !quarantinedPeopleNeedDeleting.getAvatar().isEmpty()) {
                uploadFileService.deleteUploadedFile(quarantinedPeopleNeedDeleting.getAvatar());
            }
            repo.deleteById(id);
            return true;
        }
        return false;
    }

    public List<QuarantinedPeopleDto> getQuarantinedPeopleByRoomId(Integer roomIdRQI, Integer statusQI){
        int statusRQI = 0;

        if (statusQI != 0) {
            statusRQI = 1;
        }

        return repo.getQuarantinedPeopleByRoomId(roomIdRQI, statusRQI, statusQI);
    }

    public Boolean checkUserHasAuthorAccessDetail(Integer quanrantinedPeopleId){
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            CustomUserDetail customUser = (CustomUserDetail) authentication.getPrincipal();
            int userId = customUser.getUser().getId();

        Integer checkUserHasAuthorAccessDetail = repo.checkUserHasAuthorAccessDetail(quanrantinedPeopleId, userId);
        if (checkUserHasAuthorAccessDetail == 1) {
            return true;
        }
        return false;
    }
}
