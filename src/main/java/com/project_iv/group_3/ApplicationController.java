package com.project_iv.group_3;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class ApplicationController {
    @GetMapping("/")
    public String index(){
        return "dashboard/index";
    }
}
