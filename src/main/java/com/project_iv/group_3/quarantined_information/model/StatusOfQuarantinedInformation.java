package com.project_iv.group_3.quarantined_information.model;

import java.util.HashMap;
import java.util.Map;

public class StatusOfQuarantinedInformation {
    public static Map<Integer, String> status = new HashMap<Integer, String>(){
        {
            put(0, "Chưa hoàn thành");
            put(1, "Đã hoàn thành");
            put(2, "Chưa hoàn thành/dương tính");
        }
    };

    public static Integer status_0 = (Integer) status.keySet().toArray()[0];
    public static Integer status_1 = (Integer) status.keySet().toArray()[1];
    public static Integer status_2 = (Integer) status.keySet().toArray()[2];
}
