package com.project_iv.group_3.quarantined_information.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import com.project_iv.group_3.quarantined_information.dto.CurrentRoomId;
import com.project_iv.group_3.quarantined_information.entity.QuarantinedInformation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


public interface QuarantinedInformationRepository extends JpaRepository<QuarantinedInformation,Integer>{
    
    @Query(nativeQuery = true, value = "select qi.* from quarantined_information qi where qi.quarantined_people_id = ?1")
    public List<QuarantinedInformation> getQuarantinedInformationByQuarantinedPeopleId(Integer quarantinedPeopleId);

    @Query(nativeQuery = true, value = "select exists (select * from quarantined_information where quarantined_people_id = ?1 and status = 0)")
    public Integer checkQuarantinedInformationNotComplete(Integer quarantinedPeopleId);

    @Query(nativeQuery = true, value = "select rqi.room_id as roomId from quarantined_information q inner join room_quarantined_information rqi on rqi.quarantined_information_id = q.id where rqi.status = 0 and q.id = ?1")
    public CurrentRoomId getCurrentRoomIdByQuarantinedInformationId(Integer id);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "update room_quarantined_information set room_id = ?1 where quarantined_information_id = ?2 and status = 0")
    public void updateCurrentRoomQuarantinedInformation(Integer newRoomId, Integer id);
    
    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "update room_quarantined_information set status = 1, leave_date = ?1 where quarantined_information_id = ?2 and status = 0")
    public void updateStatusRoomQuarantinedInformation(Date leaveRoomDate,Integer id);
    
    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "update quarantined_information set status = ?1 where id = ?2")
    public void updateStatusQuarantinedInformation(Integer status,Integer id);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "insert into room_quarantined_information values(0,?1,?2,null,0)")
    public void addNewRoomQuarantinedInformation(Integer newRoomId, Integer id);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "update room_quarantined_information set status = ?1, leave_date = ?2 where quarantined_information_id = ?3")
    public void updateRoomQuarantinedInformation(Integer status, Date leaveDate, Integer quarantinedInformationId);
   
    @Query(nativeQuery = true, value = "select exists (select q.id from quarantined_information q where arrival_date = ?1 and quarantined_people_id=?2)")
    public Integer checkExistsArrivalDate(String newArrivalDate,Integer quarantinedPeopleIdToCheck);
   
    @Query(nativeQuery = true, value = "select exists (select q.id from quarantined_information q where arrival_date = ?1 and arrival_date != ?2 and quarantined_people_id=?3)")
    public Integer checkExistsArrivalDateWhenEdit(String newArrivalDate,String currentArrivalDate,Integer quarantinedPeopleIdToCheck);

    @Query(nativeQuery = true, value = "select qi.* from quarantined_information qi where qi.quarantined_people_id = ?1 and qi.status = 1 order by qi.id desc limit 1")
    public QuarantinedInformation getLastDepartureDateByQuarantinedPeopleId(Integer quarantinedPeopleId);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "delete from room_quarantined_information where quarantined_information_id = ?1")
    public void deleteRoomQuarantinedInformationByQuarantinedInformationId(Integer id);
    
    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "delete from room_quarantined_information where id = ?1")
    public void deleteRoomQuarantinedInformationByRoomQuarantinedInformationId(Integer roomQuarantinedInformationId);
   
    @Query(nativeQuery = true, value = "select * from room_quarantined_information where id = ?1")
    public Map<String,Object> getRoomQuarantinedInformationByRoomQuarantinedInformationId(Integer roomQuarantinedInformationId);
    
    @Query(nativeQuery = true, value = "select count(id) as totalQuarantinedInfo from quarantined_information where quarantined_people_id = ?1")
    public Integer getTotalQuanrantinedInformationByQuarantinedPeopleId(Integer quarantinedPeopleId);

    @Query(nativeQuery = true, value = "select * from quarantined_information where quarantined_people_id = ?1 and status = 0")
    public QuarantinedInformation getQuarantinedInformationNotCompleteByQuarantinedPeopleId(Integer quarantinedPeopleId);

    @Query(nativeQuery = true, value = "select exists (select * from quarantined_information where id = ?1 and status = 0)")
    public Integer checkQuarantinedInformationNotCompleteById(Integer quarantinedInformationId);
    
    @Query(nativeQuery = true, value = "select number_of_day_quarantined from level_infection where id = ?1")
    public Integer getNumberOfDayQuarantinedByLevelInfectionId(Integer levelInfectionId);
   
    @Query(nativeQuery = true, value = "select exists (select * from room_quarantined_information where quarantined_information_id = ?1 and status = 1)")
    public Integer checkExistsRoomHistory(Integer id);
   
    @Query(nativeQuery = true, value = "select exists (select * from test where quarantined_information_id = ?1)")
    public Integer checkExistsTest(Integer quarantinedInformationId);
  
    @Query(nativeQuery = true, value = "select exists (select * from application_form where quarantined_information_id = ?1)")
    public Integer checkExistsApplicationForm(Integer quarantinedInformationId);
    
    @Query(nativeQuery = true, value = "select rqi.*,r.room_name,qi.arrival_date from room_quarantined_information rqi inner join room r on r.room_id = rqi.room_id inner join quarantined_information qi on qi.id = rqi.quarantined_information_id where quarantined_information_id = ?1 order by leave_date desc")
    public List<Object> getAllRoomQuarantinedInformationByQuarantinedInforId(Integer quarantinedInformationId);

    @Query(nativeQuery = true, value = "select count(*) from quarantined_information where status = ?1")
    public Integer getDataApplication(Integer status);

}
