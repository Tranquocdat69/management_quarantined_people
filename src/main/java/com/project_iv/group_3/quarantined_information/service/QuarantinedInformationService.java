package com.project_iv.group_3.quarantined_information.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.project_iv.group_3.application_form.service.ApplicationFormService;
import com.project_iv.group_3.quarantined_information.dto.CurrentRoomId;
import com.project_iv.group_3.quarantined_information.entity.QuarantinedInformation;
import com.project_iv.group_3.quarantined_information.model.StatusOfQuarantinedInformation;
import com.project_iv.group_3.quarantined_information.repository.QuarantinedInformationRepository;
import com.project_iv.group_3.quarantined_people.entity.QuarantinedPeople;
import com.project_iv.group_3.test.service.TestService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuarantinedInformationService {
    @Autowired
    private QuarantinedInformationRepository repo;
    
    @Autowired
    private ApplicationFormService applicationFormService;
  
    @Autowired
    private TestService testService;

    public QuarantinedInformation addNewQuarantinedInformation(QuarantinedInformation quarantinedInformation,QuarantinedPeople quarantinedPeopleById,Date arrivalDate, Integer newRoomId){
        quarantinedInformation.setStatus(StatusOfQuarantinedInformation.status_0);
        quarantinedInformation.setQuarantinedPeople(quarantinedPeopleById);
        quarantinedInformation.setArrivalDate(arrivalDate);

        Integer numberOfDayQuarantined = quarantinedInformation.getLevelInfection().getNumberOfDayQuarantined();
        Calendar c = Calendar.getInstance(); 
        c.setTime(arrivalDate); 
        c.add(Calendar.DATE, numberOfDayQuarantined);
        Date departureDate = c.getTime();

        quarantinedInformation.setDepartureDate(departureDate);
        QuarantinedInformation save = repo.save(quarantinedInformation);
        repo.addNewRoomQuarantinedInformation(newRoomId, save.getId());

        return save;
    }
    
    public QuarantinedInformation editQuarantinedInformation(QuarantinedInformation quarantinedInformation,QuarantinedPeople quarantinedPeopleById,Date arrivalDate, Integer statusEditRoom, Integer newRoomId, Integer currentRoomId,Date leaveRoomDate) throws InterruptedException{
        quarantinedInformation.setStatus(StatusOfQuarantinedInformation.status_0);
        quarantinedInformation.setQuarantinedPeople(quarantinedPeopleById);
        
        quarantinedInformation.setArrivalDate(arrivalDate);

        Integer numberOfDayQuarantined = quarantinedInformation.getLevelInfection().getNumberOfDayQuarantined();
        Calendar c = Calendar.getInstance(); 
        c.setTime(arrivalDate); 
        c.add(Calendar.DATE, numberOfDayQuarantined);
        Date departureDate = c.getTime();

        quarantinedInformation.setDepartureDate(departureDate);

        QuarantinedInformation save = repo.save(quarantinedInformation);
        if (save != null) {
            if (newRoomId != currentRoomId) {
                if (statusEditRoom == 1) {
                    repo.updateCurrentRoomQuarantinedInformation(newRoomId,quarantinedInformation.getId());
                }
                
                if (statusEditRoom == 2) {
                    repo.updateStatusRoomQuarantinedInformation(leaveRoomDate,quarantinedInformation.getId());
                    repo.addNewRoomQuarantinedInformation(newRoomId, quarantinedInformation.getId());
                }
                
            }
        }

        return save;
    }
    

    public List<QuarantinedInformation> getQuarantinedInformationByQuarantinedPeopleId(Integer quarantinedPeopleId){
        return repo.getQuarantinedInformationByQuarantinedPeopleId(quarantinedPeopleId);
    }
    
    public QuarantinedInformation getQuarantinedInformationyById(Integer id){
        boolean existsById = repo.existsById(id);
        if (existsById) {
            return repo.getById(id);
        }

        return null;
    }

    public CurrentRoomId getCurrentRoomIdByQuarantinedInformationId(Integer id){
        CurrentRoomId result = repo.getCurrentRoomIdByQuarantinedInformationId(id);
        if (result != null) {
            return result;
        }

        return null;
    }


    public Boolean checkQuarantinedInformationNotComplete(Integer quarantinedPeopleId){
        Integer checkQuarantinedInformationNotComplete = repo.checkQuarantinedInformationNotComplete(quarantinedPeopleId);
        if (checkQuarantinedInformationNotComplete == 0) {
            return true;
        }
        return false;
    }

    public Boolean checkExistsArrivalDate(String newArrivalDate,Integer quarantinedPeopleIdToCheck){
        Integer checkExistsArrivalDate = repo.checkExistsArrivalDate(newArrivalDate, quarantinedPeopleIdToCheck);
        if (checkExistsArrivalDate == 1) {
            return true;
        }
        return false;
    };
    
    public Boolean checkExistsArrivalDateWhenEdit(String newArrivalDate, String currentArrivalDate,Integer quarantinedPeopleIdToCheck){
        Integer checkExistsArrivalDateWhenEdit = repo.checkExistsArrivalDateWhenEdit(newArrivalDate,currentArrivalDate,quarantinedPeopleIdToCheck);
        if (checkExistsArrivalDateWhenEdit == 1) {
            return true;
        }
        return false;
    };

    public QuarantinedInformation getLastDepartureDateByQuarantinedPeopleId(Integer quarantinedPeopleId){
        QuarantinedInformation lastDepartureDateByQuarantinedPeopleId = repo.getLastDepartureDateByQuarantinedPeopleId(quarantinedPeopleId);
        if (lastDepartureDateByQuarantinedPeopleId != null) {
            return lastDepartureDateByQuarantinedPeopleId;
        }
        return new QuarantinedInformation();
    }

    public Boolean deleteRoomQuarantinedInformationByQuarantinedInformationId(Integer id){
        boolean existsById = repo.existsById(id);
        if (existsById) {
            Boolean checkQuarantinedInformationExistInApplicationForm = applicationFormService.checkQuarantinedInformationExistInApplicationForm(id);
            Boolean checkQuarantinedInformationExistInTest = testService.checkQuarantinedInformationExistInTest(id);
            if (!checkQuarantinedInformationExistInApplicationForm && !checkQuarantinedInformationExistInTest) {
                repo.deleteRoomQuarantinedInformationByQuarantinedInformationId(id);
                repo.deleteById(id);
                return true;
            }
        }
        return false;
    }

    public Integer getTotalQuanrantinedInformationByQuarantinedPeopleId(Integer quarantinedPeopleId){
        return repo.getTotalQuanrantinedInformationByQuarantinedPeopleId(quarantinedPeopleId);
    }

    public QuarantinedInformation getQuarantinedInformationNotCompleteByQuarantinedPeopleId(Integer quarantinedPeopleId){
        QuarantinedInformation quarantinedInformation = repo.getQuarantinedInformationNotCompleteByQuarantinedPeopleId(quarantinedPeopleId);
        if (quarantinedInformation != null) {
            return quarantinedInformation;
        }
        return new QuarantinedInformation();
    }

    public Boolean checkQuarantinedInformationNotCompleteById(Integer quarantinedInformationId){
        Integer checkQuarantinedInformationNotCompleteById = repo.checkQuarantinedInformationNotCompleteById(quarantinedInformationId);
        if (checkQuarantinedInformationNotCompleteById == 0) {
            return true;
        }
        return false;
    }

    public Integer getNumberOfDayQuarantinedByLevelInfectionId(Integer levelInfectionId){
        return repo.getNumberOfDayQuarantinedByLevelInfectionId(levelInfectionId);
    }

    public Boolean checkExistsRoomHistory(Integer id){
        Integer checkExistsRoomHistory = repo.checkExistsRoomHistory(id);
        if (checkExistsRoomHistory == 1) {
            return true;
        }
        return false;
    }
    
    public Boolean checkExistsTest(Integer id){
        Integer checkExistsTest = repo.checkExistsTest(id);
        if (checkExistsTest == 1) {
            return true;
        }
        return false;
    }
  
    public Boolean checkExistsApplicationForm(Integer id){
        Integer checkExistsApplicationForm = repo.checkExistsApplicationForm(id);
        if (checkExistsApplicationForm == 1) {
            return true;
        }
        return false;
    }

    public void updateStatusRoomQuarantinedInformation(Integer id){
        repo.updateStatusRoomQuarantinedInformation(new Date(), id);
    }
    
    public void updateStatusQuarantinedInformation(Integer status ,Integer id){
        repo.updateStatusQuarantinedInformation(status, id);
    }

    public List<Object> getAllRoomQuarantinedInformationByQuarantinedInforId(Integer quarantinedInformationId){
        return repo.getAllRoomQuarantinedInformationByQuarantinedInforId(quarantinedInformationId);
    }

    public Boolean deleteRoomQuarantinedInformationByRoomQuarantinedInformationId(Integer roomQuarantinedInformationId){
        Map<String, Object> getRoomQuarantinedInformationByRoomQuarantinedInformationId = repo.getRoomQuarantinedInformationByRoomQuarantinedInformationId(roomQuarantinedInformationId);

        if (getRoomQuarantinedInformationByRoomQuarantinedInformationId != null && !getRoomQuarantinedInformationByRoomQuarantinedInformationId.isEmpty()) {
            if (Integer.parseInt(getRoomQuarantinedInformationByRoomQuarantinedInformationId.get("status").toString()) != 0) {
                repo.deleteRoomQuarantinedInformationByRoomQuarantinedInformationId(roomQuarantinedInformationId);
                return true;
            }
        }
        return false;
    }

    public Map<String, Object> getRoomQuarantinedInformationByRoomQuarantinedInformationId(Integer roomQuarantinedInformationId){
        Map<String, Object> getRoomQuarantinedInformationByRoomQuarantinedInformationId = repo.getRoomQuarantinedInformationByRoomQuarantinedInformationId(roomQuarantinedInformationId);
        if (getRoomQuarantinedInformationByRoomQuarantinedInformationId != null && !getRoomQuarantinedInformationByRoomQuarantinedInformationId.isEmpty()) {
            return getRoomQuarantinedInformationByRoomQuarantinedInformationId;
        }

        return null;
    }

    public QuarantinedInformation completeQuarantined(QuarantinedInformation quarantinedInformation){
        quarantinedInformation.setStatus(StatusOfQuarantinedInformation.status_1);

        QuarantinedInformation save = repo.save(quarantinedInformation);
        repo.updateRoomQuarantinedInformation(1, new Date(), save.getId());

        return save;
    }

    public Integer getDataApplication(Integer status){
        return repo.getDataApplication(status);
    }
}
