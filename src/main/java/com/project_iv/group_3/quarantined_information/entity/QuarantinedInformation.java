package com.project_iv.group_3.quarantined_information.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project_iv.group_3.level_infection.entity.LevelInfection;
import com.project_iv.group_3.quarantined_people.entity.QuarantinedPeople;

@JsonIgnoreProperties({"hibernateLazyInitializer"})
@Entity
@Table(name = "quarantined_information")
public class QuarantinedInformation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "source_of_contact", nullable = false)
    private String sourceOfContact;
    
    @Column(name = "arrival_date", nullable = false)
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy",timezone = "GMT+8")
    private Date arrivalDate;

    @Column(name = "departure_date")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd/MM/yyyy",timezone = "GMT+8")
    private Date departureDate;

    @ManyToOne
    @JoinColumn(name="quarantined_people_id")
    private QuarantinedPeople quarantinedPeople;

    @Column(name = "note")
    private String note;

    @ManyToOne
    @JoinColumn(name="level_id")
    private LevelInfection levelInfection;
    
    @Column(name = "status",nullable = false)
    private Integer status;

    // @ManyToMany(cascade = CascadeType.)
    // @JoinTable(
    //         name = "room_quarantined_information",
    //         joinColumns = @JoinColumn(name = "quarantined_information_id"),
    //         inverseJoinColumns = @JoinColumn(name = "room_id")
    // )
    // @JsonIgnore
    // private List<Room> rooms = new ArrayList<Room>();



    //getters and settters
    public QuarantinedPeople getQuarantinedPeople() {
        return quarantinedPeople;
    }

    public void setQuarantinedPeople(QuarantinedPeople quarantinedPeople) {
        this.quarantinedPeople = quarantinedPeople;
    }
    
    public LevelInfection getLevelInfection() {
        return levelInfection;
    }

    public void setLevelInfection(LevelInfection levelInfection) {
        this.levelInfection = levelInfection;
    }

    // public List<Room> getRooms() {
    //     return rooms;
    // }

    // public void setRooms(List<Room> rooms) {
    //     this.rooms = rooms;
    // }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSourceOfContact() {
        return sourceOfContact;
    }

    public void setSourceOfContact(String sourceOfContact) {
        this.sourceOfContact = sourceOfContact;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
