package com.project_iv.group_3.quarantined_information.dto;

public interface CurrentRoomId {
   public Integer getRoomId();   
}
