package com.project_iv.group_3.quarantined_information.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.project_iv.group_3.area.entity.Area;
import com.project_iv.group_3.area.service.AreaService;
import com.project_iv.group_3.level_infection.entity.LevelInfection;
import com.project_iv.group_3.level_infection.service.LevelInfectionService;
import com.project_iv.group_3.quarantined_information.dto.CurrentRoomId;
import com.project_iv.group_3.quarantined_information.entity.QuarantinedInformation;
import com.project_iv.group_3.quarantined_information.service.QuarantinedInformationService;
import com.project_iv.group_3.quarantined_people.entity.QuarantinedPeople;
import com.project_iv.group_3.quarantined_people.service.QuarantinedPeopleService;
import com.project_iv.group_3.test.service.TestService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class QuarantinedInformationController {
    
    @Autowired
    private QuarantinedInformationService serviceQuarantinedInformation;
   
    @Autowired
    private QuarantinedPeopleService serviceQuarantinedPeople;

    @Autowired 
    private LevelInfectionService serviceLevelInfection;

    @Autowired
    private AreaService areaService;

    @Autowired
    private TestService testService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate; 

    public static Integer currentRoomIdWhenEdit;

    @GetMapping("/quarantined-information/getQuarantinedInformationyQuarantinedPeopleId")
    @ResponseBody
    public List<QuarantinedInformation> getQuarantinedInformationyQuarantinedPeopleId(@RequestParam Integer quarantinedPeopleId){
        return serviceQuarantinedInformation.getQuarantinedInformationByQuarantinedPeopleId(quarantinedPeopleId);
    }

    @GetMapping("/quarantined-information/getQuarantinedInformationNotCompleteByQuarantinedPeopleId")
    @ResponseBody
    public QuarantinedInformation getQuarantinedInformationNotCompleteByQuarantinedPeopleId(@RequestParam Integer quarantinedPeopleId){
        return serviceQuarantinedInformation.getQuarantinedInformationNotCompleteByQuarantinedPeopleId(quarantinedPeopleId);
    }

    @GetMapping("/quarantined-information/getQuarantinedInformationyById")
    @ResponseBody
    public QuarantinedInformation getQuarantinedInformationyById(@RequestParam Integer id){
        return serviceQuarantinedInformation.getQuarantinedInformationyById(id);
    }

    @GetMapping("/quarantined-information/checkQuarantinedInformationNotComplete")
    @ResponseBody
    public Boolean checkQuarantinedInformationNotComplete(@RequestParam Integer quarantinedPeopleId){
        return serviceQuarantinedInformation.checkQuarantinedInformationNotComplete(quarantinedPeopleId);
    }

    @GetMapping("/quarantined-information/checkQuarantinedInformationNotCompleteById")
    @ResponseBody
    public Boolean checkQuarantinedInformationNotCompleteById(@RequestParam Integer quarantinedInformationId){
        return serviceQuarantinedInformation.checkQuarantinedInformationNotCompleteById(quarantinedInformationId);
    }

    @GetMapping("/quarantined-information/create-quarantined-information/{quarantinedPeopleId}")
    public String createPage(@PathVariable Integer quarantinedPeopleId, Model model){
        QuarantinedPeople getQuarantinedPeopleById = serviceQuarantinedPeople.getById(quarantinedPeopleId);
        if (getQuarantinedPeopleById != null) {
            List<Area> listAreas = areaService.getAllAreasExcludeAreaNotExistInRoomByUserId();
            Boolean checkQuarantinedInformationNotComplete = serviceQuarantinedInformation.checkQuarantinedInformationNotComplete(quarantinedPeopleId);
            if (checkQuarantinedInformationNotComplete) {
                if (listAreas.size() > 0) {
                    List<LevelInfection> allLevelInfection = serviceLevelInfection.getAllLevelInfection();
                    model.addAttribute("listAreas", listAreas);
                    model.addAttribute("listLevels", allLevelInfection);
    
                    model.addAttribute("quarantinedPeopleId", quarantinedPeopleId);
                    model.addAttribute("quarantinedInformation", new QuarantinedInformation());
    
                    return "dashboard/quarantined_information/create";
                }else{
                    return "redirect:/rooms/create-room";
                }
            }else{
                return "redirect:/quarantined-people/detail-quarantined-people/"+quarantinedPeopleId;
            }
        }
        return "redirect:/error/404.html";
    }

    @PostMapping("/quarantined-information/doCreateQuarantinedInformation")
    public String doCreatePage(Integer quarantinedPeopleId,HttpServletRequest request,QuarantinedInformation quarantinedInformation) throws ParseException{
        QuarantinedPeople getQuarantinedPeopleById = serviceQuarantinedPeople.getById(quarantinedPeopleId);
        if (getQuarantinedPeopleById != null) {
            Boolean checkQuarantinedInformationNotComplete = serviceQuarantinedInformation.checkQuarantinedInformationNotComplete(quarantinedPeopleId);
            if (checkQuarantinedInformationNotComplete) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date arrivalDate = simpleDateFormat.parse(request.getParameter("arrivalDateTemp").toString());
                Integer newRoomId = Integer.parseInt(request.getParameter("room").toString());
                
                QuarantinedPeople quarantinedPeopleById = serviceQuarantinedPeople.getById(quarantinedPeopleId);
                QuarantinedInformation addNewQuarantinedInformation = serviceQuarantinedInformation.addNewQuarantinedInformation(quarantinedInformation,quarantinedPeopleById,arrivalDate,newRoomId);
                
                if (addNewQuarantinedInformation != null) {
                    this.simpMessagingTemplate.convertAndSend("/topic/getQuarantinedInformation/"+quarantinedPeopleId,"created");
                    this.simpMessagingTemplate.convertAndSend("/topic/getRoom","created quarantined information");
                    return"redirect:/quarantined-people/detail-quarantined-people/"+quarantinedPeopleId;
                }
            }
        }

        return "redirect:/error/404.html";
        
    }

    @GetMapping("/quarantined-information/edit-quarantined-information/{quarantinedInformationId}")
    public String showEditPage(@PathVariable Integer quarantinedInformationId, Model model){

        QuarantinedInformation quarantinedInformationyById = serviceQuarantinedInformation.getQuarantinedInformationyById(quarantinedInformationId);
        if (quarantinedInformationyById != null) {
        Boolean checkUserHasAuthorAccessDetail = serviceQuarantinedPeople.checkUserHasAuthorAccessDetail(quarantinedInformationyById.getQuarantinedPeople().getId());
        
            if (checkUserHasAuthorAccessDetail) {
                if (quarantinedInformationyById.getStatus() == 0) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    String currentArrivalDate = simpleDateFormat.format(quarantinedInformationyById.getArrivalDate());
                    String currentDepartureDate = simpleDateFormat.format(quarantinedInformationyById.getDepartureDate());

                    List<LevelInfection> listLevels = serviceLevelInfection.getAllLevelInfection();
                    
                    CurrentRoomId currentRoomId = serviceQuarantinedInformation.getCurrentRoomIdByQuarantinedInformationId(quarantinedInformationyById.getId());

                    model.addAttribute("listLevels", listLevels);
                    model.addAttribute("quarantinedInformation", quarantinedInformationyById);
                    model.addAttribute("quarantinedPeopleId", quarantinedInformationyById.getQuarantinedPeople().getId());
                    model.addAttribute("currentArrivalDate", currentArrivalDate);
                    model.addAttribute("currentDepartureDate", currentDepartureDate);
                    model.addAttribute("currentRoomId", currentRoomId);
                    model.addAttribute("currentLevelInfectionId", quarantinedInformationyById.getLevelInfection().getId());
                    model.addAttribute("currentLevelInfectionName", quarantinedInformationyById.getLevelInfection().getLevel());

                    currentRoomIdWhenEdit = currentRoomId.getRoomId();

                    return "dashboard/quarantined_information/edit";
                }else{
                    return "redirect:/error/404.html";
                }
            }else{
                return "redirect:/error/403.html";
            }
        }
            return "redirect:/error/404.html";

    }

    @PostMapping("/quarantined-information/doEditQuarantinedInformation")
    public String doEditPage(Integer id,Integer quarantinedPeopleId, QuarantinedInformation quarantinedInformation,HttpServletRequest request) throws ParseException, InterruptedException {
        QuarantinedInformation quarantinedInformationyById = serviceQuarantinedInformation.getQuarantinedInformationyById(id);
        
        if (quarantinedInformationyById != null) {
            Boolean checkUserHasAuthorAccessDetail = serviceQuarantinedPeople.checkUserHasAuthorAccessDetail(quarantinedInformationyById.getQuarantinedPeople().getId());
                if (checkUserHasAuthorAccessDetail) {
                   
                if (quarantinedInformationyById.getStatus() == 0) {
                    Boolean checkExistsTest = serviceQuarantinedInformation.checkExistsTest(id);
                    Boolean checkExistsApplicationForm = serviceQuarantinedInformation.checkExistsApplicationForm(id);
                    Boolean checkExistsRoomHistory = serviceQuarantinedInformation.checkExistsRoomHistory(id);

                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    Date arrivalDate = null;

                    if (checkExistsRoomHistory || checkExistsApplicationForm || checkExistsTest) {
                        arrivalDate = quarantinedInformationyById.getArrivalDate();
                        quarantinedInformation.setLevelInfection(quarantinedInformationyById.getLevelInfection());
                    }else{
                        arrivalDate = simpleDateFormat.parse(request.getParameter("arrivalDateTemp").toString());
                    }

                    Date leaveRoomDate = null;
                    if (request.getParameter("leaveDateTemp") != null) {
                        leaveRoomDate = simpleDateFormat.parse(request.getParameter("leaveDateTemp").toString());
                    }
                    Integer newRoomId = Integer.parseInt(request.getParameter("room").toString());
                    Integer statusEditRoom = Integer.parseInt(request.getParameter("statusEditRoom").toString());
                
                        QuarantinedPeople quarantinedPeopleById = serviceQuarantinedPeople.getById(quarantinedPeopleId);
                
                        QuarantinedInformation editQuarantinedInformation = serviceQuarantinedInformation.editQuarantinedInformation(quarantinedInformation, quarantinedPeopleById, arrivalDate,statusEditRoom,newRoomId,currentRoomIdWhenEdit,leaveRoomDate);
                        if (editQuarantinedInformation != null) {
                            this.simpMessagingTemplate.convertAndSend("/topic/getQuarantinedInformation/"+quarantinedPeopleId,"edited");
                            if (statusEditRoom == 1 || statusEditRoom == 2) {
                                this.simpMessagingTemplate.convertAndSend("/topic/getRoom","edited quarantined information");
                                this.simpMessagingTemplate.convertAndSend("/topic/getRoomQuarantinedInformation/"+id,"edited room quarantined information");
                            }
                            return "redirect:/quarantined-people/detail-quarantined-people/"+editQuarantinedInformation.getQuarantinedPeople().getId();
                        }   
                }
            }
            return "redirect:/error/403.html";
        }
        
        return "redirect:/error/404.html";
    }

    @GetMapping("/quarantined-information/checkExistsArrivalDate")
    @ResponseBody
    public Boolean checkExistsArrivalDate(@RequestParam String newArrivalDate,@RequestParam Integer quarantinedPeopleIdToCheck){
        return serviceQuarantinedInformation.checkExistsArrivalDate(newArrivalDate, quarantinedPeopleIdToCheck);
    }
   
    @GetMapping("/quarantined-information/checkExistsArrivalDateWhenEdit")
    @ResponseBody
    public Boolean checkExistsArrivalDateWhenEdit(@RequestParam String newArrivalDate,@RequestParam String currentArrivalDate,@RequestParam Integer quarantinedPeopleIdToCheck){
        return serviceQuarantinedInformation.checkExistsArrivalDateWhenEdit(newArrivalDate,currentArrivalDate,quarantinedPeopleIdToCheck);
    }

    @GetMapping("/quarantined-information/getLastDepartureDateByQuarantinedPeopleId")
    @ResponseBody
    public QuarantinedInformation getLastDepartureDateByQuarantinedPeopleId(@RequestParam Integer quarantinedPeopleId){
       return serviceQuarantinedInformation.getLastDepartureDateByQuarantinedPeopleId(quarantinedPeopleId);
    }

    @GetMapping("/quarantined-information/deleteQuanrantinedInformationById")
    @ResponseBody
    public Boolean deleteQuanrantinedInformationById(@RequestParam Integer id,@RequestParam Integer quarantinedPeopleId){
        QuarantinedInformation quarantinedInformationyById = serviceQuarantinedInformation.getQuarantinedInformationyById(id);
        Boolean deleteQuanrantinedInformationById = serviceQuarantinedInformation.deleteRoomQuarantinedInformationByQuarantinedInformationId(id);
        if (deleteQuanrantinedInformationById) {
            if (quarantinedInformationyById.getStatus() == 0) {
                this.simpMessagingTemplate.convertAndSend("/topic/getRoom","deleted quarantined information");
            }
            this.simpMessagingTemplate.convertAndSend("/topic/getQuarantinedInformation/"+quarantinedPeopleId,"deleted");
        }
        return deleteQuanrantinedInformationById;
    }
   
    @GetMapping("/quarantined-information/getTotalQuanrantinedInformationByQuarantinedPeopleId")
    @ResponseBody
    public Integer getTotalQuanrantinedInformationByQuarantinedPeopleId(@RequestParam Integer quarantinedPeopleId){
        return serviceQuarantinedInformation.getTotalQuanrantinedInformationByQuarantinedPeopleId(quarantinedPeopleId);
    }
   
    @GetMapping("/quarantined-information/getNumberOfDayQuarantinedByLevelInfectionId")
    @ResponseBody
    public Integer getNumberOfDayQuarantinedByLevelInfectionId(@RequestParam Integer levelInfectionId){
        return serviceQuarantinedInformation.getNumberOfDayQuarantinedByLevelInfectionId(levelInfectionId);
    }
    
    @GetMapping("/quarantined-information/checkExistsRoomHistory")
    @ResponseBody
    public Boolean checkExistsRoomHistory(@RequestParam Integer id){
        return serviceQuarantinedInformation.checkExistsRoomHistory(id);
    }
    
    @GetMapping("/quarantined-information/checkExistsTest")
    @ResponseBody
    public Boolean checkExistsTest(@RequestParam Integer id){
        return serviceQuarantinedInformation.checkExistsTest(id);
    }
    
    @GetMapping("/quarantined-information/checkExistsApplicationForm")
    @ResponseBody
    public Boolean checkExistsApplicationForm(@RequestParam Integer id){
        return serviceQuarantinedInformation.checkExistsApplicationForm(id);
    }

    @GetMapping("/quarantined-information/detail-quarantined-information/{quarantinedInformationId}")
    public String detailQuarantinedInformation(@PathVariable Integer quarantinedInformationId, Model model){
        QuarantinedInformation quarantinedInformationyById = serviceQuarantinedInformation.getQuarantinedInformationyById(quarantinedInformationId);
        if (quarantinedInformationyById != null) {
            Boolean checkUserHasAuthorAccessDetail = serviceQuarantinedPeople.checkUserHasAuthorAccessDetail(quarantinedInformationyById.getQuarantinedPeople().getId());
            if (checkUserHasAuthorAccessDetail) {
                model.addAttribute("id", quarantinedInformationId);
                model.addAttribute("qPeopleId", quarantinedInformationyById.getQuarantinedPeople().getId());
                return "dashboard/quarantined_information/detail";
            }
        }
        return "redirect:/error/403.html";
    }

    @GetMapping("/quarantined-information/getAllRoomQuarantinedInformationByQuarantinedInforId")
    @ResponseBody 
    public List<Object> getAllRoomQuarantinedInformationByQuarantinedInforId(@RequestParam Integer quarantinedInformationId){
        return serviceQuarantinedInformation.getAllRoomQuarantinedInformationByQuarantinedInforId(quarantinedInformationId);
    }

    @GetMapping("/quarantined-information/deleteRoomQuarantinedInformationByRoomQuarantinedInformationId")
    @ResponseBody 
    public Boolean deleteRoomQuarantinedInformationByRoomQuarantinedInformationId(@RequestParam Integer roomQuarantinedInformationId){
        Map<String, Object> roomQuarantinedInformationByRoomQuarantinedInformationId = serviceQuarantinedInformation.getRoomQuarantinedInformationByRoomQuarantinedInformationId(roomQuarantinedInformationId);
       if (roomQuarantinedInformationByRoomQuarantinedInformationId != null && !roomQuarantinedInformationByRoomQuarantinedInformationId.isEmpty()) {
            int quarantinedInformationId = Integer.parseInt(roomQuarantinedInformationByRoomQuarantinedInformationId.get("quarantined_information_id").toString());
            this.simpMessagingTemplate.convertAndSend("/topic/getRoomQuarantinedInformation/"+quarantinedInformationId,"deleted room quarantined information");

            return serviceQuarantinedInformation.deleteRoomQuarantinedInformationByRoomQuarantinedInformationId(roomQuarantinedInformationId);
       }

       return false;
    }

    @GetMapping("/quarantined-information/completeQuarantined")
    @ResponseBody
    public Boolean completeQuarantined(@RequestParam Integer quarantinedInformationId){
        QuarantinedInformation quarantinedInformationyById = serviceQuarantinedInformation.getQuarantinedInformationyById(quarantinedInformationId);
        if (quarantinedInformationyById != null) {
            if (quarantinedInformationyById.getStatus() == 0) {
                Boolean checkUserHasAuthorAccessDetail = serviceQuarantinedPeople.checkUserHasAuthorAccessDetail(quarantinedInformationyById.getQuarantinedPeople().getId());
                if (checkUserHasAuthorAccessDetail) {
                    Integer checkDepartureDate = quarantinedInformationyById.getDepartureDate().compareTo(new Date());
                    if (checkDepartureDate <= 0) {
                        Integer checkTestResult = testService.getTotalNegativeResultByQuarantinedInformationId(quarantinedInformationId);
                        if (checkTestResult == 2) {
                            QuarantinedInformation completeQuarantined = serviceQuarantinedInformation.completeQuarantined(quarantinedInformationyById);
                            if (completeQuarantined != null) {
                                return true;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    @GetMapping("/quarantined-information/getDataApplication")
    @ResponseBody
    public Integer getDataApplication(@RequestParam Integer status){
        return serviceQuarantinedInformation.getDataApplication(status);
    }
}
