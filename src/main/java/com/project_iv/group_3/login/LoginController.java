package com.project_iv.group_3.login;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.project_iv.group_3.user.entity.User;
import com.project_iv.group_3.user.service.EmailService;
import com.project_iv.group_3.user.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import net.bytebuddy.utility.RandomString;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate; 
    
    @GetMapping(value="/login")
    public String loginError(HttpServletRequest request, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            HttpSession session = request.getSession(false);
            String errorMessage = null;
            if (session != null) {
                AuthenticationException ex = (AuthenticationException) session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
                if (ex != null) {
                    errorMessage = "Tên đăng nhập hoặc mật khẩu không đúng";
                    session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
                }
            }
            model.addAttribute("errorMessage", errorMessage);
            return "/login/login";
        }else{
            return "redirect:/";
            }
    }

    @GetMapping(value = "/change-password-first-time/{passwordToken}")
    public String changePasswordFirstTime(@PathVariable(required = true) String passwordToken,HttpServletRequest request,HttpServletResponse response,Model model) throws ServletException{
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (passwordToken == null) {
                return "redirect:/error/404.html";
            }else{
                User findByPasswordToken = userService.findByPasswordToken(passwordToken);
                if (findByPasswordToken != null) {
                    model.addAttribute("passwordResetToken", passwordToken);
                    if (authentication.isAuthenticated() && authentication instanceof AnonymousAuthenticationToken == false) {
                        request.logout();
                    }
                    return "login/change_password_first_time/change_password_first_time";
                }else{
                    return "redirect:/error/404.html";
                }
            }
        }
        
        @PostMapping("/change-password-first-time/doChangePasswordFirstTime")
        public RedirectView doChangePasswordFirstTime(HttpServletRequest request, RedirectAttributes attributes){
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String passwordResetToken = request.getParameter("passwordResetToken");
            String newPassword = request.getParameter("newPassword");
            String encodedNewPassword = encoder.encode(newPassword);

            User updatePasswordFirstLogin = userService.updatePasswordFirstLogin(passwordResetToken, encodedNewPassword);
            if (updatePasswordFirstLogin != null) {
                RedirectView redirectView = new RedirectView("/login",true);
                attributes.addFlashAttribute("infoMessage", "Xin mời đăng nhập lại hệ thống với mật khẩu mới");
                return redirectView;
            }else{
                RedirectView redirectView = new RedirectView("/change-password-first-time/"+passwordResetToken,true);
                return redirectView;
            }
        }

        @GetMapping("/forgot-password")
        public String forgotPasswordPage(){
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication.isAuthenticated() && authentication instanceof AnonymousAuthenticationToken == false && authentication != null) {
                return "redirect:/error/404.html";
            }else{
                return "/login/forgot_password/forgot_password";
            }
        }

        @PostMapping("/forgot-password/doForgotPassword")
        public RedirectView doForgotPassword(String email,RedirectAttributes attributes,HttpServletRequest request) throws UnsupportedEncodingException, MessagingException, MalformedURLException{
            String passwordResetToken = RandomString.make(30);

            String urlToResetPassword = "";
            URL url = new URL(request.getRequestURL().toString());
            String host  = url.getHost();
            String scheme = url.getProtocol();
            Integer port  = url.getPort();
            
            if (!email.trim().isEmpty() && email != null) {
                User updatePasswordResetTokenByEmail = userService.updatePasswordResetTokenByEmail(email, passwordResetToken);
                if (updatePasswordResetTokenByEmail != null) {
                    urlToResetPassword = scheme+"://"+host+":"+port+"/reset-password/"+passwordResetToken;
                    emailService.sendEmailResetPassword(email, urlToResetPassword);

                    RedirectView redirectView = new RedirectView("/forgot-password",true);
                    attributes.addFlashAttribute("infoMessage", "Tin nhắn sẽ được gửi đến email : <br>"+"<b>"+email+"</b>");
                    return redirectView;
                }
            }
            RedirectView redirectView = new RedirectView("/forgot-password",true);
            return redirectView;
        }

        @GetMapping(value = "/reset-password/{passwordToken}")
        public String resetPassword(@PathVariable(required = true) String passwordToken,HttpServletRequest request,HttpServletResponse response,Model model) throws ServletException{
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication.isAuthenticated() && authentication instanceof AnonymousAuthenticationToken == false && authentication != null) {
                return "redirect:/error/404.html";
            }else{
                if (passwordToken == null) {
                    return "redirect:/error/404.html";
                }else{
                    User findByPasswordToken = userService.findByPasswordToken(passwordToken);
                    if (findByPasswordToken != null) {
                        model.addAttribute("passwordResetToken", passwordToken);
                        return "login/reset_password/reset_password";
                    }else{
                        return "redirect:/error/404.html";
                    }
                }
            }    
        }

        @PostMapping("/reset-password/doResetPassword")
        public RedirectView doResetPassword(HttpServletRequest request, RedirectAttributes attributes){
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String passwordResetToken = request.getParameter("passwordResetToken");
            String newPassword = request.getParameter("newPassword");
            String encodedNewPassword = encoder.encode(newPassword);

            User updatePasswordFirstLogin = userService.updatePasswordFirstLogin(passwordResetToken, encodedNewPassword);
            if (updatePasswordFirstLogin != null) {
                this.simpMessagingTemplate.convertAndSend("/topic/forceLogout/"+updatePasswordFirstLogin.getUsername(),"logout");
                RedirectView redirectView = new RedirectView("/login",true);
                attributes.addFlashAttribute("infoMessage", "Xin mời đăng nhập lại hệ thống với mật khẩu mới");
                return redirectView;
            }
                RedirectView redirectView = new RedirectView("/change-password-first-time/"+passwordResetToken,true);
                return redirectView;
        }
}
