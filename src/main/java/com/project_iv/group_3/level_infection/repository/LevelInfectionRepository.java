package com.project_iv.group_3.level_infection.repository;

import java.util.List;

import com.project_iv.group_3.level_infection.entity.LevelInfection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LevelInfectionRepository extends JpaRepository<LevelInfection,Integer>{

    @Query(nativeQuery = true, value = "select * from level_infection where id <= ?1")
    public List<LevelInfection> getLevelInfectionLessThanId(Integer id);
    
}
