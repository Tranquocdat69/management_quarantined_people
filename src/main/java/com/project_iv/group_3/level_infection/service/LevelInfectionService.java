package com.project_iv.group_3.level_infection.service;

import java.util.List;

import com.project_iv.group_3.level_infection.entity.LevelInfection;
import com.project_iv.group_3.level_infection.repository.LevelInfectionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LevelInfectionService {

    @Autowired 
    private LevelInfectionRepository repository;
    
    public List<LevelInfection> getAllLevelInfection(){
       return repository.findAll();
    }

    public List<LevelInfection> getLevelInfectionLessThanId(Integer id){
        return repository.getLevelInfectionLessThanId(id);
    };

}
