package com.project_iv.group_3.level_infection.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "level_infection")
public class LevelInfection {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "level", nullable = false)
    private String level;

    @Column(name = "number_of_day_quarantined", nullable = false)
    private Integer numberOfDayQuarantined;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getNumberOfDayQuarantined() {
        return numberOfDayQuarantined;
    }

    public void setNumberOfDayQuarantined(Integer numberOfDayQuarantined) {
        this.numberOfDayQuarantined = numberOfDayQuarantined;
    }

}
