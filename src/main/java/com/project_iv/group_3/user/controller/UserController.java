package com.project_iv.group_3.user.controller;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.project_iv.group_3.area.entity.Area;
import com.project_iv.group_3.area.service.AreaService;
import com.project_iv.group_3.common.model.MyUploadFile;
import com.project_iv.group_3.common.service.MyUploadFileService;
import com.project_iv.group_3.role.entity.Role;
import com.project_iv.group_3.role.service.RoleService;
import com.project_iv.group_3.user.dto.UserDTO;
import com.project_iv.group_3.user.entity.User;
import com.project_iv.group_3.user.service.EmailService;
import com.project_iv.group_3.user.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import net.bytebuddy.utility.RandomString;

@Controller
public class UserController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private EmailService emailService;

    @Autowired
    private AreaService areaService;
    
    @Autowired
    private MyUploadFileService uploadFileService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate; 

    private static Integer adminUpdateUserId;

    private static String adminUpdateUsername;


    private static List<Area> currentListAreas = new ArrayList<Area>();

    private static List<Role> currentListRoles = new ArrayList<Role>();
    // @Autowired
    // private QuarantinedPeoplelService quarantinedPeoplelService;
    
    @Autowired
    private RoleService roleService;
    
    @GetMapping("/users")
    public String index(){
        return "dashboard/user/index";
    }
    
    @GetMapping("/current-user/getCurrentLoggedUser")
    @ResponseBody
    public User getCurrentLoggedUser(Principal principal){
        User user = (User) userService.findByUsername(principal.getName());
        if (user != null) {
            return user;
        }else{
            return new User();
        }
    }
    
    @GetMapping("/users/getAllUsersExcludeAdmin")
    @ResponseBody
    public List<UserDTO> getAllUsersExcludeAdmin(){
        List<UserDTO> allUsers = userService.getAllUsersExcludeAdmin();
        return allUsers;
    }

    @GetMapping("/users/getByUsername")
    @ResponseBody
    public User getByUsername(@RequestParam String username){
        User user = (User) userService.findByUsername(username);
        if (user != null) {
            return user;
        }else{
            return new User();
        }
    }
    
    @GetMapping("/users/getByEmail")
    @ResponseBody
    public User getByEmail(@RequestParam String email){
        User user = (User) userService.findByEmail(email);
        if (user != null) {
            return user;
        }else{
            return new User();
        }
    }

    @GetMapping("/forgot-password/getByEmail")
    @ResponseBody
    public Boolean checkEmailExist(@RequestParam String email){
        User user = (User) userService.findByEmail(email);
        if (user != null) {
            return true;
        }else{
            return false;
        }
    }

    @GetMapping("/users/getByPhoneNumber")
    @ResponseBody
    public User getByPhoneNumber(@RequestParam String phoneNumber){
        User user = (User) userService.findByPhoneNumber(phoneNumber);
        if (user != null) {
            return user;
        }else{
            return new User();
        }
    }
    
    @GetMapping("/users/checkDuplicateUsername")
    @ResponseBody
    public User checkDuplicateUsername(@RequestParam String currentUsername, @RequestParam String newUsername){
        User user = userService.checkDuplicateUsername(currentUsername, newUsername);
        if (user != null) {
            return user;
        }else{
            return new User();
        }
    }
    
    @GetMapping("/users/checkDuplicateEmail")
    @ResponseBody
    public User checkDuplicateEmail(@RequestParam String currentEmail, @RequestParam String newEmail){
        User user = userService.checkDuplicateEmail(currentEmail, newEmail);
        if (user != null) {
            return user;
        }else{
            return new User();
        }
    }
    
    @GetMapping("/users/checkDuplicatePhoneNumber")
    @ResponseBody
    public User checkDuplicatePhoneNumber(@RequestParam String currentPhoneNumber, @RequestParam String newPhoneNumber){
        User user = userService.checkDuplicatePhoneNumber(currentPhoneNumber, newPhoneNumber);
        if (user != null) {
            return user;
        }else{
            return new User();
        }
    }
   
    @GetMapping("/current-user/checkDuplicatePhoneNumber")
    @ResponseBody
    public User checkProfileDuplicatePhoneNumber(@RequestParam String currentPhoneNumber, @RequestParam String newPhoneNumber){
        User user = userService.checkDuplicatePhoneNumber(currentPhoneNumber, newPhoneNumber);
        if (user != null) {
            return user;
        }else{
            return new User();
        }
    }

    @GetMapping("/users/create-user")
    public String showFormCreate(Model model){
        List<Area> listAllAreas = areaService.listAll();
        if (listAllAreas.size() > 0) {
            model.addAttribute("user", new User());
            model.addAttribute("myUploadFile", new MyUploadFile());
            model.addAttribute("listRoles", roleService.getAllRolesExcludeAdmin());
            model.addAttribute("listAreas", areaService.listAll());
            return "dashboard/user/create";
        }
        return "redirect:/areas/create-area";
    }

    @PostMapping("/users/add-user")
    public String addUser(User user, MyUploadFile myUploadFile) throws UnsupportedEncodingException, MessagingException, InterruptedException{
        
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String rawTempPass = RandomString.make(6);
        String passwordResetToken = RandomString.make(30);
        String email = user.getEmail();
        String encodedPass = encoder.encode(rawTempPass);

        emailService.sendEmail(email, rawTempPass);

        try {
            MultipartFile multipartFile = myUploadFile.getMultipartFile();
            if (multipartFile != null) {
                String tempFileName = multipartFile.getOriginalFilename();
                if (tempFileName != "") {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                    String extension = tempFileName.substring(tempFileName.indexOf(".")+1).toLowerCase();
                    String name = tempFileName.substring(0, tempFileName.lastIndexOf("."));
                    String fileName = name + timeStamp + "."+extension;
                    File file = new File(uploadFileService.getFolderUpload(), fileName);

                    multipartFile.transferTo(file);
                    Thread.sleep(200);
                    user.setAvatar(fileName);
                }
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
        User addNewUser = userService.addNewUser(user,encodedPass,passwordResetToken);

        if (addNewUser != null) {
            Thread.sleep(800);
            this.simpMessagingTemplate.convertAndSend("/topic/getUser","added");
            return "redirect:/users";
        }else{
            return "dashboard/user/create";
        }
    }

    @GetMapping("/users/isCorrectPassword")
    @ResponseBody
    public Boolean isCorrectChangePasswordFirstTime(@RequestParam String passwordResetToken, @RequestParam String password){
        boolean isMatch = false;
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        User findByPasswordToken = userService.findByPasswordToken(passwordResetToken);
        isMatch = encoder.matches(password, findByPasswordToken.getPassword());

        return isMatch;
    }
    
    @GetMapping("/current-user/isCorrectPassword")
    @ResponseBody
    public Boolean isCorrectChangePassword(Principal principal,@RequestParam String currentPassword){
        boolean isMatch = false;
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        User findById = userService.findByUsername(principal.getName());
        isMatch = encoder.matches(currentPassword, findById.getPassword());

        return isMatch;
    }

    @GetMapping("/users/detail/{id}")
    public String getDetailUser(@PathVariable Integer id, Model model){
        User findById = userService.findById(id);
        if (findById != null) {
            model.addAttribute("user", findById);
            model.addAttribute("currentUsername", findById.getUsername());
            model.addAttribute("currentEmail", findById.getEmail());
            model.addAttribute("currentPhoneNumber", findById.getPhoneNumber());
    
            model.addAttribute("myUploadFile", new MyUploadFile());
            model.addAttribute("listRoles", roleService.getAllRolesExcludeAdmin());
            model.addAttribute("listAreas", areaService.listAll());
    
            adminUpdateUserId = findById.getId();
            adminUpdateUsername = findById.getUsername();
            currentListAreas = findById.getAreas();
            currentListRoles = findById.getRoles();

            return "dashboard/user/detail";
        }
        return "redirect:/error/404.html";
    }
    
    @PostMapping("/users/edit-user")
    public String editUser(Integer id,User user, MyUploadFile myUploadFile,HttpServletRequest request,Principal principal) throws InterruptedException{
        User findById = userService.findById(id);
        if (findById != null) {
            if (id == adminUpdateUserId) {
                try {
                    MultipartFile multipartFile = myUploadFile.getMultipartFile();
                    if (multipartFile != null) {
                        String tempFileName = multipartFile.getOriginalFilename();
                        if (tempFileName != "") {
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                            String extension = tempFileName.substring(tempFileName.indexOf(".")+1).toLowerCase();
                            String name = tempFileName.substring(0, tempFileName.lastIndexOf("."));
                            String fileName = name + timeStamp + "."+extension;
                            File file = new File(uploadFileService.getFolderUpload(), fileName);
                            String currentAvatar = request.getParameter("nameOfAvatar");
        
                            if (currentAvatar != null && !currentAvatar.trim().isEmpty()) {
                                uploadFileService.deleteUploadedFile(currentAvatar);
                            }
        
                            multipartFile.transferTo(file);
                            Thread.sleep(200);
                            user.setAvatar(fileName);
                        }
                    }
                  } catch (Exception e) {
                    e.printStackTrace();
                  }
        
                User editUser = userService.editUserByAdmin(id,user.getFullname(),user.getUsername(),user.getEmail(),user.getAvatar() != null ? user.getAvatar() : "",user.getPhoneNumber(), user.getRoles(),user.getAreas());
                if (editUser != null) {
                    String usernameEditUser = editUser.getUsername();
                    Thread.sleep(800);
                    this.simpMessagingTemplate.convertAndSend("/topic/getUser","edited");
                    this.simpMessagingTemplate.convertAndSend("/topic/getUser/"+editUser.getUsername(),"edited "+editUser.getUsername());
    
                    if (!adminUpdateUsername.equals(usernameEditUser)) {
                        this.simpMessagingTemplate.convertAndSend("/topic/forceLogout/"+adminUpdateUsername,"logout");
                    }
                    List<Area> areas = editUser.getAreas();
                    if (areas.size() != currentListAreas.size() && !currentListAreas.containsAll(areas)) {
                        this.simpMessagingTemplate.convertAndSend("/topic/forceLogout/"+adminUpdateUsername,"logout");
                    }
                    
                    List<Role> roles = editUser.getRoles();
                    if (roles.size() != currentListRoles.size() && !currentListRoles.containsAll(roles)) {
                        this.simpMessagingTemplate.convertAndSend("/topic/forceLogout/"+adminUpdateUsername,"logout");
                    }
                    
                    return "redirect:/users";
                }else{
                    return "redirect:/users/detail/"+user.getId();
                }
            }
        }
        
        return "redirect:/error/404.html";
    }

    @GetMapping("/users/searchUsersExcludeAdmin")
    @ResponseBody
    public List<UserDTO> searchUsersExcludeAdmin(@RequestParam String keyword){
        List<UserDTO> searchUsersExcludeAdmin = userService.searchUsersExcludeAdmin(keyword);
        return searchUsersExcludeAdmin;
    }

    @GetMapping("/users/deleteUser")
    @ResponseBody
    public Boolean deleteUser(@RequestParam Integer id){
        User findById = userService.findById(id);
        Boolean isDelete = userService.deleteUser(id);
        if (isDelete) {
            this.simpMessagingTemplate.convertAndSend("/topic/forceLogout/"+findById.getUsername(),"logout");
            this.simpMessagingTemplate.convertAndSend("/topic/getUser","deleted");
        }
        return isDelete;
    }

    @GetMapping("/current-user/profile")
    public String profilePage(Principal principal,Model model) {
        model.addAttribute("user", userService.findByUsername(principal.getName()));
        model.addAttribute("myUploadFile", new MyUploadFile());
        return "dashboard/user/profile";
    }

    @PostMapping("/current-user/edit-profile")
    public String editProfilePage(Integer id,User user,MyUploadFile myUploadFile,HttpServletRequest request,Principal principal) throws InterruptedException{
        User findByUsername = userService.findByUsername(principal.getName());
        if (id == findByUsername.getId()) {
            try {
                MultipartFile multipartFile = myUploadFile.getMultipartFile();
                if (multipartFile != null) {
                    String tempFileName = multipartFile.getOriginalFilename();
                    if (tempFileName != "") {
                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
                        String extension = tempFileName.substring(tempFileName.indexOf(".")+1).toLowerCase();
                        String name = tempFileName.substring(0, tempFileName.lastIndexOf("."));
                        String fileName = name + timeStamp + "."+extension;
                        File file = new File(uploadFileService.getFolderUpload(), fileName);
    
                        String currentAvatar = request.getParameter("currentAvatar");
                        if (currentAvatar != null && !currentAvatar.trim().isEmpty()) {
                            uploadFileService.deleteUploadedFile(currentAvatar);
                        }
                        
                        multipartFile.transferTo(file);
                        Thread.sleep(200);
                        user.setAvatar(fileName);
                    }
                }
              } catch (Exception e) {
                e.printStackTrace();
              }
    
              User editProfileUser = userService.editProfileUser(id, user.getFullname(), user.getAvatar() != null ? user.getAvatar() : "", user.getPhoneNumber());
              
              if (editProfileUser != null) {
                  Thread.sleep(800);
                  this.simpMessagingTemplate.convertAndSend("/topic/getUser","edited");
                  this.simpMessagingTemplate.convertAndSend("/topic/getUser/"+editProfileUser.getUsername(),"edited "+editProfileUser.getUsername());
                  return "redirect:/current-user/profile";
                }
            }
            return "redirect:/error/404.html";
        
    }
    
    @GetMapping("/current-user/change-password")
    public String changePasswordPage(Principal principal, Model model){
        model.addAttribute("user", userService.findByUsername(principal.getName()));
        return "dashboard/user/change-password";
    }

    @PostMapping("/current-user/doChangePassword")
    public RedirectView doChangePassword(Integer id,HttpServletRequest request, RedirectAttributes attributes) throws ServletException{
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String newPassword = request.getParameter("newPassword");
        String encodedNewPassword = encoder.encode(newPassword);

        User userUpdatePassword = userService.updatePassword(id, encodedNewPassword);
        if (userUpdatePassword != null) {
            this.simpMessagingTemplate.convertAndSend("/topic/forceLogout/"+userUpdatePassword.getUsername(),"logout");
            request.logout();
            RedirectView redirectView = new RedirectView("/login",true);
            attributes.addFlashAttribute("infoMessage", "Xin mời đăng nhập lại hệ thống với mật khẩu mới");
            return redirectView;
        }

        RedirectView redirectView = new RedirectView("/current-user/change-password",true);
        return redirectView;
    }

}
