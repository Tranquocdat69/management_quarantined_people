package com.project_iv.group_3.user.dto;

public interface UserDTO {

    public String getId();
    public String getUserName();
    public String getEmail();
    public String getFullname();
    public String getPhoneNumber();
    public String getAvatar();
    

}
