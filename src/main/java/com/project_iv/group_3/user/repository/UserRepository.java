package com.project_iv.group_3.user.repository;

import java.util.List;

import com.project_iv.group_3.user.dto.UserDTO;
import com.project_iv.group_3.user.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<User,Integer>{
    
    @Query("SELECT u FROM User u WHERE u.email = ?1")
    public User findByEmail(String email);
    
    @Query("SELECT u FROM User u WHERE u.username = ?1")
    public User findByUsername(String username);
   
    @Query("SELECT u FROM User u WHERE u.phoneNumber = ?1")
    public User findByPhoneNumber(String phoneNumber);
    
    @Query("SELECT u FROM User u WHERE u.passwordResetToken = ?1")
    public User findByPasswordToken(String passwordResetToken);
    
    @Query(nativeQuery = true,value =  "SELECT * FROM users WHERE username != ?1 and username = ?2")
    public User checkDuplicateUsername(String currentUsername, String newUsername);
    
    @Query(nativeQuery = true,value =  "SELECT * FROM users WHERE email != ?1 and email = ?2")
    public User checkDuplicateEmail(String currentEmail, String newEmail);
    
    @Query(nativeQuery = true,value =  "SELECT * FROM users WHERE phone_number != ?1 and phone_number = ?2")
    public User checkDuplicatePhoneNumber(String currentPhoneNumber, String newPhoneNumber);
    
    @Query(nativeQuery = true, value =  "SELECT DISTINCT u.*,u.phone_number as phoneNumber FROM users u inner join users_roles ur on u.id = ur.user_id inner join roles r on r.id = ur.role_id where r.name != 'ADMIN' order by u.id desc")
    public List<UserDTO> getAllExcludeAdmin();

    @Query(nativeQuery = true, value =  "SELECT DISTINCT u.*,u.phone_number as phoneNumber FROM users u inner join users_roles ur on u.id = ur.user_id inner join roles r on r.id = ur.role_id where r.name != 'ADMIN' and CONCAT(u.fullname,u.username,u.email,u.phone_number) LIKE %?1% order by u.id desc")
    public List<UserDTO> searchUsersExcludeAdmin(String keyword);
    
    @Query(nativeQuery = true, value = "select exists (SELECT u.username FROM users u inner join users_roles ur on u.id = ur.user_id inner join roles r on r.id = ur.role_id where u.id = ?1 and r.name = 'ADMIN')")
    public Integer checkDeletedUserIsAdmin(Integer id);

    @Query(nativeQuery = true, value = "select exists (select u.username from users u inner join application_form a on u.id = a.creator_id where u.id=?1)")
    public Integer checkUserExistInApplicationForm(Integer id);
    
    @Query(nativeQuery = true, value = "select exists (select u.username from users u inner join test t on u.id = t.creator_id where u.id=?1)")
    public Integer checkUserExistInTest(Integer id);

    @Query(nativeQuery = true, value = "select exists (select * from users_roles where user_id = ?1)")
    public Integer checkUserExistInUserRole(Integer id);
    
    @Query(nativeQuery = true, value = "select exists (select * from management_assignment where user_id = ?1)")
    public Integer checkUserExistInManagementAssignment(Integer id);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "delete from users_roles where user_id = ?1")
    public void deleteUserRoleByUserId(Integer id);
    
    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "delete from management_assignment where user_id = ?1")
    public void deleteManagementAssignmentByUserId(Integer id);

    @Query(nativeQuery = true, value = "SELECT u.* FROM users u inner join users_roles ur on u.id = ur.user_id inner join roles r on r.id = ur.role_id where r.name = 'ADMIN'")
    public List<User> getAllUserAdmin();

 
}
