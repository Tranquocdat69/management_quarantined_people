package com.project_iv.group_3.user.service;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;
    
    public void sendEmail(String recipientEmail,String passwordtoLogin)
        throws MessagingException, UnsupportedEncodingException {

        MimeMessage message = mailSender.createMimeMessage();              
        MimeMessageHelper helper = new MimeMessageHelper(message);
        
        helper.setFrom("c1808j1@gmail.com", "HỆ THỐNG QUẢN LÍ NGƯỜI TRONG KHU VỰC CÁCH LY");
        helper.setTo(recipientEmail);
        
        String subject = "Mật khẩu để đăng nhập";
        
        String content = "Chào bạn!"
        +"<br/>"
        +"<br/>"
        +"Đây là mật khẩu tạm thời để đăng nhập <b style='color:red'>(Tuyệt đối không chuyển tiếp hay cung cấp đến bất kì ai)</b>: "
        +"<h1>"+passwordtoLogin+"</h1>"
        +"<b>Thay đổi mật khẩu sau khi đăng nhập lần đầu tiên</b>"
        +"<br/>"
        +"<br/>"
        +"Bạn nhận được thông báo này vì địa chỉ email này được dùng để đăng kí vào hệ thống quản lí người khu vực cách ly"
        +"<br/>"
        +"Trân trọng!";
        helper.setSubject(subject);
        
        helper.setText(content, true);
        
        mailSender.send(message);
    }
    
    public void sendEmailResetPassword(String recipientEmail, String urlToResetPassword)
        throws MessagingException, UnsupportedEncodingException {

        MimeMessage message = mailSender.createMimeMessage();              
        MimeMessageHelper helper = new MimeMessageHelper(message);
        
        helper.setFrom("c1808j1@gmail.com", "HỆ THỐNG QUẢN LÍ NGƯỜI TRONG KHU VỰC CÁCH LY");
        helper.setTo(recipientEmail);
        
        String subject = "Đặt lại mật khẩu";
        
        String content = "Chào bạn!"
        +"<br/>"
        +"<br/>"
        +"Bạn đã yêu cầu thay đổi mật khẩu truy cập trên hệ thống quản lí người trong khu vực cách ly"
        +"<br/>"
        +"Bạn vui lòng nhấn vào liên kết sau để xác nhận đổi lại mật khẩu: "+urlToResetPassword
        +"<br/>"
        +"<br/>"
        +"<b>(Trường hợp không thể mở được liên kết. Bạn vui lòng chép đường dẫn trên vào thanh địa chỉ của mình trên trình duyệt rồi nhấn phím Enter)</b>"
        +"<br/>"
        +"<br/>"
        +"Bạn nhận được thông báo này vì địa chỉ email này được dùng để đăng kí vào hệ thống quản lí người khu vực cách ly"
        +"<br/>"
        +"Trân trọng!";
        
        helper.setSubject(subject);
        
        helper.setText(content, true);
        
        mailSender.send(message);
    }
}
