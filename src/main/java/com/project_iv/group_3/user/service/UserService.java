package com.project_iv.group_3.user.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.project_iv.group_3.area.entity.Area;
import com.project_iv.group_3.common.service.MyUploadFileService;
import com.project_iv.group_3.role.entity.Role;
import com.project_iv.group_3.user.dto.UserDTO;
import com.project_iv.group_3.user.entity.User;
import com.project_iv.group_3.user.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MyUploadFileService uploadFileService;

    public User findByEmail(String email){
        return userRepository.findByEmail(email);
    }
    
    public User findByUsername(String username){
        return userRepository.findByUsername(username);
    }
    
    public User findByPhoneNumber(String phoneNumber){
        return userRepository.findByPhoneNumber(phoneNumber);
    }
    public User findByPasswordToken(String passwordToken){
        return userRepository.findByPasswordToken(passwordToken);
    }

    public User findById(Integer id){
        boolean existsById = userRepository.existsById(id);
        if (existsById) {
            Integer checkDeletedUserIsAdmin = userRepository.checkDeletedUserIsAdmin(id);
            if (checkDeletedUserIsAdmin == 0) {
                    User userById = userRepository.getById(id);
                    if (userById != null) {
                        return userById;
                    }
                }
        }

        return null;
    }

    public User addNewUser(User user, String encodedPass,String passwordResetToken){
        user.setPasswordResetToken(passwordResetToken);
        user.setPassword(encodedPass);
        user.setIsFirstTimeLogin(true);
        user.setCreatedDate(new Date());
        User savedUser = userRepository.save(user);
        return savedUser;
    }

    public User editUserByAdmin(Integer id,String fullname,String username, String email,String avatar ,String phoneNumber,List<Role> roles,List<Area> areas){
        User userNeedToUpdate = userRepository.getById(id);
        if (userNeedToUpdate != null) {
            userNeedToUpdate.setFullname(fullname);
            userNeedToUpdate.setUsername(username);
            userNeedToUpdate.setEmail(email);
            if (avatar != "") {
                userNeedToUpdate.setAvatar(avatar);
            }
            userNeedToUpdate.setPhoneNumber(phoneNumber);
            userNeedToUpdate.setRoles(roles);
            userNeedToUpdate.setAreas(areas);
            userNeedToUpdate.setUpdatedDate(new Date());
    
            User savedUser = userRepository.save(userNeedToUpdate);
            return savedUser;
        }

        return new User();
    }
  
    public User editProfileUser(Integer id,String fullname,String avatar ,String phoneNumber){
        User userNeedToUpdate = userRepository.getById(id);
        if (userNeedToUpdate != null) {
            userNeedToUpdate.setFullname(fullname);
            if (avatar != "") {
                userNeedToUpdate.setAvatar(avatar);
            }
            userNeedToUpdate.setPhoneNumber(phoneNumber);
            userNeedToUpdate.setUpdatedDate(new Date());
    
            User savedUser = userRepository.save(userNeedToUpdate);
            return savedUser;
        }

        return new User();
    }
   
    public User updatePasswordFirstLogin(String passwordResetToken,String encodedNewPassword){
        if (passwordResetToken != "") {
        User userNeedToUpdate = userRepository.findByPasswordToken(passwordResetToken);
            if (userNeedToUpdate != null) {
                userNeedToUpdate.setPassword(encodedNewPassword);
                userNeedToUpdate.setIsFirstTimeLogin(false);
                userNeedToUpdate.setPasswordResetToken(null);
                userNeedToUpdate.setUpdatedDate(new Date());
        
                User savedUser = userRepository.save(userNeedToUpdate);
                return savedUser;
            }
        }

        return new User();
    }
    
    public User updatePassword(Integer id, String encodedNewPassword){
        User userNeedToUpdate = userRepository.getById(id);
        if (userNeedToUpdate != null) {
            userNeedToUpdate.setPassword(encodedNewPassword);
            userNeedToUpdate.setUpdatedDate(new Date());

            User savedUser = userRepository.save(userNeedToUpdate);
            return savedUser;
        }
        return new User();

    }

    public User updatePasswordResetTokenByEmail(String email,String passwordResetToken){
        User findByEmail = userRepository.findByEmail(email);
        if (findByEmail != null) {
            findByEmail.setPasswordResetToken(passwordResetToken);
            findByEmail.setUpdatedDate(new Date());
            User savedUser = userRepository.save(findByEmail);
            return savedUser;
        }
        return new User();
    }

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public List<User> getAllUserAdmin(){
        return userRepository.getAllUserAdmin();
    }
 

    public List<UserDTO> getAllUsersExcludeAdmin(){
        return userRepository.getAllExcludeAdmin();
    }

    public User checkDuplicateUsername(String currentUsername, String newUsername){
        return userRepository.checkDuplicateUsername(currentUsername, newUsername);
    }
    
    public User checkDuplicateEmail(String currentEmail, String newEmail){
        return userRepository.checkDuplicateEmail(currentEmail, newEmail);
    }
    
    public User checkDuplicatePhoneNumber(String currentPhoneNumber, String newPhoneNumber){
        return userRepository.checkDuplicatePhoneNumber(currentPhoneNumber, newPhoneNumber);
    }

    public List<UserDTO> searchUsersExcludeAdmin(String keyword){
        if (keyword.trim().isEmpty()) {
            return new ArrayList<UserDTO>();
        }
        return userRepository.searchUsersExcludeAdmin(keyword);
    }

    public Boolean deleteUser(Integer id){
        User userToDelete = userRepository.getById(id);
        if (userToDelete != null) {
            Integer checkDeletedUserIsAdmin = userRepository.checkDeletedUserIsAdmin(id);
            if (checkDeletedUserIsAdmin == 0) {
                Integer checkUserExistInApplicationForm = userRepository.checkUserExistInApplicationForm(id);
                Integer checkUserExistInTest = userRepository.checkUserExistInTest(id);
                if (checkUserExistInApplicationForm == 0 && checkUserExistInTest == 0) {
                    Integer checkUserExistInUserRole = userRepository.checkUserExistInUserRole(id);
                    Integer checkUserExistInManagementAssignment = userRepository.checkUserExistInManagementAssignment(id);
                    if (checkUserExistInUserRole == 1) {
                        userRepository.deleteUserRoleByUserId(id);
                    }
                    if (checkUserExistInManagementAssignment == 1) {
                        userRepository.deleteManagementAssignmentByUserId(id);
                    }
                    if (userToDelete.getAvatar() != null &&  !userToDelete.getAvatar().isEmpty()) {
                        uploadFileService.deleteUploadedFile(userToDelete.getAvatar());
                    }
                    userRepository.deleteById(id);
                    return true;
                }
            }
        }
        return false;
    }

}
