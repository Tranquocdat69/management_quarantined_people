package com.project_iv.group_3.role.model;

public enum ListRole {
    ADMIN,
    ADDER,
    HANDLER,
    TESTER
}
