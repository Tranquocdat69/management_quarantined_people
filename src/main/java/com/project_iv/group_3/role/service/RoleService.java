package com.project_iv.group_3.role.service;

import java.util.List;

import com.project_iv.group_3.role.entity.Role;
import com.project_iv.group_3.role.repository.RoleRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    public Role getRoleByName(String name){
       return roleRepository.findByName(name);
    }

    public List<Role> getAllRolesExcludeAdmin(){
        return roleRepository.getAllExcludeAdmin();
    }
}
