package com.project_iv.group_3.role.repository;

import java.util.List;

import com.project_iv.group_3.role.entity.Role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface RoleRepository extends JpaRepository<Role,Integer>{

    @Query("SELECT r FROM Role r WHERE r.name = :name")
    public Role findByName(@Param("name") String name);

    @Query("SELECT r FROM Role r where r.name != 'ADMIN'")
    public List<Role> getAllExcludeAdmin();
}
