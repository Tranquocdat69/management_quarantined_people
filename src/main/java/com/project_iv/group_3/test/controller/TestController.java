package com.project_iv.group_3.test.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.project_iv.group_3.area.entity.Area;
import com.project_iv.group_3.quarantined_information.entity.QuarantinedInformation;
import com.project_iv.group_3.quarantined_information.model.StatusOfQuarantinedInformation;
import com.project_iv.group_3.quarantined_information.service.QuarantinedInformationService;
import com.project_iv.group_3.quarantined_people.entity.QuarantinedPeople;
import com.project_iv.group_3.quarantined_people.service.QuarantinedPeopleService;
import com.project_iv.group_3.room.entity.Room;
import com.project_iv.group_3.room.service.RoomService;
import com.project_iv.group_3.test.entity.Test;
import com.project_iv.group_3.test.service.TestService;
import com.project_iv.group_3.test_type.entity.TestType;
import com.project_iv.group_3.test_type.service.TestTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
    @Autowired
    private TestService TestService;

    @Autowired
    private TestTypeService TestTypeService;

    @Autowired
    private RoomService RoomService;

    @Autowired
    private QuarantinedInformationService quarantinedInformationService;
    
    @Autowired
    private QuarantinedPeopleService quarantinedPeopleService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @GetMapping("/test/getAllTestByQuarantinedInformationIdDesc")
    @ResponseBody
    public List<Test> getAllTestByQuarantinedInformationIdDesc(@RequestParam Integer quarantinedInformationId) {
        List<Test> listAll = TestService.getAllTestByQuarantinedInformationIdDesc(quarantinedInformationId);

        return listAll;
    }

    @GetMapping("/test/create-test/{quarantinedPeopleId}")
    public String createPage(@PathVariable Integer quarantinedPeopleId, Model model) {
        Boolean checkUserHasAuthorAccessDetail = quarantinedPeopleService.checkUserHasAuthorAccessDetail(quarantinedPeopleId);
        if (checkUserHasAuthorAccessDetail) {
            QuarantinedInformation quarantinedInformationNotCompleteByQuarantinedPeopleId = quarantinedInformationService.getQuarantinedInformationNotCompleteByQuarantinedPeopleId(quarantinedPeopleId);
            if (quarantinedInformationNotCompleteByQuarantinedPeopleId.getStatus() != null) {
                    Integer totalNegativeResultByQuarantinedInformationId = TestService.getTotalNegativeResultByQuarantinedInformationId(quarantinedInformationNotCompleteByQuarantinedPeopleId.getId());
                    Integer totalPositiveResultByQuarantinedInformationId = TestService.getTotalPositiveResultByQuarantinedInformationId(quarantinedInformationNotCompleteByQuarantinedPeopleId.getId());
                    if (totalNegativeResultByQuarantinedInformationId < 2 && totalPositiveResultByQuarantinedInformationId < 2) {
                        List<TestType> listTestTypes = TestTypeService.listAll();
                        if (listTestTypes.size() > 0) {
                            model.addAttribute("test", new Test());
                            model.addAttribute("listTestTypes",listTestTypes);
                            model.addAttribute("quarantinedInformationId",quarantinedInformationNotCompleteByQuarantinedPeopleId.getId());
                            model.addAttribute("quarantinedPeopleId", quarantinedPeopleId);
                            model.addAttribute("arrivalDate", quarantinedInformationNotCompleteByQuarantinedPeopleId.getArrivalDate());
                            model.addAttribute("departureDate", quarantinedInformationNotCompleteByQuarantinedPeopleId.getDepartureDate());
    
                            return "dashboard/test/create";
                        }
                        return "redirect:/test-type/create-testType";
                    }
            }
            return "redirect:/error/404.html";
        }

        return "redirect:/error/403.html";
    }

    @PostMapping("/test/addTest")
    public String addTest(Test test, Integer quarantinedPeopleId, Integer quarantinedInformationId,HttpServletRequest request) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date testDate = simpleDateFormat.parse(request.getParameter("testDateTemp").toString());
        Date receiveSampleDate = simpleDateFormat.parse(request.getParameter("receiveSampleDateTemp").toString());

        QuarantinedInformation quarantinedInformationNotCompleteByQuarantinedPeopleId = quarantinedInformationService.getQuarantinedInformationNotCompleteByQuarantinedPeopleId(quarantinedPeopleId);
        if (quarantinedInformationNotCompleteByQuarantinedPeopleId.getStatus() != null) {
            Test addTest = TestService.addNewTest(test, quarantinedInformationNotCompleteByQuarantinedPeopleId,testDate,receiveSampleDate);
            if (addTest != null) {
                // Integer totalPositiveResult = TestService.getTotalPositiveResult(quarantinedInformationId);
                // if (totalPositiveResult >= 2) {
                //     quarantinedInformationService.updateStatusRoomQuarantinedInformation(quarantinedInformationId);
                //     quarantinedInformationService.updateStatusQuarantinedInformation(StatusOfQuarantinedInformation.status_2, quarantinedInformationId);
                // }
                this.simpMessagingTemplate.convertAndSend("/topic/getTest/"+quarantinedInformationId,"created new test");

                return "redirect:/quarantined-information/detail-quarantined-information/"+quarantinedInformationNotCompleteByQuarantinedPeopleId.getId();
            }
        }
        return "redirect:/test/create-test";
    }

    @GetMapping("/test/checkDateExistsInTestByQuarantinedInformationId")
    @ResponseBody
    public Boolean checkDateExistsInTestByQuarantinedInformationId(@RequestParam String receiveSampleDate,@RequestParam String testDate,@RequestParam Integer quarantinedInformationId){
        return TestService.checkDateExistsInTestByQuarantinedInformationId(receiveSampleDate, testDate, quarantinedInformationId);
    }   
    
    @GetMapping("/test/getTotalTestHaveQualifiedSampleStatusByQuarantinedInformationId")
    @ResponseBody
    public Integer getTotalTestHaveQualifiedSampleStatusByQuarantinedInformationId(@RequestParam Integer quarantinedPeopleId){
        QuarantinedInformation quarantinedInformationNotCompleteByQuarantinedPeopleId = quarantinedInformationService.getQuarantinedInformationNotCompleteByQuarantinedPeopleId(quarantinedPeopleId);
        return TestService.getTotalTestHaveQualifiedSampleStatusByQuarantinedInformationId(quarantinedInformationNotCompleteByQuarantinedPeopleId.getId());
        
    }   
    
    @GetMapping("/test/getTotalPositiveResultByQuarantinedInformationId")
    @ResponseBody
    public Integer getTotalPositiveResultByQuarantinedInformationId(@RequestParam Integer quarantinedPeopleId){
        QuarantinedInformation quarantinedInformationNotCompleteByQuarantinedPeopleId = quarantinedInformationService.getQuarantinedInformationNotCompleteByQuarantinedPeopleId(quarantinedPeopleId);
        return TestService.getTotalPositiveResultByQuarantinedInformationId(quarantinedInformationNotCompleteByQuarantinedPeopleId.getId());
    }   
    
    @GetMapping("/test/getTotalNegativeResultByQuarantinedInformationId")
    @ResponseBody
    public Integer getTotalNegativeResultByQuarantinedInformationId(@RequestParam Integer quarantinedPeopleId){
        QuarantinedInformation quarantinedInformationNotCompleteByQuarantinedPeopleId = quarantinedInformationService.getQuarantinedInformationNotCompleteByQuarantinedPeopleId(quarantinedPeopleId);
        return TestService.getTotalNegativeResultByQuarantinedInformationId(quarantinedInformationNotCompleteByQuarantinedPeopleId.getId());
    }   
    
    @GetMapping("/test/deleteTestById")
    @ResponseBody
    public Boolean deleteTestById(@RequestParam Integer id, @RequestParam Integer quarantinedInformationId){
        QuarantinedInformation quarantinedInformationyById = quarantinedInformationService.getQuarantinedInformationyById(quarantinedInformationId);
        Boolean checkUserHasAuthorAccessDetail = quarantinedPeopleService.checkUserHasAuthorAccessDetail(quarantinedInformationyById.getQuarantinedPeople().getId());
        if (checkUserHasAuthorAccessDetail) {
            Boolean deleteTestById = TestService.deleteTestById(id);
            if (deleteTestById) {
                this.simpMessagingTemplate.convertAndSend("/topic/getTest/"+quarantinedInformationId,"deleted test");
            }
            return deleteTestById;
        }
        
        return false;
    }
    
    @GetMapping("/test/detail-test/{id}")
    public String detailPage(@PathVariable Integer id, Model model){
        Test testById = TestService.getTestById(id);
        if (testById != null){
            QuarantinedInformation quarantinedInformationyById = quarantinedInformationService.getQuarantinedInformationyById(testById.getQuarantinedInformation().getId());
            Boolean checkUserHasAuthorAccessDetail = quarantinedPeopleService.checkUserHasAuthorAccessDetail(quarantinedInformationyById.getQuarantinedPeople().getId());
            if (checkUserHasAuthorAccessDetail) {
                QuarantinedPeople quarantinedPeople = testById.getQuarantinedInformation().getQuarantinedPeople();
                TestType testType = testById.getTestType();
                Room roomQuarantinedInformationLast = RoomService.getRoomQuarantinedInformationLast(quarantinedInformationyById.getId());
                Area area = roomQuarantinedInformationLast.getArea();
                model.addAttribute("area",area);
                model.addAttribute("roomQuarantinedInformationLast",roomQuarantinedInformationLast);
                model.addAttribute("testType",testType);
                model.addAttribute("quarantinedPeople",quarantinedPeople);
                model.addAttribute("testById", testById);
                return "dashboard/test/detail";
            }
            return "redirect:/error/403.html";
        }
        return "redirect:/error/404.html";
    }

    @GetMapping("/test/updatePositiveResultForQuarantinedInformation")
    @ResponseBody
    public Boolean updatePositiveResultForQuarantinedInformation(@RequestParam Integer quarantinedInformationId){
        QuarantinedInformation quarantinedInformationyById = quarantinedInformationService.getQuarantinedInformationyById(quarantinedInformationId);
        if (quarantinedInformationyById.getStatus() == 0) {
            if (quarantinedInformationyById != null) {
                quarantinedInformationService.updateStatusQuarantinedInformation(StatusOfQuarantinedInformation.status_2, quarantinedInformationId);
                quarantinedInformationService.updateStatusRoomQuarantinedInformation(quarantinedInformationId);

                this.simpMessagingTemplate.convertAndSend("/topic/getRoom","update result: positive");
                this.simpMessagingTemplate.convertAndSend("/topic/getTest/"+quarantinedInformationId,"update result: positive");
                this.simpMessagingTemplate.convertAndSend("/topic/getApplicationForm/"+quarantinedInformationId,"update result: positive");
                this.simpMessagingTemplate.convertAndSend("/topic/getQuarantinedInformation/"+quarantinedInformationyById.getQuarantinedPeople().getId(),"update result: positive");
                this.simpMessagingTemplate.convertAndSend("/topic/getQuarantinedPeople","update result: positive");

                return true;
            }
        }
        return false;
    }

}
