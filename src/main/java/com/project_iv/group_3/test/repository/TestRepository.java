package com.project_iv.group_3.test.repository;

import java.util.List;

import com.project_iv.group_3.test.entity.Test;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TestRepository extends JpaRepository<Test,Integer> {
    
    @Query(nativeQuery = true, value = "select * from test where quarantined_information_id=?1 order by test_date desc")
    public List<Test> getAllTestByQuarantinedInformationIdDesc(Integer quarantinedInformationId);
   
    @Query(nativeQuery = true, value = "select * from test where test_id = ?1")
    public List<Test> getTestById(Integer id);

    @Query(nativeQuery = true, value = "select exists (select t.quarantined_information_id from test t where t.quarantined_information_id = ?1)")
    public Integer checkQuarantinedInformationExistInTest(Integer quarantinedInformationId);
    
    @Query(nativeQuery = true, value = "select exists (select * from test where receive_sample_date = ?1 and test_date = ?2 and quarantined_information_id=?3 and sample_status = 1)")
    public Integer checkDateExistsInTestByQuarantinedInformationId(String receiveSampleDate, String testDate,Integer quarantinedInformationId);

    @Query(nativeQuery = true, value = "select count(test_id) from test where sample_status = 1 and quarantined_information_id=?1")
    public Integer getTotalTestHaveQualifiedSampleStatusByQuarantinedInformationId(Integer quarantinedInformationId);
    
    @Query(nativeQuery = true, value = "select count(test_id) from test where test_result = 1 and sample_status = 1 and quarantined_information_id=?1")
    public Integer getTotalPositiveResultByQuarantinedInformationId(Integer quarantinedInformationId);
   
    @Query(nativeQuery = true, value = "select count(test_id) from test where test_result = 0 and sample_status = 1 and quarantined_information_id=?1")
    public Integer getTotalNegativeResultByQuarantinedInformationId(Integer quarantinedInformationId);

}
