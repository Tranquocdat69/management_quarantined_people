package com.project_iv.group_3.test.service;

import java.util.Date;
import java.util.List;

import com.project_iv.group_3.quarantined_information.entity.QuarantinedInformation;
import com.project_iv.group_3.quarantined_information.repository.QuarantinedInformationRepository;
import com.project_iv.group_3.security.user_detail.CustomUserDetail;
import com.project_iv.group_3.test.entity.Test;
import com.project_iv.group_3.test.repository.TestRepository;
import com.project_iv.group_3.user.entity.User;
import com.project_iv.group_3.user.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class TestService {
    @Autowired
    private TestRepository TestRepository;

    @Autowired
    private QuarantinedInformationRepository qiRepository;

    @Autowired 
    private UserService userService;
    
    public List<Test> getAllTestByQuarantinedInformationIdDesc(Integer quarantinedInformationId){
        return TestRepository.getAllTestByQuarantinedInformationIdDesc(quarantinedInformationId);
    }

    public List<QuarantinedInformation> getQuarantinedInformationByQuarantinedPeopleId(Integer quarantinedPeopleId){
        return qiRepository.getQuarantinedInformationByQuarantinedPeopleId(quarantinedPeopleId);
    }

    public Test addNewTest(Test test,QuarantinedInformation quarantinedInformation, Date testDate, Date receiveSampleDate){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetail customUser = (CustomUserDetail) authentication.getPrincipal();
        String userName = customUser.getUser().getUsername();

        User userCreate = userService.findByUsername(userName);
       
        test.setUser(userCreate);
        test.setTestDate(testDate);
        test.setReceiveSampleDate(receiveSampleDate);
        test.setQuarantinedInformation(quarantinedInformation);

        Test saveTest = TestRepository.save(test);
        return saveTest;
    }

    public Boolean checkQuarantinedInformationExistInTest(Integer quarantinedInformationId){
        Integer checkQuarantinedInformationExistInTest = TestRepository.checkQuarantinedInformationExistInTest(quarantinedInformationId);
        if (checkQuarantinedInformationExistInTest == 1) {
            return true;
        }
        return false;
    }

    public Boolean checkDateExistsInTestByQuarantinedInformationId(String receiveSampleDate, String testDate,Integer quarantinedInformationId){
        Integer checkDateExistsInTestByQuarantinedInformationId = TestRepository.checkDateExistsInTestByQuarantinedInformationId(receiveSampleDate, testDate,quarantinedInformationId);
        if (checkDateExistsInTestByQuarantinedInformationId == 1) {
            return true;
        }
        return false;
    }
    
    public Integer getTotalTestHaveQualifiedSampleStatusByQuarantinedInformationId(Integer quarantinedInformationId){
        return TestRepository.getTotalTestHaveQualifiedSampleStatusByQuarantinedInformationId(quarantinedInformationId);
    }
   
    public Integer getTotalPositiveResultByQuarantinedInformationId(Integer quarantinedInformationId){
        return TestRepository.getTotalPositiveResultByQuarantinedInformationId(quarantinedInformationId);
    }
    
    public Integer getTotalNegativeResultByQuarantinedInformationId(Integer quarantinedInformationId){
        return TestRepository.getTotalNegativeResultByQuarantinedInformationId(quarantinedInformationId);
    }

    public Boolean deleteTestById(Integer id){
        boolean existsById = TestRepository.existsById(id);
        if (existsById) {
            TestRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public Test getTestById(Integer id){
        boolean existsById = TestRepository.existsById(id);
        if (existsById) {
            return TestRepository.getById(id);
        }

        return null;
    }
}
