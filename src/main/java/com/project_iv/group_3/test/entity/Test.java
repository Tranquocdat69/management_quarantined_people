package com.project_iv.group_3.test.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.project_iv.group_3.quarantined_information.entity.QuarantinedInformation;
import com.project_iv.group_3.test_type.entity.TestType;
import com.project_iv.group_3.user.entity.User;

@Entity
@Table(name = "test")
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "test_id")
    private Integer testId;

    @ManyToOne
    @JoinColumn(name = "test_type_id")
    private TestType testType;

    @Column(name = "diagnose", nullable = false)
    private String diagnose;
    
    @Column(name = "sample", nullable = false)
    private String sample;
    
    @Column(name = "sampler", nullable = false)
    private String sampler;
   
    @Column(name = "sample_status", nullable = false)
    private Boolean sampleStatus;
    
    @JsonFormat(pattern = "dd/MM/yyyy",timezone = "GMT+8")
    @Temporal(TemporalType.DATE)
    @Column(name = "receive_sample_date", nullable = false)
    private Date receiveSampleDate;
    
    @JsonFormat(pattern = "dd/MM/yyyy",timezone = "GMT+8")
    @Temporal(TemporalType.DATE)
    @Column(name = "test_date", nullable = false)
    private Date testDate;

    @Column(name = "test_result", nullable = false)
    private Boolean testResult;

    @Column(name = "tester", nullable = false)
    private String tester;

    @ManyToOne
    @JoinColumn(name = "quarantined_information_id")
    private QuarantinedInformation quarantinedInformation;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private User user;

    public Integer getTestId() {
        return testId;
    }

    public void setTestId(Integer testId) {
        this.testId = testId;
    }

    public TestType getTestType() {
        return testType;
    }

    public void setTestType(TestType testType) {
        this.testType = testType;
    }

    public String getDiagnose() {
        return diagnose;
    }

    public void setDiagnose(String diagnose) {
        this.diagnose = diagnose;
    }

    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public Date getReceiveSampleDate() {
        return receiveSampleDate;
    }

    public void setReceiveSampleDate(Date receiveSampleDate) {
        this.receiveSampleDate = receiveSampleDate;
    }

    public Date getTestDate() {
        return testDate;
    }

    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }

    public Boolean getTestResult() {
        return testResult;
    }

    public void setTestResult(Boolean testResult) {
        this.testResult = testResult;
    }

    public String getTester() {
        return tester;
    }

    public void setTester(String tester) {
        this.tester = tester;
    }
    
    public QuarantinedInformation getQuarantinedInformation() {
        return quarantinedInformation;
    }

    public void setQuarantinedInformation(QuarantinedInformation quarantinedInformation) {
        this.quarantinedInformation = quarantinedInformation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSampler() {
        return sampler;
    }

    public void setSampler(String sampler) {
        this.sampler = sampler;
    }

    public Boolean getSampleStatus() {
        return sampleStatus;
    }

    public void setSampleStatus(Boolean sampleStatus) {
        this.sampleStatus = sampleStatus;
    }
}
