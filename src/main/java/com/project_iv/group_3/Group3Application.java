package com.project_iv.group_3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Group3Application {

	public static void main(String[] args) {
		SpringApplication.run(Group3Application.class, args);
	}

}
