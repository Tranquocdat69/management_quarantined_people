package com.project_iv.group_3.test_type.controller;

import java.util.List;

import com.project_iv.group_3.test_type.entity.TestType;
import com.project_iv.group_3.test_type.service.TestTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestTypeController {
    @Autowired
    private TestTypeService TestTypeService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @GetMapping("/test-type")
    public String list(){
        return "dashboard/test_type/index";
    }

    @GetMapping("/test-type/getAllTestType")
    @ResponseBody
    public List<TestType> getAllTestType(){
        List<TestType> listAll = TestTypeService.listAll();
        return listAll;
    }

    @GetMapping("/test-type/create-testType")
    public String showNewTestType(Model model) {
        model.addAttribute("testType", new TestType());
         
        return "dashboard/test_type/create"; 
    }
    
    @PostMapping("/test-type/add-testType")
    public String addTestType(@ModelAttribute("testType") TestType testType){
        TestTypeService.addNewTestType(testType);
        this.simpMessagingTemplate.convertAndSend("/topic/getTestType","created");

        return "redirect:/test-type";
    }
    @GetMapping("/test-type/edit-testType/{id}")
    public String showEditForm(@PathVariable(name="id") Integer id,Model model){
        TestType testType =TestTypeService.getById(id);
        if (testType != null) {
            model.addAttribute("testType", testType);
            return "dashboard/test_type/edit";
        }

        return "redirect:/error/404.html";
    }

    @PostMapping("/test-type/edit-testType")
    public String saveArea( Integer id,@ModelAttribute("testType") TestType testType){
        TestType testTypeById =TestTypeService.getById(id);
        if (testTypeById != null) {
            TestTypeService.addNewTestType(testType);
            this.simpMessagingTemplate.convertAndSend("/topic/getTestType","edited");

            return "redirect:/test-type";
        }
        return "redirect:/error/404.html";
    }

    @GetMapping("/test-type/delete")
    @ResponseBody
    public Boolean delete(@RequestParam Integer id){
        Boolean isDelete = TestTypeService.delete(id);
        if (isDelete) {
            this.simpMessagingTemplate.convertAndSend("/topic/getTestType","deleted");
        }
        return isDelete;
    }

    
    @GetMapping("/test-type/SearchTestType")
    @ResponseBody
    public List<TestType> SearchTestType(@RequestParam String searchType){
        List<TestType> SearchTestType = TestTypeService.SearchTestType(searchType);
        return SearchTestType;
    }

    @GetMapping("/test-type/CheckExsitTestType")
    @ResponseBody
    public boolean CheckExsitTestType(@RequestParam String testType){
        return TestTypeService.CheckExsitTestType(testType);
    }
    @GetMapping("/test-type/CheckExsitEditTestType")
    @ResponseBody
    public boolean CheckExsitEditTestType(@RequestParam String currentName, String newName){
        return TestTypeService.CheckExsitEditTestType(currentName,newName);
    }
}
