package com.project_iv.group_3.test_type.service;

import java.util.ArrayList;
import java.util.List;

import com.project_iv.group_3.test_type.entity.TestType;
import com.project_iv.group_3.test_type.repository.TestTypeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestTypeService {
    @Autowired
    private TestTypeRepository res;

    public List<TestType> listAll(){
        return res.getAllTestTypesIdDesc();
    }

    public void addNewTestType(TestType testType){
        res.save(testType);
    }

    public void save(TestType id){
        res.save(id);
    }

    public TestType getById(Integer id){
        boolean existsById = res.existsById(id);
        if (existsById) {
            return res.findById(id).get();
        }
        return null;
    }

    public Boolean delete(Integer id){
        TestType testTypeToDelete= res.getById(id);
        Integer checkExsitTestTypeInTest = res.CheckExsitTestTypeInTest(id);
        if (checkExsitTestTypeInTest == 0) {
            if(testTypeToDelete!= null){
                res.deleteById(id);
                return true;
            }
        }
        return false;
    }
    public List<TestType> SearchTestType(String searchType){
        if (searchType.trim().isEmpty()) {
            return new ArrayList<TestType>();
        }
        return res.SearchTestType(searchType);
    }

    public Boolean CheckExsitTestType(String testType) {
        Integer checkExsitTestType = res.CheckExsitTestType(testType);
        if (checkExsitTestType == 1) {
            return true;
        }

        return false;
    }
    public Boolean CheckExsitEditTestType(String currentName, String newName){
        Integer checkExsitEditTestType = res.CheckExsitEditTestType(currentName, newName);
        if (checkExsitEditTestType ==1) {
            return true;
        }
        return false;
    }

}
