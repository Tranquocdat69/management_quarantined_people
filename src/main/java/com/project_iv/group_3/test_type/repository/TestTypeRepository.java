package com.project_iv.group_3.test_type.repository;

import java.util.List;

import com.project_iv.group_3.test_type.entity.TestType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TestTypeRepository extends JpaRepository<TestType,Integer>{
    @Query(nativeQuery = true, value = "SELECT DISTINCT * FROM test_type ORDER BY id DESC")
    public List<TestType> getAllTestTypesIdDesc();

    @Query(nativeQuery = true, value = "select DISTINCT * FROM test_type where name LIKE %?1% ORDER BY id DESC")
    public List<TestType> SearchTestType(String searchType);

    @Query(nativeQuery = true, value = "select exists ( select * from test_type where name = ?1)")
    public Integer CheckExsitTestType(String testType);

    @Query(nativeQuery = true, value = "select exists ( select * from test_type where name != ?1 and name = ?2)")
    public Integer CheckExsitEditTestType(String currentName, String newName);
    
    @Query(nativeQuery = true, value = "select exists (select test_id from test where test_type_id = ?1)")
    public Integer CheckExsitTestTypeInTest(Integer testTypeId);
}
