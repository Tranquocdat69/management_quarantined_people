export const REGEX_EMAIL = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
export const REGEX_USERNAME = /^[a-z](?:_?[a-z0-9]+)*$/i;