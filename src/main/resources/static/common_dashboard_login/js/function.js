   export function showError(inputFocus, msg, customHtml = null) {
       if (inputFocus != null) {
           inputFocus.focus();
       }
       Swal.fire({
           title: 'Lỗi!',
           text: msg,
           html: customHtml,
           icon: 'error',
           confirmButtonText: 'OK'
       });
   };

   export function elementById(strId) {
       return document.getElementById(strId);
   }
   export function elementByName(strName) {
       return document.getElementsByName(strName);
   }
   export function elementByClassName(strName) {
       return document.getElementsByClassName(strName);
   }

   export function valueOf(element) {
       return element.value.trim();
   }

   export function checkRegex(string, regexString) {
       return string.match(regexString);
   }

   export function changeSubmitForm(formId, buttonId, toFunction) {
       formId.addEventListener("submit", function(e) {
           e.preventDefault();
       });
       buttonId.addEventListener("click", function() {
           toFunction(formId);
       });
   };

   export function showHidePassword(iconToShowHide, inputElement) {
       iconToShowHide.addEventListener("click", function() {
           if (inputElement.getAttribute("type") === "password") {
               inputElement.setAttribute("type", "text");
               iconToShowHide.classList.remove("bi", "bi-eye-fill");
               iconToShowHide.classList.add("bi", "bi-eye-slash-fill");
           } else {
               inputElement.setAttribute("type", "password");
               iconToShowHide.classList.remove("bi", "bi-eye-slash-fill");
               iconToShowHide.classList.add("bi", "bi-eye-fill");
           }
       });
   }