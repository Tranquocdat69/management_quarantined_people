import { showError, elementById, changeSubmitForm, valueOf } from "../../common_dashboard_login/js/function.js";

var formLogin;
var username;
var password;
var btnSubmit;

window.onload = (function() {
    start();

    changeSubmitForm(formLogin, btnSubmit, validateForm);

    checkErrorMessageAfterSubmit(errorMessage);

    checkInfoMessageBeforeSubmit(infoMessage);
})();

function start() {
    formLogin = elementById("formLogin");
    username = elementById("username");
    password = elementById("password");
    btnSubmit = elementById("btnSubmit");

    username.focus();
}

function checkErrorMessageAfterSubmit(errorMsg) {
    if (errorMsg !== null) {
        showError(null, errorMsg)
    };
}

function checkInfoMessageBeforeSubmit(infoMsg) {
    if (infoMsg !== null) {
        Swal.fire({
            title: 'Thông báo',
            text: infoMsg,
            icon: 'info',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Ok'
        })
    };
}

function validateForm(formId) {
    let valueUsername = valueOf(username);
    let valuePassword = valueOf(password);

    if (valueUsername === '') {
        showError(username, "Vui lòng nhập tên đăng nhập");
    } else {
        if (valueUsername.length > 45) {
            showError(username, "Độ dài tên đăng nhập tối đa 45 kí tự");
        } else {
            if (valuePassword === '') {
                showError(password, 'Vui lòng nhập mật khẩu');
            } else {
                formId.addEventListener("submit", function() {
                    formId.submit();
                });
            }
        }

    }
}