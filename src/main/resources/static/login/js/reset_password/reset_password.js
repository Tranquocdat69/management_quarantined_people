import { changeSubmitForm, elementById, showError, showHidePassword, valueOf } from "../../../common_dashboard_login/js/function.js";

var formResetPassword;
var newPassword;
var confirmNewPassword;
var btnSubmitResetPassword;

var iconShowNewPass;
var iconShowConfirmNewPass;


window.onload = (function() {
    start();

    changeSubmitForm(formResetPassword, btnSubmitResetPassword, validateForm);

})();

function start() {
    formResetPassword = elementById("formResetPassword");
    btnSubmitResetPassword = elementById("btnSubmitResetPassword");
    newPassword = elementById("newPassword");
    confirmNewPassword = elementById("confirmNewPassword");

    iconShowNewPass = elementById("iconShowNewPass");
    iconShowConfirmNewPass = elementById("iconShowConfirmNewPass");

    showHidePassword(iconShowNewPass, newPassword);
    showHidePassword(iconShowConfirmNewPass, confirmNewPassword);

    newPassword.focus();
}

function validateForm(formId) {
    let valueNewPass = valueOf(newPassword);
    let valueConfirmPass = valueOf(confirmNewPassword);

    if (valueNewPass === "") {
        showError(newPassword, "Nhập mật khẩu mới", null);
    } else if (valueNewPass.length < 6) {
        showError(newPassword, "Độ dài mật khẩu tối thiểu 6 kí tự", null);
    } else if (valueNewPass.length > 50) {
        showError(newPassword, "Độ dài mật khẩu tối đa 50 kí tự", null);
    } else if (valueConfirmPass === "") {
        showError(confirmNewPassword, "Xác nhận mật khẩu mới", null);
    } else if (valueConfirmPass !== valueNewPass) {
        showError(confirmNewPassword, "Mật khẩu xác nhận không khớp", null);
    } else {
        formId.submit();
    }

}