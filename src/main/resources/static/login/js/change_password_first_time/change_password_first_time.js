import { showError, elementById, changeSubmitForm, valueOf, showHidePassword } from "../../../common_dashboard_login/js/function.js";
import { URL_COVID } from "../../../dashboard/js/common_dashboard/variable.js";

var formChangePassword;
var currentPassword;
var newPassword;
var confirmNewPassword;
var btnSubmitChangePasswordFirstTime;

var iconShowCurrentPass;
var iconShowNewPass;
var iconShowConfirmNewPass;

window.onload = (function() {
    start();

    changeSubmitForm(formChangePassword, btnSubmitChangePasswordFirstTime, validateForm);

})();

function start() {
    formChangePassword = elementById("formChangePassword");
    currentPassword = elementById("currentPassword");
    newPassword = elementById("newPassword");
    confirmNewPassword = elementById("confirmNewPassword");
    btnSubmitChangePasswordFirstTime = elementById("btnSubmitChangePasswordFirstTime");
    iconShowCurrentPass = elementById("iconShowCurrentPass");
    iconShowNewPass = elementById("iconShowNewPass");
    iconShowConfirmNewPass = elementById("iconShowConfirmNewPass");

    showHidePassword(iconShowCurrentPass, currentPassword);
    showHidePassword(iconShowNewPass, newPassword);
    showHidePassword(iconShowConfirmNewPass, confirmNewPassword);

    currentPassword.focus();
};



function validateForm(formId) {
    let valueCurrentdPass = valueOf(currentPassword);
    let valueNewPass = valueOf(newPassword);
    let valueConfirmPass = valueOf(confirmNewPassword);

    if (valueCurrentdPass === "") {
        showError(currentPassword, "Nhập mật khẩu hiện tại", null);
    } else if (valueNewPass === "") {
        showError(newPassword, "Nhập mật khẩu mới", null);
    } else if (valueNewPass === valueCurrentdPass) {
        showError(newPassword, "Mật khẩu mới phải khác mật khẩu cũ", null);
    } else if (valueNewPass.length < 6) {
        showError(newPassword, "Độ dài mật khẩu tối thiểu 6 kí tự", null);
    } else if (valueNewPass.length > 50) {
        showError(newPassword, "Độ dài mật khẩu tối đa 50 kí tự", null);
    } else if (valueConfirmPass === "") {
        showError(confirmNewPassword, "Xác nhận mật khẩu mới", null);
    } else if (valueConfirmPass !== valueNewPass) {
        showError(confirmNewPassword, "Mật khẩu xác nhận không khớp", null);
    } else {
        fetch(URL_COVID + "/users/isCorrectPassword/?passwordResetToken=" + passwordResetToken + "&&password=" + valueCurrentdPass)
            .then(response => response.json())
            .then(data => {
                if (!data) {
                    showError(currentPassword, "Mật khẩu hiện tại không đúng", null);
                } else {
                    formId.submit();
                }
            });
    }
}