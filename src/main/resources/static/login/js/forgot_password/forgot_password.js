import { changeSubmitForm, checkRegex, elementById, showError, valueOf } from "../../../common_dashboard_login/js/function.js";
import { REGEX_EMAIL } from "../../../common_dashboard_login/js/variable.js";
import { URL_COVID } from "../../../dashboard/js/common_dashboard/variable.js";

var email;
var formForgotPassword;
var btnSubmitForgotPassword;

window.onload = (function() {
    start();

    changeSubmitForm(formForgotPassword, btnSubmitForgotPassword, validateForm);

    checkInfoMessageBeforeSubmit(infoMessage)
})();

function start() {
    email = elementById("email");
    formForgotPassword = elementById("formForgotPassword");
    btnSubmitForgotPassword = elementById("btnSubmitForgotPassword");

    email.focus();
}

function checkInfoMessageBeforeSubmit(infoMsg) {
    if (infoMsg !== null) {
        Swal.fire({
            title: 'Thông báo',
            html: infoMsg,
            icon: 'info',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Ok'
        })
    };
}

function validateForm(formId) {
    let valueEmail = valueOf(email);
    if (valueEmail === "") {
        showError(email, "Nhập địa chỉ email");
    } else if (valueEmail.length > 45) {
        showError(email, "Độ dài email tối đa 45 kí tự");
    } else if (!checkRegex(valueEmail, REGEX_EMAIL)) {
        showError(email, null, 'Vui lòng nhập đúng định dạng email' + '<br/>' + '<b>VD: demo@gmail.com.vn</b>');
    } else {
        fetch(URL_COVID + "/forgot-password/getByEmail?email=" + valueEmail)
            .then(res => res.json())
            .then(data => {
                if (!data) {
                    showError(email, "Email không tồn tại trong hệ thống");
                } else {
                    formId.submit();
                }
            })
    }
}