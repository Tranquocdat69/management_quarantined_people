import { showError, valueOf, elementById, changeSubmitForm } from "../../../common_dashboard_login/js/function.js";
import { URL_COVID } from "../common_dashboard/variable.js";

var formCreateFormType;
var submitBtn;
var formTypeName;

window.onload = (function() {
    start();

    //chuyển nút submit khi click để bắt validate
    changeSubmitForm(formCreateFormType, submitBtn, validateForm);
})();

function start() {
    formTypeName = elementById("formTypeName");
    formCreateFormType = elementById("formCreateFormType");
    submitBtn = elementById("submitBtn");

    formTypeName.focus();
};

function validateForm(formId) {
    let valueFormTypeName = valueOf(formTypeName);
    let isValidatedFisrtPhase = false;

    if (valueFormTypeName === "") {
        showError(formTypeName, 'Nhập tên loại đơn');
    } else if (valueFormTypeName.length > 255) {
        showError(formTypeName, 'Tên loại đơn không được dài quá 255 kí tự');
    } else {
        isValidatedFisrtPhase = true;
    }

    if (isValidatedFisrtPhase) {
        (function checkExistUser(param, callback) {
            let obj;
            fetch(URL_COVID + "/form-type/getByName/?name=" + param)
                .then(response => response.json())
                .then(data => {
                    if (data.formTypeName !== null) {
                        showError(formTypeName, "Đã tồn tại loại đơn này");
                    }
                    obj = data;
                })
                .then(() => callback(obj));
        })(valueFormTypeName, checkOk);

        function checkOk(data) {
            if (data.formTypeName === null) {
                formId.submit();
            }
        }
    }
}