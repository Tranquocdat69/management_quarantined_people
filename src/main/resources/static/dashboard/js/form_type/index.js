import { URL_COVID, configPagination, configSearch, ROLE_HANDLER } from "../common_dashboard/variable.js"
import { ifHasAuthority, pagination, search } from "../common_dashboard/function.js"
import { elementByClassName, elementById, showError, valueOf } from "../../../common_dashboard_login/js/function.js";

var elementOfTable;
var elementOfShowError;

var stompClient = null;
var searchInput;

window.onload = (function() {
    start();

    renderUI(URL_COVID);

    connect();
})();

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function() {
        stompClient.subscribe('/topic/getFormType', function() {
            // if (configSearch.keyword !== "") {
            //     search(configSearch);
            // }
            renderUI(URL_COVID);
        });
    });
}

function start() {
    searchInput = elementById("searchInput");
    elementOfTable = elementById("formTypeTable");
    elementOfShowError = elementById("error");

    // gán biến config phân trang
    configPagination.currentPage = 1;
    configPagination.recordPerPage = 3;
    configPagination.elementPagination = elementById("pagination");

    searchInput.addEventListener("keyup", function() {
        // gán biến config tìm kiếm
        configSearch.baseUrl = URL_COVID; // là localhost:8080
        configSearch.urlSearch = "/form-type/searchFormTypes?keyword="; // gán đường dẫn đến controller
        configSearch.keyword = valueOf(searchInput); // keyword khi người dùng nhập vào

        configSearch.paramCallback.elementTable = elementOfTable; //phần tử của table để hiển thị kết quả
        configSearch.callbackTable = renderTable; //hàm được sử dụng để render dữ liệu ra table tìm kiếm
        configSearch.callbackUI = renderUI; //hàm được sử dụng để render dữ liệu ra table khi người dùng k nhập gì ở thanh tìm kiếm

        configSearch.elementShowError = elementOfShowError; // phần tử dùng để hiển thị lỗi
        configSearch.elementPagination = configPagination.elementPagination; // phần tử hiển thị phân trang

        configSearch.errorMessage = "Không có dữ liệu khớp với từ khóa :(((("; // lỗi khi hiển thị với ng dùng

        search(configSearch);
    });
}

function renderUI(url) {
    fetch(url + "/form-type/getAllFormTypes")
        .then(response => response.json())
        .then(data => {
            if (data.length > 0) {
                configPagination.lengthArray = data.length;

                //hàm xử lí dữ liệu khi chuyển trang
                function changePage(page) {
                    renderTable(elementOfTable, ((page - 1) * configPagination.recordPerPage), (page * configPagination.recordPerPage), data)
                }

                // ae thêm cái này để khi xóa không bị lỗi trang lung tung :V
                if (configPagination.currentPage > Math.ceil(configPagination.lengthArray / configPagination.recordPerPage)) {
                    configPagination.currentPage -= 1;
                }

                changePage(configPagination.currentPage);

                configPagination.callback = changePage;
                //hàm phân trang
                // configPagination: biến cấu hình phân trang
                pagination(configPagination);

                elementOfShowError.innerHTML = "";
            } else {
                elementOfTable.innerHTML = "";
                configPagination.elementPagination.innerHTML = "";
                elementOfShowError.innerHTML = "Hiện chưa có loại đơn nào trong hệ thống :(((("
            }
        });
}

// elementTable: phần tử hiển thị table
// startIndex: bắt đầu vòng for
// lengthArray: độ dài của mảng cần lặp
// data: biến lưu dữ liệu cần render ra table
function renderTable(elementTable, startIndex, lengthArray, data) {
    let baseTable = `<thead>
                <tr>
                    <th class="border-top-0">#</th>
                    <th class="border-top-0">Tên loại đơn</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                </tbody>`;

    let str = " ";
    for (let i = startIndex; i < lengthArray && i < data.length; i++) {
        str += `
                    <tr>
                        <td>${i+1}</td>
                        <td>${data[i].formTypeName}</td>
                        <td class="text-center">
                            <a href="/form-type/detail/${data[i].formTypeId}" class="btn btn-info text-white btnDetail">Chi tiết</a>
                            <button data-value="${data[i].formTypeId}" class="btn btn-danger text-white btnDelete">Xóa</button>
                        </td>
                    </tr>
                        `;

    };

    baseTable += str;
    elementTable.innerHTML = baseTable;

    let btnDelete = elementByClassName("btnDelete");
    let btnDetail = elementByClassName("btnDetail");

    for (let j = 0; j < btnDelete.length; j++) {
        btnDelete[j].setAttribute("hidden", true);
        btnDetail[j].setAttribute("hidden", true);

        btnDelete[j].addEventListener("click", function() {
            let valueId = this.getAttribute("data-value");
            Swal.fire({
                title: 'Xác nhận',
                text: "Bạn chắc chắn xóa ?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Hủy',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Xóa'
            }).then((result) => {
                if (result.isConfirmed) {
                    fetch(URL_COVID + "/form-type/deleteFormType?id=" + valueId)
                        .then(res => res.json())
                        .then(data => {
                            if (data) {
                                renderUI(URL_COVID);
                            } else {
                                showError(null, "Không thể xóa loại đơn có dữ liệu đơn đăng ký", null);
                            }
                        });
                }
            })
        });

        if (ifHasAuthority(ROLE_HANDLER)) {
            btnDelete[j].removeAttribute("hidden");
            btnDetail[j].removeAttribute("hidden");
        }
    }
}