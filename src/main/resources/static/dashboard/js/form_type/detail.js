import { showError, valueOf, elementById, changeSubmitForm } from "../../../common_dashboard_login/js/function.js";
import { URL_COVID } from "../common_dashboard/variable.js";

var formEditFormType;
var submitBtn;
var formTypeName;

window.onload = (function() {
    start();

    //chuyển nút submit khi click để bắt validate
    changeSubmitForm(formEditFormType, submitBtn, validateForm);
})();

function start() {
    formTypeName = elementById("formTypeName");
    formEditFormType = elementById("formEditFormType");
    submitBtn = elementById("submitBtn");
};

function validateForm(formId) {
    let valueFormTypeName = valueOf(formTypeName);
    let isValidatedFisrtPhase = false;

    if (valueFormTypeName === "") {
        showError(formTypeName, 'Nhập tên loại đơn');
    } else if (valueFormTypeName.length > 255) {
        showError(formTypeName, 'Tên loại đơn không được dài quá 255 kí tự');
    } else {
        isValidatedFisrtPhase = true;
    }

    if (isValidatedFisrtPhase) {
        (function checkExistFormType(currentParam, newParam, callback) {
            let obj;
            fetch(URL_COVID + "/form-type/checkDuplicateFormType/?currentFormType=" + currentParam + "&&newFormType=" + newParam)
                .then(response => response.json())
                .then(data => {
                    if (data.formTypeName !== null) {
                        showError(formTypeName, "Đã tồn tại loại đơn này");
                    }
                    obj = data;
                })
                .then(() => callback(obj));
        })(currentFormTypeName, valueFormTypeName, checkOk);

        function checkOk(data) {
            if (data.formTypeName === null) {
                Swal.fire({
                    title: 'Xác nhận',
                    text: "Bạn chắc chắn sửa thông tin loại đơn này chứ ?",
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Hủy',
                    confirmButtonText: 'Sửa'
                }).then((result) => {
                    if (result.isConfirmed) {
                        formId.submit();
                    }
                })
            }
        }
    }
}