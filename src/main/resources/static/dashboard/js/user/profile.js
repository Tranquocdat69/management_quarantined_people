import { changeSubmitForm, elementById, valueOf, showError } from "../../../common_dashboard_login/js/function.js";
import { setAvatar, previewImageBeforeUpload } from "../common_dashboard/function.js";
import { URL_COVID, URL_IMAGE, URL_IMAGE_DEFAULT } from "../common_dashboard/variable.js";

var profileAvatar;
var fullName;
var phoneNumber;
var formEditProfile;
var btnProfileSubmit;
var chooseImage;
var previewImage;
var valueNameAvatar;
var nameOfAvatar;
var stompClient = null;
var username;
var email;

window.onload = (function() {
    start();

    changeSubmitForm(formEditProfile, btnProfileSubmit, validateForm);

    renderUI(URL_COVID);

    if (valueNameAvatar === "") {
        previewImageBeforeUpload(chooseImage, previewImage, URL_IMAGE_DEFAULT)
    } else {
        previewImageBeforeUpload(chooseImage, previewImage, URL_IMAGE + valueNameAvatar)
    };

    connect();

})();

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function() {
        stompClient.subscribe('/topic/getUser/' + loggedUsername.textContent, function() {
            renderUI(URL_COVID);
        });
    });
}

function start() {
    profileAvatar = elementById("profileAvatar");
    fullName = elementById("fullName");

    phoneNumber = elementById("phoneNumber");
    formEditProfile = elementById("formEditProfile");
    btnProfileSubmit = elementById("btnProfileSubmit");
    chooseImage = elementById("chooseImage");
    previewImage = elementById("profileAvatar");
    nameOfAvatar = elementById("currentAvatar");
    username = elementById("profileUsername");
    email = elementById("profileEmail");

    valueNameAvatar = nameOfAvatar.textContent;
}

function renderUI(url) {
    fetch(url + "/current-user/getCurrentLoggedUser")
        .then(res => res.json())
        .then(data => {
            setAvatar(profileAvatar, data.avatar, URL_IMAGE, URL_IMAGE_DEFAULT);
            fullName.value = data.fullname;
            phoneNumber.value = data.phoneNumber;
            email.textContent = data.email;
        });
}


function validateForm(formId) {
    let valueFullname = valueOf(fullName);
    let valuePhone = valueOf(phoneNumber);
    let isUploadFileOk = false;

    if (valueFullname === "") {
        showError(fullName, 'Nhập họ và tên');
    } else if (valueFullname.length > 100) {
        showError(fullName, 'Họ và tên tối đa 100 kí tự');
    } else if (valuePhone === "") {
        showError(phoneNumber, "Nhập số điện thoại");
    } else if (valuePhone.length >= 11 || valuePhone.length <= 9) {
        showError(phoneNumber, null, "Số điện thoại phải là 10 chữ số" + "<br/>" + "Độ dài hiện tại: " + valuePhone.length);
    } else if (chooseImage.files.length > 0) {
        const fsize = chooseImage.files.item(0).size;
        const file = Math.round((fsize / 1024));
        const value = chooseImage.value;
        const extension = value.substring(value.lastIndexOf('.') + 1).toLowerCase();
        if (extension != "png" && extension != "jpeg" && extension != "jpg") {
            showError(chooseImage, "Chỉ chọn file có định dạng jpeg,jpg,png");
            isUploadFileOk = false;
        } else {
            if (file > 5120) {
                showError(chooseImage, null, "Chọn file ảnh không quá kích thước 5 MB <br> Kích thước file hiện tại: " + Math.round((file / 1024)) + " MB");
                isUploadFileOk = false;
            } else {
                isUploadFileOk = true;
            }
        }
    } else {
        isUploadFileOk = true;
    }

    if (isUploadFileOk) {
        (function checkPhoneExist(currentParam, newParam, callback) {
            let obj;
            fetch(URL_COVID + "/current-user/checkDuplicatePhoneNumber/?currentPhoneNumber=" + currentParam + "&&newPhoneNumber=" + newParam)
                .then(response => response.json())
                .then(data => {
                    if (data.username !== null) {
                        showError(phoneNumber, "Đã tồn tại số điện thoại");
                    }
                    obj = data;
                })
                .then(() => callback(obj));
        })(currentPhoneNumber, valuePhone, checkOk);

        function checkOk(data) {
            if (data.username === null) {
                formId.submit();
            }
        }
    }
}