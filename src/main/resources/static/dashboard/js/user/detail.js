import { showError, valueOf, checkRegex, elementById, elementByName, changeSubmitForm } from "../../../common_dashboard_login/js/function.js";
import { REGEX_EMAIL, REGEX_USERNAME } from "../../../common_dashboard_login/js/variable.js";
import { previewImageBeforeUpload, setAvatar } from "../common_dashboard/function.js";
import { URL_COVID, URL_IMAGE, URL_IMAGE_DEFAULT } from "../common_dashboard/variable.js";

var chooseImage;
var previewImage;
var formCreateUser;
var submitBtn;
var fullName;
var email;
var username;
var phoneNumber;
var checkBoxes;
var nameOfAvatar;
var valueNameAvatar;
var selectArea;

window.onload = (function() {
    start();

    //chuyển nút submit khi click để bắt validate
    changeSubmitForm(formCreateUser, submitBtn, validateForm);

    //xem trước ảnh trước khi upload lên server
    if (valueNameAvatar === "") {
        previewImageBeforeUpload(chooseImage, previewImage, URL_IMAGE_DEFAULT)
    } else {
        previewImageBeforeUpload(chooseImage, previewImage, URL_IMAGE + valueNameAvatar)
    }

    setAvatar(previewImage, valueNameAvatar, URL_IMAGE, URL_IMAGE_DEFAULT);
})();

function start() {
    fullName = elementById("fullname");
    email = elementById("email");
    username = elementById("username");
    fullName = elementById("fullname");
    phoneNumber = elementById("phoneNumber");
    chooseImage = elementById("chooseImage");
    formCreateUser = elementById("formCreateUser");
    submitBtn = elementById("submitBtn");
    previewImage = elementById("previewImage");
    checkBoxes = elementByName("roles");

    nameOfAvatar = elementById("nameOfAvatar");
    valueNameAvatar = nameOfAvatar.textContent;
    selectArea = elementById("areas");

    console.log(currentUsername, currentPhoneNumber, currentEmail);
};

function validateForm(formId) {
    let isChecked = false;

    for (var checkbox of checkBoxes) {
        if (checkbox.checked) {
            isChecked = true;
            break;
        }
    }

    let valueFullname = valueOf(fullName);
    let valueUsername = valueOf(username);
    let valueEmail = valueOf(email);
    let valuePhone = valueOf(phoneNumber);
    let isUploadFileOk = true;
    let isValidatedFisrtPhase = false;

    if (valueFullname === "") {
        showError(fullName, 'Nhập họ và tên');
    } else if (valueFullname.length > 100) {
        showError(fullName, 'Họ và tên tối đa 100 kí tự');
    } else if (valueUsername === "") {
        showError(username, 'Nhập tên đăng nhập');
    } else if (!checkRegex(valueUsername, REGEX_USERNAME)) {
        showError(username, 'Tên đăng nhập bắt đầu chứ cái, không chứa kí tự đặc biệt và khoảng cách');
    } else if (valueUsername.length > 45) {
        showError(username, 'Tên đăng nhập tối đa 45 kí tự');
    } else if (valueEmail === "") {
        showError(email, "Nhập địa chỉ email");
    } else if (valueEmail.length > 45) {
        showError(email, "Độ dài email tối đa 45 kí tự");
    } else if (!checkRegex(valueEmail, REGEX_EMAIL)) {
        showError(email, null, 'Vui lòng nhập đúng định dạng email' + '<br/>' + '<b>VD: demo@gmail.com.vn</b>');
    } else if (chooseImage.files.length > 0) {
        const fsize = chooseImage.files.item(0).size;
        const file = Math.round((fsize / 1024));
        const value = chooseImage.value;
        const extension = value.substring(value.lastIndexOf('.') + 1).toLowerCase();
        if (extension != "png" && extension != "jpeg" && extension != "jpg") {
            showError(chooseImage, "Chỉ chọn file có định dạng jpeg,jpg,png");
            isUploadFileOk = false;
        } else {
            if (file > 5120) {
                showError(chooseImage, null, "Chọn file ảnh không quá kích thước 5 MB <br> Kích thước file hiện tại: " + Math.round((file / 1024)) + " MB");
                isUploadFileOk = false;
            } else {
                isUploadFileOk = true;
                isValidatedFisrtPhase = true;
            }
        }
    } else {
        isValidatedFisrtPhase = true;
    }

    if (isUploadFileOk && isValidatedFisrtPhase) {
        if (valuePhone === "") {
            showError(phoneNumber, "Nhập số điện thoại");
        } else if (valuePhone.length >= 11 || valuePhone.length <= 9) {
            showError(phoneNumber, null, "Số điện thoại phải là 10 chữ số" + "<br/>" + "Độ dài hiện tại: " + valuePhone.length);
        } else if (!isChecked) {
            showError(null, "Chọn vai trò người dùng")
        } else {
            if (selectArea.value === "") {
                showError(selectArea, "Chọn khu vực bàn giao cho người dùng")
            } else {
                (function checkExistUser(currentParam, newParam, callback) {
                    let obj;
                    fetch(URL_COVID + "/users/checkDuplicateUsername/?currentUsername=" + currentParam + "&&newUsername=" + newParam)
                        .then(response => response.json())
                        .then(data => {
                            if (data.username !== null) {
                                showError(username, "Đã tồn tại tên đăng nhập");
                            }
                            obj = data;
                        })
                        .then(() => callback(obj));
                })(currentUsername, valueUsername, getResultUsername);

                function getResultUsername(data) {
                    if (data.username === null) {
                        checkEmailExist(currentEmail, valueEmail, getResultEmail);
                    }
                }

                function checkEmailExist(currentParam, newParam, callback) {
                    let obj;
                    fetch(URL_COVID + "/users/checkDuplicateEmail/?currentEmail=" + currentParam + "&&newEmail=" + newParam)
                        .then(response => response.json())
                        .then(data => {
                            if (data.username !== null) {
                                showError(email, "Đã tồn tại email");
                            }
                            obj = data;
                        })
                        .then(() => callback(obj));
                }

                function getResultEmail(data) {
                    if (data.username === null) {
                        checkPhoneExist(currentPhoneNumber, valuePhone, checkOk);
                    }
                }

                function checkPhoneExist(currentParam, newParam, callback) {
                    let obj;
                    fetch(URL_COVID + "/users/checkDuplicatePhoneNumber/?currentPhoneNumber=" + currentParam + "&&newPhoneNumber=" + newParam)
                        .then(response => response.json())
                        .then(data => {
                            if (data.username !== null) {
                                showError(phoneNumber, "Đã tồn tại số điện thoại");
                            }
                            obj = data;
                        })
                        .then(() => callback(obj));
                }

                function checkOk(data) {
                    if (data.username === null) {
                        Swal.fire({
                            title: 'Xác nhận',
                            text: "Bạn chắc chắn sửa thông tin người dùng này chứ ?",
                            icon: 'info',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            cancelButtonText: 'Hủy',
                            confirmButtonText: 'Sửa'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                formId.submit();
                            }
                        })
                    }
                }
            }
        }
    }
}