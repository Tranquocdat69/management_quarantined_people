import { changeSubmitForm, elementById, showError, valueOf, showHidePassword } from "../../../common_dashboard_login/js/function.js";
import { URL_COVID } from "../common_dashboard/variable.js";

var formChangePassword;
var currentPassword;
var newPassword;
var confirmNewPassword;
var btnSubmitChangePassword;
var userId;

var iconShowCurrentPass;
var iconShowNewPass;
var iconShowConfirmNewPass;

window.onload = (function() {
    start();

    changeSubmitForm(formChangePassword, btnSubmitChangePassword, validateForm);
})();

function start() {
    formChangePassword = elementById("formChangePassword");
    currentPassword = elementById("currentPassword");
    newPassword = elementById("newPassword");
    confirmNewPassword = elementById("confirmNewPassword");
    userId = elementById("userId");
    btnSubmitChangePassword = elementById("btnSubmitChangePassword");

    currentPassword.focus();

    iconShowCurrentPass = elementById("iconShowCurrentPass");
    iconShowNewPass = elementById("iconShowNewPass");
    iconShowConfirmNewPass = elementById("iconShowConfirmNewPass");

    showHidePassword(iconShowCurrentPass, currentPassword);
    showHidePassword(iconShowNewPass, newPassword);
    showHidePassword(iconShowConfirmNewPass, confirmNewPassword);
}

function validateForm(formId) {
    let valueCurrentdPass = valueOf(currentPassword);
    let valueNewPass = valueOf(newPassword);
    let valueConfirmPass = valueOf(confirmNewPassword);
    let valueUserId = valueOf(userId);

    if (valueCurrentdPass === "") {
        showError(currentPassword, "Nhập mật khẩu hiện tại", null);
    } else if (valueNewPass === "") {
        showError(newPassword, "Nhập mật khẩu mới", null);
    } else if (valueNewPass === valueCurrentdPass) {
        showError(newPassword, "Mật khẩu mới phải khác mật khẩu cũ", null);
    } else if (valueNewPass.length < 6) {
        showError(newPassword, "Độ dài mật khẩu tối thiểu 6 kí tự", null);
    } else if (valueNewPass.length > 50) {
        showError(newPassword, "Độ dài mật khẩu tối đa 50 kí tự", null);
    } else if (valueConfirmPass === "") {
        showError(confirmNewPassword, "Xác nhận mật khẩu mới", null);
    } else if (valueConfirmPass !== valueNewPass) {
        showError(confirmNewPassword, "Mật khẩu xác nhận không khớp", null);
    } else {
        fetch(URL_COVID + "/current-user/isCorrectPassword/?currentPassword=" + valueCurrentdPass)
            .then(response => response.json())
            .then(data => {
                if (!data) {
                    showError(currentPassword, "Mật khẩu hiện tại không đúng", null);
                } else {
                    Swal.fire({
                        title: 'Xác nhận',
                        text: "Bạn chắc chắn muốn thay đổi mật khẩu ?",
                        icon: 'info',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Hủy',
                        confirmButtonText: 'Đổi mật khẩu'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            formId.submit();
                        }
                    })
                }
            });
    }
}