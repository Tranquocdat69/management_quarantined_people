import { elementById, valueOf, elementByClassName, showError } from "../../../common_dashboard_login/js/function.js";
import { ifHasAuthority, setAvatar } from "../common_dashboard/function.js";
import { ROLE_ADDER, URL_COVID, URL_IMAGE, URL_IMAGE_DEFAULT } from "../common_dashboard/variable.js";

var formType;
var content;
var quarantinedInformation;
var verificationResult;
var creator;
var verifier;
var createdDate;
var verifiedDate;
var district;
var ward;
var addressDetail;
var workAddress;
var gender;
var id;
var valueId;
var quarantinedInformation;
var error;
var btnCreate;
var stompClient = null;

window.onload = (function() {
    start();

    renderUI(URL_COVID);

    // connect();
})();

// function connect() {
//     var socket = new SockJS('/ws');
//     stompClient = Stomp.over(socket);
//     stompClient.connect({}, function() {
//         stompClient.subscribe('/topic/getQuarantinedInformation/' + quarantinedPeopleId, function() {
//             setTimeout(function() {
//                 getQuarantinedInformation(URL_COVID, quarantinedPeopleId);
//             }, 400);
//         });
//         stompClient.subscribe('/topic/getQuarantinedPeople/' + quarantinedPeopleId, function() {
//             renderUI(URL_COVID);
//         });
//     });
// }

function start() {
    formType = elementById("formType");
    content = elementById("content");
    quarantinedInformation = elementById("quarantinedInformation");
    verificationResult = elementById("verificationResult");
    id = elementById("id");
    valueId = valueOf(id);

    creator = elementById("creator");
    verifier = elementById("verifier");
    createdDate = elementById("createdDate");
    verifiedDate = elementById("verifiedDate");
}

function renderUI(url) {
    fetch(url + "/application-form/informationApplicationForm?id=" + valueId)
        .then(res => res.json())
        .then(data => {
            formType.textContent = data.formType.formTypeName;
            content.textContent = data.content;
            content.setAttribute("href", "/dashboard/plugins/application_form/uploads/" + data.content);
            quarantinedInformation.textContent = data.quarantinedInformation.quarantinedPeople.fullname;
            verificationResult.textContent = data.verificationResult === 0 ? "Đang chờ" : data.verificationResult === 1 ? "Chấp thuận" : "Bác bỏ";
            creator.textContent = data.creator.fullname;
            verifier.textContent = data.verifier === null ? "Chưa xác nhận" : data.verifier.fullname;
            createdDate.textContent = data.createdDate;
            verifiedDate.textContent = data.verifiedDate === null ? "Chưa xác nhận" : data.verifiedDate;
        })
}