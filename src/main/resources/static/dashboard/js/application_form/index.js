import { URL_COVID, ROLE_HANDLER } from "../common_dashboard/variable.js"
import { ifHasAuthority } from "../common_dashboard/function.js"
import { elementByClassName, elementById, showError } from "../../../common_dashboard_login/js/function.js";

var applicationForm;
var errorForm;
var btnCreateForm;

var stompClient = null;
// var tabForm = elementById("application-tab");

window.onload = (function() {
    // if (tabForm.classList.contains("active")) {
        reloadTabForm();
    // }
    // tabForm.addEventListener("click", function() {
        // reloadTabForm();
    // });
})();

function reloadTabForm() {
    startForm();

    renderUIForm(URL_COVID);

    connectForm();
}

function connectForm() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function() {
        stompClient.subscribe('/topic/getApplicationForm/' + quarantinedInformationId, function() {
            setTimeout(function() {
                renderUIForm(URL_COVID);
            }, 200);
        });
    });
}

function startForm() {
    applicationForm = elementById("applicationForm");
    errorForm = elementById("errorForm");
    btnCreateForm = elementById("btnCreateForm");

    // gán biến config phân trang
    // configPagination.currentPage = 1;
    // configPagination.recordPerPage = 1;
    // configPagination.elementPagination = elementById("paginationForm");
}

function renderUIForm(url) {
    if (!ifHasAuthority(ROLE_HANDLER)) {
        btnCreateForm.style.display = 'none';
    } else {
        fetch(URL_COVID + "/quarantined-information/checkQuarantinedInformationNotCompleteById?quarantinedInformationId=" + quarantinedInformationId)
            .then(response => response.json())
            .then(data => {
                if (data) {
                    btnCreateForm.style.display = 'none';
                } else {
                    btnCreateForm.style.display = 'inline-block';
                    btnCreateForm.addEventListener("click", function() {
                        window.location.href = URL_COVID + "/application-form/create-application-form/" + quarantinedPeopleId;
                    });
                }
            });
    }

    fetch(url + "/application-form/getAllApplicationForms?quarantinedInformationId=" + quarantinedInformationId)
        .then(response => response.json())
        .then(data => {
            if (data.length > 0) {
                // configPagination.lengthArray = data.length;

                //hàm xử lí dữ liệu khi chuyển trang
                // function changePageForm(page) {
                renderTableForm(applicationForm, 0, data.length, data);
                // }

                // ae thêm cái này để khi xóa không bị lỗi trang lung tung :V
                // if (configPagination.currentPage > Math.ceil(configPagination.lengthArray / configPagination.recordPerPage)) {
                    // configPagination.currentPage -= 1;
                // }

                // changePageForm(configPagination.currentPage);

                // configPagination.callback = changePageForm;
                //hàm phân trang
                // configPagination: biến cấu hình phân trang
                // pagination(configPagination);

                errorForm.innerHTML = "";
            } else {
                applicationForm.innerHTML = "";
                // configPagination.elementPagination.innerHTML = "";
                errorForm.innerHTML = "Lần cách ly này hiện chưa có đơn nào"
            }
        });
}

// elementTable: phần tử hiển thị table
// startIndex: bắt đầu vòng for
// lengthArray: độ dài của mảng cần lặp
// data: biến lưu dữ liệu cần render ra table
function renderTableForm(elementTable, startIndex, lengthArray, data) {
    let baseTable = `<thead>
                <tr>
                    <th class="border-top-0">#</th>
                    <th class="border-top-0">Loại đơn</th>
                    <th class="border-top-0">Nội dung</th>
                    <th class="border-top-0">Kết quả xác nhận</th>
                </tr>
                </thead>
                <tbody>
                </tbody>`;

    let str = " ";
    let str1 = " ";
    let str2 = " ";
    let str3 = " ";

    for (let i = startIndex; i < lengthArray && i < data.length; i++) {
        str1 = `
            <tr>
                <td>
                    <a href="/application-form/information/${data[i].applicationFormId}" class="informationUrl">Đơn ${i+1}</a>
                </td>
                <td>${data[i].formType.formTypeName}</td>
                <td>
                    <a href="${URL_COVID}/dashboard/plugins/application_form/uploads/${data[i].content}" class="fileUrl">
                        ${data[i].content}
                    </a>
                </td>
                <td>${(data[i].verificationResult === 0) ? "Đang chờ" : (data[i].verificationResult === 1) ? "Chấp thuận" : "Bác bỏ"}</td>
                `;

        if (data[i].verificationResult === 0) {
            str2 = `
                <td>
                    <select class="form-select verifyResult" data-value-id="${data[i].applicationFormId}" aria-label="Default select example">
                        <option value="0" selected>Đang chờ</option>
                        <option value="1">Chấp thuận</option>
                        <option value="2">Bác bỏ</option>
                    </select>
                </td>
                `;
        } else {
            str2 = "<td></td>";
        }

        str3 = `
                <td class="text-end">
                    <a data-value-id="${data[i].applicationFormId}" data-value-result="${data[i].verificationResult}" class="btn btn-info text-white btnEditForm">Sửa</a>
                    <button data-value="${data[i].applicationFormId}" class="btn btn-danger text-white btnDeleteForm">Xóa</button>
                </td>
            </tr>
                `;

        str += (str1 + str2 + str3);
    }

    baseTable += str;
    elementTable.innerHTML = baseTable;

    let btnDeleteForm = elementByClassName("btnDeleteForm");
    let btnEditForm = elementByClassName("btnEditForm");
    let verifyResult = elementByClassName("verifyResult");
    let fileUrl = elementByClassName("fileUrl");
    let informationUrl = elementByClassName("informationUrl");

    for (let j = 0; j < btnDeleteForm.length; j++) {
        if (!ifHasAuthority(ROLE_HANDLER)) {
            btnDeleteForm[j].style.display = 'none';
            fileUrl[j].removeAttribute("href");
            fileUrl[j].style.color = '#3e5569';
            informationUrl[j].removeAttribute("href");
            informationUrl[j].style.color = '#3e5569';
        } else {
            btnDeleteForm[j].addEventListener("click", function() {
                let valueId = this.getAttribute("data-value");
                Swal.fire({
                    title: 'Xác nhận',
                    text: "Bạn chắc chắn xóa ?",
                    icon: 'info',
                    showCancelButton: true,
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Hủy',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Xóa'
                }).then((result) => {
                    if (result.isConfirmed) {
                        deleteApplicationFormById(valueId, renderUIForm, quarantinedInformationId);
                    }
                })
            });
        }
    }

    function deleteApplicationFormById(id, callbackUI, qInformationId) {
        fetch(URL_COVID + "/application-form/deleteApplicationForm?id=" + id + "&quarantinedInformationId=" + qInformationId)
            .then(res => res.json())
            .then(data => {
                if (data) {
                    callbackUI(URL_COVID);
                } else {
                    showError(null, "Đơn này không tồn tại trong hệ thống", null);
                }
            });
    }

    fetch(URL_COVID + "/quarantined-information/getQuarantinedInformationyById?id=" + quarantinedInformationId)
        .then(res => res.json())
        .then(data => {
            for (let k = 0; k < btnEditForm.length; k++) {
                if (!ifHasAuthority(ROLE_HANDLER)) {
                    btnEditForm[k].style.display = 'none';
                } else {
                    let dataValueId = btnEditForm[k].getAttribute("data-value-id");
                    let dataValueResult = btnEditForm[k].getAttribute("data-value-result");
                    if (data.status !== 0 || parseInt(dataValueResult) !== 0) {
                        btnEditForm[k].setAttribute("hidden", true);
                    }
                    btnEditForm[k].addEventListener("click", function() {
                        if (data.status === 0 && parseInt(dataValueResult) === 0) {
                            window.location.href = URL_COVID + "/application-form/detail/" + dataValueId;
                        } else {
                            showError(null, "Không thể sửa đổi đơn này");
                        }
                    });
                }
            }
        });

    for (let m = 0; m < verifyResult.length; m++) {
        if (!ifHasAuthority(ROLE_HANDLER)) {
            verifyResult[m].style.display = 'none';
        } else {
            verifyResult[m].addEventListener("change", function() {
                let dataValueId = this.getAttribute("data-value-id");
                let dataValueResult = this.value;
                Swal.fire({
                    title: 'Xác nhận',
                    html: "Bạn chắc chắn <b class='text-lowercase'>" + this.options[this.selectedIndex].text + "</b> đơn này chứ ?",
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Hủy',
                    confirmButtonText: 'Xác nhận'
                }).then((result) => {
                    if (result.isConfirmed) {
                        fetch(URL_COVID + "/application-form/verifyApplicationForm?applicationFormId=" + dataValueId + "&verificationResult=" + dataValueResult)
                            .then(res => res.json())
                            .then(data => {
                                Swal.fire({
                                    title: 'Xong!',
                                    text: 'Xác nhận đơn thành công',
                                    html: null,
                                    icon: 'success',
                                    confirmButtonText: 'OK'
                                });
                            });
                    }
                })
            });
        }
    }
}