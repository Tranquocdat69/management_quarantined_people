import { changeSubmitForm, elementById, showError, valueOf } from "../../../common_dashboard_login/js/function.js";
import { URL_COVID } from "../common_dashboard/variable.js";

var formType;
var formEditApplicationForm;
var submitFormEditApplicationForm;
var currentFormTypeId;
var uploadedFile;
var nameOfFile;
var valueNameFile;
var chooseFile;

window.onload = (function() {
    start();

    renderRooms(formType);

    changeSubmitForm(formEditApplicationForm, submitFormEditApplicationForm, validateForm);
})();

function start() {

    currentFormTypeId = currentFormType.formTypeId;

    formType = elementById("formType");
    uploadedFile = elementById("uploadedFile");
    chooseFile = elementById("chooseFile");
    nameOfFile = elementById("nameOfFile");
    valueNameFile = nameOfFile.textContent;

    uploadedFile.setAttribute("href", URL_COVID + "/dashboard/plugins/application_form/uploads/" + valueNameFile);
    uploadedFile.innerHTML = valueNameFile;

    formEditApplicationForm = elementById("formEditApplicationForm");
    submitFormEditApplicationForm = elementById("submitFormEditApplicationForm");
}

function renderRooms(formTypeSelect) {
    fetch(URL_COVID + "/form-type/getAllFormTypes")
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                let str = "";
                for (let i = 0; i < data.length; i++) {
                    if (data[i].formTypeId == currentFormTypeId) {
                        str += `<option selected value="${data[i].formTypeId}">${data[i].formTypeName}</option>`;
                    } else {
                        str += `<option value="${data[i].formTypeId}">${data[i].formTypeName}</option>`;
                    }
                }
                formTypeSelect.innerHTML = str;
            } else {
                formTypeSelect.innerHTML = "";
            }
        });
}

function validateForm(formId) {
    if (chooseFile.files.length > 0) {
        const fsize = chooseFile.files.item(0).size;
        const file = Math.round((fsize / 1024));
        const value = chooseFile.value;
        const extension = value.substring(value.lastIndexOf('.') + 1).toLowerCase();
        if (extension != "docx") {
            showError(chooseFile, "Chỉ chọn file có định dạng docx");
        } else {
            if (file > 5120) {
                showError(chooseFile, null, "Chọn file không quá kích thước 5 MB <br> Kích thước file hiện tại: " + Math.round((file / 1024)) + " MB");
            } else {
                formId.submit();
            }
        }
    } else {
        Swal.fire({
            title: 'Xác nhận',
            text: "Bạn chắc chắn sửa thông tin đơn này chứ ?",
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Hủy',
            confirmButtonText: 'Sửa'
        }).then((result) => {
            if (result.isConfirmed) {
                formId.submit();
            }
        })
    }
}