import { showError, elementById, changeSubmitForm } from "../../../common_dashboard_login/js/function.js";
import { URL_COVID } from "../common_dashboard/variable.js";

var formCreateApplicationForm;
var submitformCreateApplicationForm;
var chooseFile;
var formTypeSelect;

window.onload = (function() {
    start();

    renderFormType(formTypeSelect);

    //chuyển nút submit khi click để bắt validate
    changeSubmitForm(formCreateApplicationForm, submitformCreateApplicationForm, validateForm);
})();

function start() {
    chooseFile = elementById("chooseFile");
    formCreateApplicationForm = elementById("formCreateApplicationForm");
    submitformCreateApplicationForm = elementById("submitformCreateApplicationForm");
    formTypeSelect = elementById("formType");
}

function validateForm(formId) {
    if (chooseFile.files.length > 0) {
        const fsize = chooseFile.files.item(0).size;
        const file = Math.round((fsize / 1024));
        const value = chooseFile.value;
        const extension = value.substring(value.lastIndexOf('.') + 1).toLowerCase();
        if (extension != "docx") {
            showError(chooseFile, "Chỉ chọn file có định dạng docx");
        } else {
            if (file > 5120) {
                showError(chooseFile, null, "Chọn file không quá kích thước 5 MB <br> Kích thước file hiện tại: " + Math.round((file / 1024)) + " MB");
            } else {
                formId.submit();
            }
        }
    } else {
        showError(chooseFile, "Vui lòng chọn file để tải lên");
    }
}

function renderFormType(formTypeSelect) {
    fetch(URL_COVID + "/form-type/getAllFormTypes")
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                let str = "";
                for (let i = 0; i < data.length; i++) {
                    // if (i === 0) {
                    //     str += `<option selected value="${data[i].room_id}">${data[i].room_name}</option>`;
                    // } else {
                    // }
                    str += `<option value="${data[i].formTypeId}">${data[i].formTypeName}</option>`;
                }
                formTypeSelect.innerHTML = str;
            } else {
                formTypeSelect.innerHTML = "";
            }
        });
}