import { URL_COVID } from "../common_dashboard/variable.js";
import { showError, valueOf, checkRegex, elementById, elementByName, changeSubmitForm } from "../../../common_dashboard_login/js/function.js";


var formEditTestType;
var submitBtn;
var testTypeName;

var currentTestTypeName;



window.onload = (function() {
    start();

    changeSubmitForm(formEditTestType, submitBtn, validateForm);
})();

function start() {
    formEditTestType = elementById("formEditTestType");
    submitBtn = elementById("submitBtn");
    testTypeName = elementById("TestType");

    currentTestTypeName = valueOf(testTypeName);

}

function validateForm(formId) {
    let valueTestTypeName = valueOf(testTypeName);


    if (valueTestTypeName === "") {
        showError(testTypeName, 'Nhập loại xét nghiệm');
    } else if (valueTestTypeName.length > 255) {
        showError(testTypeName, 'Loại xét nghiệm không quá 255 ký tự');
    } else {
        fetch(URL_COVID + "/test-type/CheckExsitEditTestType?currentName=" + currentTestTypeName + "&newName=" + valueTestTypeName)
            .then(res => res.json())
            .then(data => {
                if (data) {
                    showError(testTypeName, "Loại xét nghiệm đã tồn tại");
                } else {
                    formId.submit();
                }
            })
    }
}