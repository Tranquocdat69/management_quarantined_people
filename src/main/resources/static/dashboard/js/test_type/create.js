import { URL_COVID } from "../common_dashboard/variable.js";
import { showError, valueOf, checkRegex, elementById, elementByName, changeSubmitForm } from "../../../common_dashboard_login/js/function.js";


var formCreateTestType;
var submitBtn;
var TypeName;



window.onload = (function() {
    start();

    changeSubmitForm(formCreateTestType, submitBtn, validateForm);
})();

function start() {
    formCreateTestType = elementById("formCreateTestType");
    submitBtn = elementById("submitBtn");
    TypeName = elementById("TestType");


}

function validateForm(formId) {

    let valueTestTypeName = valueOf(TypeName);



    if (valueTestTypeName === "") {
        showError(TypeName, 'Nhập loại xét nghiệm');
    } else if (valueTestTypeName.length > 255) {
        showError(TypeName, 'Loại xét nghiệm không quá 255 ký tự');
    } else {
        fetch(URL_COVID + "/test-type/CheckExsitTestType?testType=" + valueTestTypeName)
            .then(res => res.json())
            .then(data => {
                if (data) {
                    showError(TypeName, "Loại xét nghiệm đã tồn tại");
                } else {
                    formId.submit();
                }
            })
    }
}