import { elementById, valueOf, elementByClassName, showError } from "../../../common_dashboard_login/js/function.js";
import { ifHasAuthority, setAvatar } from "../common_dashboard/function.js";
import { ROLE_ADDER, URL_COVID, URL_IMAGE, URL_IMAGE_DEFAULT } from "../common_dashboard/variable.js";

var profileAvatar;
var fullname;
var phoneNumber;
var email;
var nationality;
var identityCard;
var dateOfBirth;
var province;
var district;
var ward;
var addressDetail;
var workAddress;
var gender;
var id;
var valueId;
var quarantinedInformation;
var error;
var btnCreate;
var stompClient = null;

window.onload = (function() {
    start();

    renderUI(URL_COVID);

    connect();
})();

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function() {
        stompClient.subscribe('/topic/getQuarantinedInformation/' + quarantinedPeopleId, function() {
            setTimeout(function() {
                getQuarantinedInformation(URL_COVID, quarantinedPeopleId);
            }, 400);
        });
        stompClient.subscribe('/topic/getQuarantinedPeople/' + quarantinedPeopleId, function() {
            renderUI(URL_COVID);
        });
    });
}

function start() {
    profileAvatar = elementById("profileAvatar");
    fullname = elementById("fullname");
    phoneNumber = elementById("phoneNumber");
    email = elementById("email");
    id = elementById("id");
    valueId = valueOf(id);

    nationality = elementById("nationality");
    identityCard = elementById("identityCard");
    dateOfBirth = elementById("dateOfBirth");
    province = elementById("province");
    district = elementById("district");
    ward = elementById("ward");
    addressDetail = elementById("addressDetail");
    workAddress = elementById("workAddress");
    gender = elementById("gender");
    error = elementById("error");
    btnCreate = elementById("btnCreate");
    quarantinedInformation = elementById("quarantinedInformation");
}

function renderUI(url) {
    fetch(url + "/quarantined-people/detailQuarantinedPeople?id=" + valueId)
        .then(res => res.json())
        .then(data => {
            setAvatar(profileAvatar, data.avatar, URL_IMAGE, URL_IMAGE_DEFAULT);
            fullname.textContent = data.fullname;
            nationality.textContent = data.nationality;
            province.textContent = data.province;
            district.textContent = data.district;
            ward.textContent = data.ward;
            workAddress.textContent = data.workAddress;
            addressDetail.textContent = data.addressDetail;
            phoneNumber.textContent = data.phoneNumber;
            gender.textContent = data.gender ? "Nam" : "Nữ";
            dateOfBirth.textContent = data.dateOfBirth;
            identityCard.textContent = data.identityCard;
            email.textContent = data.email;

            getQuarantinedInformation(url, data.id);
        })
}

function getQuarantinedInformation(url, quarantinedPeopleId) {
    fetch(url + "/quarantined-information/getQuarantinedInformationyQuarantinedPeopleId?quarantinedPeopleId=" + quarantinedPeopleId)
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                renderTable(quarantinedInformation, 0, data.length, data);
                error.innerHTML = "";
                if (btnCreate) {
                    btnCreate.addEventListener("click", function() {
                        fetch(URL_COVID + "/quarantined-information/checkQuarantinedInformationNotComplete?quarantinedPeopleId=" + valueId)
                            .then(res => res.json())
                            .then(data => {
                                if (data) {
                                    window.location.href = URL_COVID + "/quarantined-information/create-quarantined-information/" + valueId;
                                } else {
                                    showError(null, "Hiện người cách ly chưa hoàn thành thời gian cách ly");
                                }
                            })
                    });
                }
            } else {
                window.location.href = URL_COVID + "/quarantined-people";
            }
        })
}

function renderTable(elementTable, startIndex, lengthArray, data) {
    let baseTable = `<thead>
                <tr>
                    <th class="border-top-0">Số lần</th>
                    <th class="border-top-0">Cấp độ</th>
                    <th class="border-top-0">Ngày đến</th>
                    <th class="border-top-0">Ngày hoàn thành cách ly</th>
                    <th class="border-top-0">Trạng thái</th>
                </tr>
                </thead>
                <tbody>
                </tbody>`;

    let str = " ";
    for (let i = startIndex; i < lengthArray && i < data.length; i++) {
        str += `
                    <tr>
                        <td>
                            <a href="${URL_COVID}/quarantined-information/detail-quarantined-information/${data[i].id}">Lần ${i+1}</a>
                        </td>
                        <td>
                            ${data[i].levelInfection.level}
                        </td>
                        <td>${data[i].arrivalDate}</td>
                        <td>${(data[i].departureDate === null || data[i].departureDate === "") ? "" : data[i].departureDate}</td>
                        <td>${(data[i].status === 0) ? "Chưa hoàn thành" : (data[i].status === 1) ? "Đã hoàn thành" : "Chưa hoàn thành/ Dương tính"}</td>
                        <td>
                            <a data-value-id="${data[i].id}" data-value-status="${data[i].status}" class="btn btn-info text-white btnEdit">Sửa</a>
                            <button data-value="${data[i].id}" class="btn btn-danger text-white btnDelete">Xóa</button>
                        </td>
                    </tr>
                        `;
    };

    baseTable += str;
    elementTable.innerHTML = baseTable;

    let btnDelete = elementByClassName("btnDelete");
    let btnEdit = elementByClassName("btnEdit");

    for (let j = 0; j < btnDelete.length; j++) {
        btnDelete[j].setAttribute("hidden", true);
        btnEdit[j].setAttribute("hidden", true);

        btnDelete[j].addEventListener("click", function() {
            let valueId = this.getAttribute("data-value");
            Swal.fire({
                title: 'Xác nhận',
                text: "Bạn chắc chắn xóa ?",
                icon: 'info',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Hủy',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Xóa'
            }).then((result) => {
                if (result.isConfirmed) {
                    fetch(URL_COVID + "/quarantined-information/getTotalQuanrantinedInformationByQuarantinedPeopleId?quarantinedPeopleId=" + quarantinedPeopleId)
                        .then(res => res.json())
                        .then(data => {
                            if (parseInt(data) === 1) {
                                Swal.fire({
                                    title: 'Cảnh báo',
                                    text: "Bạn sẽ xóa toàn bộ thông tin người cách ly này?",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    cancelButtonColor: '#d33',
                                    cancelButtonText: 'Hủy',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Xóa'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        deleteQuanrantinedInformationById(valueId, quarantinedPeopleId, null, deleteQuanrantinedPeopleById);
                                    }
                                });
                            } else {
                                deleteQuanrantinedInformationById(valueId, quarantinedPeopleId, getQuarantinedInformation, null);
                            }
                        });
                }
            })
        });

        let valueStatus = btnEdit[j].getAttribute("data-value-status");
        let valueId = btnEdit[j].getAttribute("data-value-id");
        if (parseInt(valueStatus) !== 0) {
            btnEdit[j].setAttribute("hidden", true);
        }
        btnEdit[j].addEventListener("click", function() {
            if (parseInt(valueStatus) === 0) {
                window.location.href = URL_COVID + "/quarantined-information/edit-quarantined-information/" + valueId;
            } else {
                showError(null, "Không thể sửa đổi thông tin cách ly này");
            }
        });

        if (ifHasAuthority(ROLE_ADDER)) {
            btnDelete[j].removeAttribute("hidden");

            if (parseInt(valueStatus) !== 0) {
                btnEdit[j].setAttribute("hidden", true);
            } else {
                btnEdit[j].removeAttribute("hidden");
            }
        }

    }

    function deleteQuanrantinedInformationById(id, quarantinedPeopleId, callbackUI, callbackDeleteQuarantinedPeople) {
        fetch(URL_COVID + "/quarantined-information/deleteQuanrantinedInformationById?id=" + id + "&quarantinedPeopleId=" + quarantinedPeopleId)
            .then(res => res.json())
            .then(data => {
                if (data) {
                    if (callbackUI != null) {
                        callbackUI(URL_COVID, quarantinedPeopleId);
                    }
                    if (callbackDeleteQuarantinedPeople != null) {
                        callbackDeleteQuarantinedPeople(quarantinedPeopleId);
                    }
                } else {
                    showError(null, "Không thể xóa thông tin cách ly có dữ liệu bên đơn hoặc xét nghiệm", null);
                }
            });
    }

    function deleteQuanrantinedPeopleById(id) {
        fetch(URL_COVID + "/quarantined-people/deleteQuarantinedPeople?id=" + id)
            .then(res => res.json())
            .then(data => {
                if (data) {
                    getQuarantinedInformation(URL_COVID, quarantinedPeopleId);
                } else {
                    showError(null, "Không thể xóa thông tin cách ly", null);
                }
            });
    }
}