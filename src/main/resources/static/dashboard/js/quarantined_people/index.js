import { elementByClassName, elementById, showError, valueOf } from "../../../common_dashboard_login/js/function.js";
import { ifHasAuthority, isChoseDateGreaterThanToday, pagination, renderRooms, search } from "../common_dashboard/function.js";
import { URL_COVID, configPagination, configSearch, ROLE_TESTER, ROLE_HANDLER, ROLE_ADDER } from "../common_dashboard/variable.js"


var elementOfTable;
var elementOfShowError;
var searchInput;

var area;
var room;
var btnFilter;
var loader;
var pageWrapper;
var choseAreaId;
var choseRoomId;
var choseStatus;
var stompClient = null;

window.onload = (function() {

    start();

    setTimeout(() => {
        renderUI(URL_COVID);
    }, 220);

    connect();

    if (area.options.length > 0) {
        renderRooms(area.options[area.selectedIndex].value, room);

        area.addEventListener("change", () => {
            renderRooms(area.options[area.selectedIndex].value, room);
        });
    }

    btnFilter.addEventListener("click", function() {
        loader.removeAttribute("hidden");
        pageWrapper.classList.add("blur");

        setTimeout(function() {
            pageWrapper.classList.remove("blur");
            loader.setAttribute("hidden", true);
            renderUI(URL_COVID);
        }, 400);

        if (area.options.length > 0) {
            sessionStorage.setItem("chose_area_id", area.options[area.selectedIndex].value);
        }

        if (room.options.length > 0) {
            sessionStorage.setItem("chose_room_id", room.options[room.selectedIndex].value);
        }

        sessionStorage.setItem("chose_status", statusSelect.options[statusSelect.selectedIndex].value);
    });

    if (choseAreaId != null && choseRoomId != null && choseStatus) {
        area.value = choseAreaId;
        statusSelect.value = choseStatus;
        setTimeout(function() {
            renderRooms(choseAreaId, room);
            setTimeout(function() {
                room.value = choseRoomId;
            }, 60);
        }, 120);
    }
})();

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function() {
        stompClient.subscribe('/topic/getQuarantinedPeople', function() {
            renderUI(URL_COVID);
        });
    });
}

function start() {
    searchInput = elementById("searchInput");
    elementOfTable = elementById("quarantinedPeopleTable");
    elementOfShowError = elementById("error");

    configPagination.currentPage = 1;
    configPagination.recordPerPage = 4;
    configPagination.elementPagination = elementById("pagination");

    area = elementById("area");
    room = elementById("room");
    btnFilter = elementById("btnFilter");
    loader = elementById("loader");
    pageWrapper = elementById("pageWrapper");

    searchInput.addEventListener("keyup", function() {
        let roomValue = 0;
        if (room.options.length > 0) {
            roomValue = room.options[room.selectedIndex].value;
        }
        let statusValue = statusSelect.options[statusSelect.selectedIndex].value;

        // gán biến config tìm kiếm
        configSearch.baseUrl = URL_COVID; // là localhost:8080
        configSearch.urlSearch = "/quarantined-people/searchQuarantinedPeople?roomIdRQI=" + roomValue + "&statusQI=" + statusValue + "&keyword="; // gán đường dẫn đến controller
        configSearch.keyword = valueOf(searchInput); // keyword khi người dùng nhập vào

        configSearch.paramCallback.elementTable = elementOfTable; //phần tử của table để hiển thị kết quả
        configSearch.callbackTable = renderTable; //hàm được sử dụng để render dữ liệu ra table tìm kiếm
        configSearch.callbackUI = renderUI; //hàm được sử dụng để render dữ liệu ra table khi người dùng k nhập gì ở thanh tìm kiếm

        configSearch.elementShowError = elementOfShowError; // phần tử dùng để hiển thị lỗi
        configSearch.elementPagination = configPagination.elementPagination; // phần tử hiển thị phân trang

        configSearch.errorMessage = "Không có dữ liệu khớp với từ khóa :(((("; // lỗi khi hiển thị với ng dùng

        search(configSearch);
    });

    choseAreaId = sessionStorage.getItem("chose_area_id");
    choseRoomId = sessionStorage.getItem("chose_room_id");
    choseStatus = sessionStorage.getItem("chose_status");
}

function renderUI(url) {
    let roomValue = 0;
    if (room.options.length > 0) {
        roomValue = room.options[room.selectedIndex].value;
    }
    let statusValue = statusSelect.options[statusSelect.selectedIndex].value;

    fetch(url + "/quarantined-people/getQuarantinedPeopleByRoomId?roomIdRQI=" + (roomValue != null ? roomValue : 0) + "&statusQI=" + statusValue)
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                configPagination.lengthArray = data.length;

                function changePage(page) {
                    renderTable(elementOfTable, ((page - 1) * configPagination.recordPerPage), (page * configPagination.recordPerPage), data)
                }

                if (configPagination.currentPage > Math.ceil(configPagination.lengthArray / configPagination.recordPerPage)) {
                    configPagination.currentPage -= 1;
                }
                changePage(configPagination.currentPage);

                configPagination.callback = changePage;
                //hàm phân trang
                // configPagination: biến cấu hình phân trang
                pagination(configPagination);

                elementOfShowError.innerHTML = "";
            } else {
                elementOfTable.innerHTML = "";
                configPagination.elementPagination.innerHTML = "";
                elementOfShowError.innerHTML = "Hiện chưa có người cách ly trong phòng này :(((("
            }
        })
}

function renderTable(elementTable, startIndex, lengthArray, data) {
    let baseTable = `<thead>
                <tr>
                    <th class="border-top-0">#</th>
                    <th class="border-top-0">Họ và tên</th>
                    <th class="border-top-0">Giới tính</th>
                    <th class="border-top-0">Ngày tháng năm sinh</th>
                    <th class="border-top-0">Số điện thoại</th>
                    <th class="border-top-0">CMT/CCCD</th>
                </tr>
                </thead>
                <tbody>
                </tbody>`;

    let str = " ";
    for (let i = startIndex; i < lengthArray && i < data.length; i++) {
        str += `
                    <tr>
                        <td>${i+1}</td>
                        <td>
                        <a href="/quarantined-people/detail-quarantined-people/${data[i].id}">
                            ${data[i].fullname}
                        </a>
                        </td>
                        <td>${data[i].gender ? "Nam" : "Nữ"}</td>
                        <td>${data[i].dateOfBirth}</td>
                        <td>${data[i].phoneNumber}</td>
                        <td>${data[i].identityCard}</td>
                        <td>
                            <a data-value="${data[i].id}" class="btn btn-success text-white btnComplete">Hoàn thành</a>
                            <a data-value="${data[i].id}" class="btn btn-warning text-white btnAddTest">Xét nghiệm</a>
                            <a data-value="${data[i].id}" class="btn btn-info text-white btnAddForm">Đơn</a>
                            <a href="/quarantined-people/edit-quarantined-people/${data[i].id}" class="btn btn-info text-white btnEdit">Sửa</a>
                            </td>
                            </tr>
                            `;
    };

    baseTable += str;
    elementTable.innerHTML = baseTable;

    let btnAddForm = elementByClassName("btnAddForm");
    let btnAddTest = elementByClassName("btnAddTest");
    let btnEdit = elementByClassName("btnEdit");
    let btnComplete = elementByClassName("btnComplete");

    for (let j = 0; j < btnAddForm.length; j++) {
        btnAddForm[j].setAttribute("hidden", true);
        btnAddTest[j].setAttribute("hidden", true);
        btnEdit[j].setAttribute("hidden", true);
        btnComplete[j].setAttribute("hidden", true);

        btnAddTest[j].addEventListener("click", function() {
            let valueId = this.getAttribute("data-value");
            fetch(URL_COVID + "/quarantined-information/checkQuarantinedInformationNotComplete?quarantinedPeopleId=" + valueId)
                .then(res => res.json())
                .then(data => {
                    if (data) {
                        showError(null, "Không thể thêm mới xét nghiệm do người này không ở trong khu cách ly", null);
                    } else {
                        getTotalNegativeResultByQuarantinedInformationId(valueId);
                    }
                });
        });

        btnAddForm[j].addEventListener("click", function() {
            let valueId = this.getAttribute("data-value");
            fetch(URL_COVID + "/quarantined-information/checkQuarantinedInformationNotComplete?quarantinedPeopleId=" + valueId)
                .then(res => res.json())
                .then(data => {
                    if (data) {
                        showError(null, "Không thể thêm mới đơn do người này không ở trong khu cách ly", null);
                    } else {
                        window.location.href = URL_COVID + "/application-form/create-application-form/" + valueId;
                    }
                });
        });

        btnComplete[j].addEventListener("click", function() {
            if (ifHasAuthority(ROLE_ADDER)) {
                let valueId = this.getAttribute("data-value");
                fetch(URL_COVID + "/quarantined-information/getQuarantinedInformationNotCompleteByQuarantinedPeopleId?quarantinedPeopleId=" + valueId)
                    .then(res => res.json())
                    .then(data => {
                        if (data.id === null) {
                            showError(null, "Không thể hoàn thành cách ly do người này không ở trong khu cách ly", null);
                        } else {
                            if (isChoseDateGreaterThanToday(data.departureDate)) {
                                showError(null, "Không thể hoàn thành cách ly do người này chưa cách ly đủ số ngày", null);
                            } else {
                                checkTestResult(valueId, data.id);
                            }
                        }
                    });
            } else {
                showError(null, "Bạn không được quyền hoàn thành cách ly", null);
            }
        });

        if (ifHasAuthority(ROLE_TESTER)) {
            btnAddTest[j].removeAttribute("hidden")
        }

        if (ifHasAuthority(ROLE_HANDLER)) {
            btnAddForm[j].removeAttribute("hidden")
        }

        if (ifHasAuthority(ROLE_ADDER)) {
            btnEdit[j].removeAttribute("hidden")
        }

        if (ifHasAuthority(ROLE_ADDER)) {
            let valueId = btnComplete[j].getAttribute("data-value");
            fetch(URL_COVID + "/quarantined-information/getQuarantinedInformationNotCompleteByQuarantinedPeopleId?quarantinedPeopleId=" + valueId)
                .then(res => res.json())
                .then(dataQI => {
                    if (dataQI.id !== null) {
                        fetch(URL_COVID + "/test/getTotalNegativeResultByQuarantinedInformationId?quarantinedPeopleId=" + valueId)
                            .then(res => res.json())
                            .then(dataTest => {
                                if (dataQI != null && !isChoseDateGreaterThanToday(dataQI.departureDate) && parseInt(dataTest) === 2) {
                                    btnComplete[j].removeAttribute("hidden")
                                }
                            });
                    }
                });
        }
    }

    function getTotalNegativeResultByQuarantinedInformationId(quarantinedPeopleId) {
        fetch(URL_COVID + "/test/getTotalNegativeResultByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
            .then(res => res.json())
            .then(data => {
                if (parseInt(data) === 2) {
                    showError(null, "Đã có kết quả âm tính không thể thêm xét nghiệm");
                } else {
                    getTotalPositiveResultByQuarantinedInformationId(quarantinedPeopleId);
                }
            });
    }

    function getTotalPositiveResultByQuarantinedInformationId(quarantinedPeopleId) {
        fetch(URL_COVID + "/test/getTotalPositiveResultByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
            .then(res => res.json())
            .then(data => {
                if (parseInt(data) === 2) {
                    showError(null, "Đã có kết quả dương tính không thể thêm xét nghiệm");
                } else {
                    getTotalTestHaveQualifiedSampleStatus(quarantinedPeopleId);
                }
            });
    }

    function getTotalTestHaveQualifiedSampleStatus(quarantinedPeopleId) {
        fetch(URL_COVID + "/test/getTotalTestHaveQualifiedSampleStatusByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
            .then(res => res.json())
            .then(data => {
                if (parseInt(data) === 3) {
                    showError(null, null, "Không thể thêm được xét nghiệm <br> <b>Tối đa 3 xét nghiệm với tình trạng mẫu: Đạt</b>")
                } else {
                    window.location.href = URL_COVID + "/test/create-test/" + quarantinedPeopleId;
                }
            });
    }

    function checkTestResult(quarantinedPeopleId, quarantinedInformationId) {
        fetch(URL_COVID + "/test/getTotalNegativeResultByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
            .then(res => res.json())
            .then(data => {
                if (parseInt(data) === 2) {
                    completeQuarantined(quarantinedInformationId);
                } else {
                    showError(null, "Chưa có kết quả âm tính không thể hoàn thành cách ly");
                }
            });
    }

    function completeQuarantined(quarantinedInformationId) {
        Swal.fire({
            title: 'Xác nhận',
            text: "Bạn chắc chắn hoàn thành cách ly cho người này chứ ?",
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Hủy',
            confirmButtonText: 'Xác nhận'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(URL_COVID + "/quarantined-information/completeQuarantined?quarantinedInformationId=" + quarantinedInformationId)
                    .then(res => res.json())
                    .then(data => {
                        Swal.fire({
                            title: 'Xong!',
                            text: 'Hoàn thành cách ly thành công',
                            html: null,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        });
                        renderUI(URL_COVID);
                    });
            }
        })
    }

    // <button data-value="${data[i].id}" class="btn btn-danger text-white btnDelete">Xóa</button>
    // let btnDelete = elementByClassName("btnDelete");
    // for (let j = 0; j < btnDelete.length; j++) {
    //     btnDelete[j].addEventListener("click", function() {
    //         let valueId = this.getAttribute("data-value");
    //         Swal.fire({
    //             title: 'Xác nhận',
    //             text: "Bạn chắc chắn xóa ?",
    //             icon: 'warning',
    //             showCancelButton: true,
    //             cancelButtonColor: '#d33',
    //             cancelButtonText: 'Hủy',
    //             confirmButtonColor: '#3085d6',
    //             confirmButtonText: 'Xóa'
    //         }).then((result) => {
    //             if (result.isConfirmed) {
    //                 fetch(URL_COVID + "/quarantined-people/deleteQuarantinedPeople?id=" + valueId)
    //                     .then(res => res.json())
    //                     .then(data => {
    //                         if (data) {
    //                             renderUI(URL_COVID);
    //                         } else {
    //                             showError(null, "Không thể xóa người cách ly có dữ liệu bên thông tin cách ly", null);
    //                         }
    //                     });
    //             }
    //         })
    //     });
    // }
}