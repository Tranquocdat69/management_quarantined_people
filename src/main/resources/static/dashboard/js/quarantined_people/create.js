import { changeSubmitForm, checkRegex, elementById, showError, valueOf } from "../../../common_dashboard_login/js/function.js";
import { REGEX_EMAIL } from "../../../common_dashboard_login/js/variable.js";
import { isDate, previewImageBeforeUpload, openDatePicker, renderProvinces, renderDistricts, renderWards, renderRooms, isChoseDateGreaterThanToday } from "../common_dashboard/function.js";
import { URL_COVID, URL_GET_PROVINCE, URL_GET_DISTRICT, URL_GET_WARD, URL_IMAGE_DEFAULT } from "../common_dashboard/variable.js";

var fullname;
var email;
var phoneNumber;
var nationality;
var identityCard;
var dateOfBirth;
var addressDetail;
var workAddress;
var chooseImage;
var previewImage;

var provinceList;
var districtList;
var wardList;
var formCreateQuarantinedPeople;
var submitformCreateQuarantinedPeople;

var area;
var room;
var sourceOfContact;
var arrivalDateTemp;

window.onload = (function() {
    start();

    changeSubmitForm(formCreateQuarantinedPeople, submitformCreateQuarantinedPeople, validateForm);

    previewImageBeforeUpload(chooseImage, previewImage, URL_IMAGE_DEFAULT)

    renderProvinces(URL_GET_PROVINCE, provinceList);
    provinceList.addEventListener("change", () => renderDistricts(URL_GET_DISTRICT, provinceList, districtList, wardList));
    districtList.addEventListener("change", () => renderWards(URL_GET_WARD, districtList, wardList));

    openDatePicker("#dateOfBirth");
    openDatePicker("#arrivalDateTemp");

    renderRooms(area.options[area.selectedIndex].value, room);

    area.addEventListener("change", () => {
        renderRooms(area.options[area.selectedIndex].value, room);
    });

})();

function start() {

    fullname = elementById("fullname");
    email = elementById("email");
    phoneNumber = elementById("phoneNumber");

    nationality = elementById("nationality");
    identityCard = elementById("identityCard");
    dateOfBirth = elementById("dateOfBirth");

    provinceList = elementById("provinceList");
    districtList = elementById("districtList");
    wardList = elementById("wardList");
    addressDetail = elementById("addressDetail");

    workAddress = elementById("workAddress");
    chooseImage = elementById("chooseImage");
    previewImage = elementById("previewImage");

    formCreateQuarantinedPeople = elementById("formCreateQuarantinedPeople");
    submitformCreateQuarantinedPeople = elementById("submitformCreateQuarantinedPeople");

    area = elementById("area");
    room = elementById("room");

    sourceOfContact = elementById("sourceOfContact");
    arrivalDateTemp = elementById("arrivalDateTemp");


    fullname.focus();
}

function validateForm(formId) {
    let valueFullname = valueOf(fullname);
    let valueEmail = valueOf(email);
    let valuePhone = valueOf(phoneNumber);

    let valueNationality = valueOf(nationality);
    let valueIdentityCard = valueOf(identityCard);
    let valueDateOfBirth = valueOf(dateOfBirth);

    let valueAddressDetail = valueOf(addressDetail);

    let valueWorkAddress = valueOf(workAddress);

    let isEmailOk = false;
    let isDateOfBirthOk = false;
    let isUploadFileOk = false;

    let valueSourceOfContact = valueOf(sourceOfContact);
    let valueArrivalDateTemp = valueOf(arrivalDateTemp);
    let isArrivalDateOk = false;


    if (valueFullname === "") {
        showError(fullname, 'Nhập họ và tên');
    } else if (valueFullname.length > 100) {
        showError(fullname, 'Họ và tên tối đa 100 kí tự');
    } else if (valueEmail != "") {
        if (valueEmail.length > 45) {
            showError(email, "Độ dài email tối đa 45 kí tự");
            isEmailOk = false;
        } else if (!checkRegex(valueEmail, REGEX_EMAIL)) {
            isEmailOk = false;
            showError(email, null, 'Vui lòng nhập đúng định dạng email' + '<br/>' + '<b>VD: demo@gmail.com.vn</b>');
        } else {
            isEmailOk = true;
        }
    } else {
        isEmailOk = true;
    }

    if (isEmailOk) {
        if (valuePhone === "") {
            showError(phoneNumber, "Nhập số điện thoại");
        } else if (valuePhone.length >= 11 || valuePhone.length <= 9) {
            showError(phoneNumber, null, "Số điện thoại phải là 10 chữ số" + "<br/>" + "Độ dài hiện tại: " + valuePhone.length);
        } else if (valueNationality === "") {
            showError(nationality, "Nhập quốc tịch");
        } else if (valueNationality.length > 255) {
            showError(nationality, "Độ dài quốc tịch tối đa 255 kí tự");
        } else if (valueIdentityCard === "") {
            showError(identityCard, "Nhập CMT/CCCD");
        } else if (valueIdentityCard.length != 9 && valueIdentityCard.length != 12) {
            showError(identityCard, null, "CMT/CCCD độ dài 9 số hoặc 12 số <br> Độ dài hiện tại: " + valueIdentityCard.length);
        } else if (valueDateOfBirth === "") {
            showError(dateOfBirth, "Nhập ngày tháng năm sinh");
        } else if (valueDateOfBirth != "") {
            if (!isDate(valueDateOfBirth)) {
                showError(dateOfBirth, null, "Ngày tháng năm sinh không hợp lệ hoặc sai định dạng <br> VD: 01/01/2021")
                isDateOfBirthOk = false;
            } else {
                if (isChoseDateGreaterThanToday(valueDateOfBirth)) {
                    showError(dateOfBirth, "Không thể chọn ngày sinh lớn hơn ngày hiện tại")
                    isDateOfBirthOk = false;
                } else {
                    isDateOfBirthOk = true;
                }
            }
        }
    }

    if (isDateOfBirthOk) {
        if (provinceList.options[provinceList.selectedIndex].getAttribute("data-code-province") === null) {
            showError(provinceList, "Chọn Tỉnh/Thành phố")
        } else if (districtList.options[districtList.selectedIndex].getAttribute("data-code-district") === null) {
            showError(districtList, "Chọn Quận/Huyện")
        } else if (wardList.options[wardList.selectedIndex].getAttribute("data-ward") === null) {
            showError(wardList, "Chọn Phường/Xã")
        } else if (valueAddressDetail === "") {
            showError(addressDetail, "Nhập địa chỉ chi tiết")
        } else if (valueAddressDetail.length > 255) {
            showError(addressDetail, "Độ dài địa chỉ chi tiết tối đa 255 kí tự")
        } else if (valueWorkAddress === "") {
            showError(workAddress, "Nhập địa chỉ làm việc");
        } else if (valueWorkAddress.length > 255) {
            showError(workAddress, "Độ dài địa chỉ làm việc tối đa 255 kí tự");
        } else if (chooseImage.files.length > 0) {
            const fsize = chooseImage.files.item(0).size;
            const file = Math.round((fsize / 1024));
            const value = chooseImage.value;
            const extension = value.substring(value.lastIndexOf('.') + 1).toLowerCase();
            if (extension != "png" && extension != "jpeg" && extension != "jpg") {
                showError(chooseImage, "Chỉ chọn file có định dạng jpeg,jpg,png");
                isUploadFileOk = false;
            } else {
                if (file > 5120) {
                    showError(chooseImage, null, "Chọn file ảnh không quá kích thước 5 MB <br> Kích thước file hiện tại: " + Math.round((file / 1024)) + " MB");
                    isUploadFileOk = false;
                } else {
                    isUploadFileOk = true;
                }
            }
        } else {
            isUploadFileOk = true;
        }
    }

    if (isUploadFileOk) {
        (function checkIdentityCardExist(param, callback) {
            fetch(URL_COVID + "/quarantined-people/checkIdentityCardExist?identityCard=" + param)
                .then(res => res.json())
                .then(data => {
                    if (data) {
                        showError(identityCard, "CMT/CCCD đã tồn tại trong hệ thống");
                    } else {
                        callback();
                    }
                })
        })(valueIdentityCard, checkQuarantinedInformation);

        function checkQuarantinedInformation() {
            if (valueSourceOfContact === "") {
                showError(sourceOfContact, "Nhập nguồn tiếp xúc mầm bệnh");
            } else if (valueArrivalDateTemp === "") {
                showError(arrivalDateTemp, "Nhập ngày vào khu cách ly");
            } else if (valueArrivalDateTemp != "") {
                if (!isDate(valueArrivalDateTemp)) {
                    showError(arrivalDateTemp, null, "Ngày tháng năm không hợp lệ hoặc sai định dạng <br> VD: 01/01/2021")
                    isArrivalDateOk = false;
                } else {
                    if (isChoseDateGreaterThanToday(valueArrivalDateTemp)) {
                        showError(arrivalDateTemp, "Không thể chọn ngày vào khu cách ly lớn hơn ngày hiện tại")
                        isArrivalDateOk = false;
                    } else {
                        isArrivalDateOk = true;
                    }
                }
            }

            if (isArrivalDateOk) {
                fetch(URL_COVID + "/rooms/checkLimitBedNumberInRoom?roomId=" + room.options[room.selectedIndex].value)
                    .then(res => res.json())
                    .then(data => {
                        if (data.bedNumber === data.totalPeopleInRoom) {
                            showError(null, null, "Phòng không còn giường trống <br> <b>Số người/Số giường: " + data.bedNumber + "/" + data.totalPeopleInRoom + "</b>");
                        } else {
                            Swal.fire({
                                title: 'Xác nhận',
                                html: "Thêm người cách ly " + valueFullname + " vào <br><b>" + room.options[room.selectedIndex].text + "</b><br><b>" + area.options[area.selectedIndex].text + "</b>",
                                icon: 'warning',
                                showCancelButton: true,
                                cancelButtonColor: '#d33',
                                cancelButtonText: 'Hủy',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Xác nhận'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    formId.submit();
                                }
                            })
                        }
                    });
            }
        }
    }
}