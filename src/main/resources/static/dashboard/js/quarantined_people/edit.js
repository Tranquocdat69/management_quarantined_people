import { changeSubmitForm, checkRegex, elementById, showError, valueOf } from "../../../common_dashboard_login/js/function.js";
import { REGEX_EMAIL } from "../../../common_dashboard_login/js/variable.js";
import { isDate, previewImageBeforeUpload, openDatePicker, renderProvinces, renderDistricts, renderWards, setAvatar } from "../common_dashboard/function.js";
import { URL_COVID, URL_GET_PROVINCE, URL_GET_DISTRICT, URL_GET_WARD, TRANSLATE_DATE, URL_IMAGE, URL_IMAGE_DEFAULT } from "../common_dashboard/variable.js";

var fullname;
var email;
var phoneNumber;
var nationality;
var identityCard;
var dateOfBirth;
var addressDetail;
var workAddress;
var chooseImage;
var previewImage;

var provinceList;
var districtList;
var wardList;
var formEditQuarantinedPeople;
var submitFormEditQuarantinedPeople;
var nameOfAvatar;
var valueNameAvatar;

window.onload = (function() {
    start();

    changeSubmitForm(formEditQuarantinedPeople, submitFormEditQuarantinedPeople, validateForm);

    if (valueNameAvatar === "") {
        previewImageBeforeUpload(chooseImage, previewImage, URL_IMAGE_DEFAULT)
    } else {
        previewImageBeforeUpload(chooseImage, previewImage, URL_IMAGE + valueNameAvatar)
    }

    renderProvinces(URL_GET_PROVINCE, provinceList);

    setTimeout(() => {
        provinceList.value = currentProvince;
        if (provinceList.value === currentProvince) {
            setTimeout(function() {
                renderCurrentDistricts(URL_GET_DISTRICT, provinceList, districtList, wardList);
                setTimeout(function() {
                    renderCurrentWards(URL_GET_WARD, districtList, wardList);
                }, 480);
            }, 280);
        }
    }, 110);





    provinceList.addEventListener("change", () => renderDistricts(URL_GET_DISTRICT, provinceList, districtList, wardList));
    districtList.addEventListener("change", () => renderWards(URL_GET_WARD, districtList, wardList));

    setAvatar(previewImage, valueNameAvatar, URL_IMAGE, URL_IMAGE_DEFAULT);

    openDatePicker("#dateOfBirth");
})();

function start() {
    fullname = elementById("fullname");
    email = elementById("email");
    phoneNumber = elementById("phoneNumber");

    nationality = elementById("nationality");
    identityCard = elementById("identityCard");
    dateOfBirth = elementById("dateOfBirth");

    provinceList = elementById("provinceList");
    districtList = elementById("districtList");
    wardList = elementById("wardList");
    addressDetail = elementById("addressDetail");

    workAddress = elementById("workAddress");
    chooseImage = elementById("chooseImage");
    previewImage = elementById("previewImage");

    dateOfBirth.value = currentDateOfBirth;

    nameOfAvatar = elementById("nameOfAvatar");
    valueNameAvatar = valueOf(nameOfAvatar);

    formEditQuarantinedPeople = elementById("formEditQuarantinedPeople");
    submitFormEditQuarantinedPeople = elementById("submitFormEditQuarantinedPeople");

    fullname.focus();
}

function validateForm(formId) {
    let valueFullname = valueOf(fullname);
    let valueEmail = valueOf(email);
    let valuePhone = valueOf(phoneNumber);

    let valueNationality = valueOf(nationality);
    let valueIdentityCard = valueOf(identityCard);
    let valueDateOfBirth = valueOf(dateOfBirth);

    let valueAddressDetail = valueOf(addressDetail);

    let valueWorkAddress = valueOf(workAddress);

    let isEmailOk = false;
    let isDateOfBirthOk = false;
    let isUploadFileOk = false;

    if (valueFullname === "") {
        showError(fullname, 'Nhập họ và tên');
    } else if (valueFullname.length > 100) {
        showError(fullname, 'Họ và tên tối đa 100 kí tự');
    } else if (valueEmail != "") {
        if (valueEmail.length > 45) {
            showError(email, "Độ dài email tối đa 45 kí tự");
            isEmailOk = false;
        } else if (!checkRegex(valueEmail, REGEX_EMAIL)) {
            isEmailOk = false;
            showError(email, null, 'Vui lòng nhập đúng định dạng email' + '<br/>' + '<b>VD: demo@gmail.com.vn</b>');
        } else {
            isEmailOk = true;
        }
    } else {
        isEmailOk = true;
    }

    if (isEmailOk) {
        if (valuePhone === "") {
            showError(phoneNumber, "Nhập số điện thoại");
        } else if (valuePhone.length >= 11 || valuePhone.length <= 9) {
            showError(phoneNumber, null, "Số điện thoại phải là 10 chữ số" + "<br/>" + "Độ dài hiện tại: " + valuePhone.length);
        } else if (valueNationality === "") {
            showError(nationality, "Nhập quốc tịch");
        } else if (valueNationality.length > 255) {
            showError(nationality, "Độ dài quốc tịch tối đa 255 kí tự");
        } else if (valueIdentityCard === "") {
            showError(identityCard, "Nhập CMT/CCCD");
        } else if (valueIdentityCard.length != 9 && valueIdentityCard.length != 12) {
            showError(identityCard, null, "CMT/CCCD độ dài 9 số hoặc 12 số <br> Độ dài hiện tại: " + valueIdentityCard.length);
        } else if (valueDateOfBirth === "") {
            showError(dateOfBirth, "Nhập ngày tháng năm sinh");
        } else if (valueDateOfBirth != "") {
            if (!isDate(valueDateOfBirth)) {
                showError(dateOfBirth, null, "Ngày tháng năm sinh không hợp lệ hoặc sai định dạng <br> VD: 01/01/2021")
                isDateOfBirthOk = false;
            } else {
                isDateOfBirthOk = true;
            }
        }
    }

    if (isDateOfBirthOk) {
        if (provinceList.options[provinceList.selectedIndex].getAttribute("data-code-province") === null) {
            showError(provinceList, "Chọn Tỉnh/Thành phố")
        } else if (districtList.options[districtList.selectedIndex].getAttribute("data-code-district") === null) {
            showError(districtList, "Chọn Quận/Huyện")
        } else if (wardList.options[wardList.selectedIndex].getAttribute("data-ward") === null) {
            showError(wardList, "Chọn Phường/Xã")
        } else if (valueAddressDetail === "") {
            showError(addressDetail, "Nhập địa chỉ chi tiết")
        } else if (valueAddressDetail.length > 255) {
            showError(addressDetail, "Độ dài địa chỉ chi tiết tối đa 255 kí tự")
        } else if (valueWorkAddress === "") {
            showError(workAddress, "Nhập địa chỉ làm việc");
        } else if (valueWorkAddress.length > 255) {
            showError(workAddress, "Độ dài địa chỉ làm việc tối đa 255 kí tự");
        } else if (chooseImage.files.length > 0) {
            const fsize = chooseImage.files.item(0).size;
            const file = Math.round((fsize / 1024));
            const value = chooseImage.value;
            const extension = value.substring(value.lastIndexOf('.') + 1).toLowerCase();
            if (extension != "png" && extension != "jpeg" && extension != "jpg") {
                showError(chooseImage, "Chỉ chọn file có định dạng jpeg,jpg,png");
                isUploadFileOk = false;
            } else {
                if (file > 5120) {
                    showError(chooseImage, null, "Chọn file ảnh không quá kích thước 5 MB <br> Kích thước file hiện tại: " + Math.round((file / 1024)) + " MB");
                    isUploadFileOk = false;
                } else {
                    isUploadFileOk = true;
                }
            }
        } else {
            isUploadFileOk = true;
        }
    }

    if (isUploadFileOk) {
        (function checkIdentityCardExist(currentParam, newParam) {
            fetch(URL_COVID + "/quarantined-people/checkIdentityCardExistWhenEdit?currentIdentityCard=" + currentParam + "&newIdentityCard=" + newParam)
                .then(res => res.json())
                .then(data => {
                    if (data) {
                        showError(identityCard, "CMT/CCCD đã tồn tại trong hệ thống");
                    } else {
                        Swal.fire({
                            title: 'Xác nhận',
                            text: "Bạn chắc chắn sửa thông tin người cách ly này chứ ?",
                            icon: 'info',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            cancelButtonText: 'Hủy',
                            confirmButtonText: 'Sửa'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                formId.submit();
                            }
                        })
                    }
                })
        })(currentIdentityCard, valueIdentityCard);
    }
}

function renderCurrentDistricts(url, provinceSelect, districtSelect, wardSelect) {
    let selectedProvince = provinceSelect.options[provinceSelect.selectedIndex].getAttribute("data-code-province");
    fetch(url)
        .then(response => response.json())
        .then(
            data => {
                let str = `<option>Quận/Huyện</option>`;
                for (let i = 0; i < data.length; i++) {
                    if (data[i].province_code == selectedProvince) {
                        if (data[i].name === currentDistrict) {
                            str += `<option selected data-code-district="${data[i].code}" value="${data[i].name}">${data[i].name}</option>`;
                        } else {
                            str += `<option data-code-district="${data[i].code}" value="${data[i].name}">${data[i].name}</option>`;
                        }
                    }
                }
                districtSelect.innerHTML = str;

            }
        );
};

function renderCurrentWards(url, districtSelect, wardSelect) {
    let selectedDistrict = districtSelect.options[districtSelect.selectedIndex].getAttribute("data-code-district");
    fetch(url)
        .then(response => response.json())
        .then(
            data => {
                let str = `<option>Phường/Xã</option>`;
                for (let i = 0; i < data.length; i++) {
                    if (data[i].district_code == selectedDistrict) {
                        if (data[i].name === currentWard) {
                            str += `<option selected data-ward="${data[i].code}" value="${data[i].name}">${data[i].name}</option>`;
                        } else {
                            str += `<option data-ward="${data[i].code}" value="${data[i].name}">${data[i].name}</option>`;
                        }
                    }
                }
                wardSelect.innerHTML = str;
            }
        );
}