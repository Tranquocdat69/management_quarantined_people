import { URL_COVID } from "../common_dashboard/variable.js";
import { showError, valueOf, checkRegex, elementById, elementByName, changeSubmitForm } from "../../../common_dashboard_login/js/function.js";


var formEditArea;
var submitBtn;
var areaName;
var areaAddress;
var contact;
var currentContact;

var currentAreaName;

window.onload=(function(){
    start();

    changeSubmitForm(formEditArea,submitBtn,validateForm);
})();

function start(){
    formEditArea = elementById("formEditArea");
    submitBtn = elementById("submitBtn");
    areaName= elementById("areaName");
    areaAddress= elementById("areaAddress");
    contact = elementById("contact");

    currentContact = valueOf(contact);
    
    currentAreaName = valueOf(areaName);
}

function validateForm(formId){

    let valueAreaName = valueOf(areaName);
    let valueAreaAddress = valueOf(areaAddress);
    let valueContact = valueOf(contact);


    if(valueAreaName===""){
        showError(areaName,'Nhập khu cách ly');
    }else if(valueAreaName.length >100){
        showError(areaName,'Khu cách ly tối đa 100 kí tự');
    }else if(valueAreaAddress===""){
        showError(areaAddress,'Nhập địa chỉ');
    }else if(valueAreaAddress.length >500){
        showError(areaAddress,'Địa chỉ tối đa 500 kí tự');
    }else if (valueContact === "") {
        showError(contact, "Nhập số điện thoại");
    } else if (valueContact.length >= 11 || valueContact.length <= 9) {
        showError(contact, null, "Số điện thoại phải là 10 chữ số" + "<br/>" + "Độ dài hiện tại: " + valueContact.length);
    }else{
        (function CheckExsitDiffContact(callback){
            fetch(URL_COVID + "/areas/CheckExsitDiffContact?currentContact="+currentContact+"&&newContact="+valueContact)
        .then(res => res.json())
        .then(data => {
            if (data) {
                showError(contact,"Số điện thoại đã được sử dụng");
            }else{
                // formId.submit();
                callback();
            }
        })
        })(CheckExsitEditArea);

        function CheckExsitEditArea(){
            fetch(URL_COVID + "/areas/CheckExsitEditArea?currentAreaName="+currentAreaName +"&newAreaName="+valueAreaName)
            .then(res => res.json())
            .then(data => {
                if (data) {
                    showError(areaName,"Khu vực đã tồn tại trong hệ thống");
                }else{
                     formId.submit();
                }
            })
        }
    }
    
}