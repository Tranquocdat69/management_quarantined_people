import { elementById, elementByClassName, showError } from "../../../common_dashboard_login/js/function.js";
import { convertDefaultDateTypeMySQLToStringDate, convertStringToDate, ifHasAuthority, pagination } from "../common_dashboard/function.js";
import { URL_COVID, configPagination, ROLE_ADMIN } from "../common_dashboard/variable.js";

var roomHistory = elementById("roomHistory-tab");
var tableRoomHistory;
var errorRoomHistory;
var stompClient = null;

window.onload = (function() {
    startRoomHistory();

    renderUIRoomHistory(URL_COVID);

    connect();
});

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function() {
        stompClient.subscribe('/topic/getRoomQuarantinedInformation/' + quarantinedInformationId, function() {
            setTimeout(() => {
                renderUIRoomHistory(URL_COVID);
            }, 120);
        });
    });
}

function startRoomHistory() {
    tableRoomHistory = elementById("tableRoomHistory");
    errorRoomHistory = elementById("errorRoomHistory");

    // configPagination.currentPage = 1;
    // configPagination.recordPerPage = 1;
    // configPagination.elementPagination = elementById("paginationRoomHistory");
}

function renderUIRoomHistory(url) {
    fetch(url + "/quarantined-information/getAllRoomQuarantinedInformationByQuarantinedInforId?quarantinedInformationId=" + quarantinedInformationId)
        .then(response => response.json())
        .then(data => {
            console.log(data.length);
            if (data.length > 0) {
                // configPagination.lengthArray = data.length;

                renderTableRoomHistory(tableRoomHistory, 0, data.length, data);
                //hàm xử lí dữ liệu khi chuyển trang
                // function changePageTest(page) {
                //     renderTableRoomHistory(tableRoomHistory, ((page - 1) * configPagination.recordPerPage), (page * configPagination.recordPerPage), data)
                // }

                // ae thêm cái này để khi xóa không bị lỗi trang lung tung :V
                // if (configPagination.currentPage > Math.ceil(configPagination.lengthArray / configPagination.recordPerPage)) {
                //     configPagination.currentPage -= 1;
                // }

                // changePageTest(configPagination.currentPage);

                // configPagination.callback = changePageTest;
                //hàm phân trang
                // configPagination: biến cấu hình phân trang
                // pagination(configPagination);

                errorRoomHistory.innerHTML = "";
            } else {
                tableRoomHistory.innerHTML = "";
                // configPagination.elementPagination.innerHTML = "";
                errorRoomHistory.innerHTML = "Hiện chưa có xét nghiệm trong lần cách ly này :(((("
            }
        });
}

function renderTableRoomHistory(elementTable, startIndex, lengthArray, data) {
    let baseTable = `<thead>
                <tr>
                    <th class="border-top-0">Tên phòng</th>
                    <th class="border-top-0">Ngày rời phòng</th>
                    <th class="border-top-0">Số ngày đã ở tại phòng</th>
                    <th class="border-top-0">Tình trạng</th>
                    
                </tr>
                </thead>
                <tbody>
                </tbody>`;

    let str = " ";
    for (let i = startIndex; i < lengthArray && i < data.length; i++) {
        if (parseInt(data[i][4]) === 0) {
            str += `
                    <tr>
                        <td>${data[i][5]}</td>
                        <td>Đang ở</td>
                        <td>Đang ở</td>
                        <td>${data[i][4] ? "Đã ở" : "Đang ở"}</td>
                    </tr>
                        `;
        } else {
            str += `
            <tr>
                <td>${data[i][5]}</td>
                <td>${convertDefaultDateTypeMySQLToStringDate(data[i][3])}</td>
                <td>${
                    Math.floor((convertStringToDate(convertDefaultDateTypeMySQLToStringDate(data[i][3])) - convertStringToDate(convertDefaultDateTypeMySQLToStringDate(data[i][6])) ) / (3600000*24))
                     - 
                    (data[i+1] && data[i+1][3] ? Math.floor((convertStringToDate(convertDefaultDateTypeMySQLToStringDate(data[i+1][3])) - convertStringToDate(convertDefaultDateTypeMySQLToStringDate(data[i+1][6])) ) / (3600000*24)) : 0)
                }</td>
                <td>${data[i][4] ? "Đã ở" : "Đang ở"}</td>
                <td>
                    <button data-value="${data[i][0]}" class="btn btn-danger text-white btnDeleteRoomHistory">Xóa</button>
                </td>
            </tr>
                `;
        }
    };

    baseTable += str;
    elementTable.innerHTML = baseTable;

    let btnDeleteRoomHistory = elementByClassName("btnDeleteRoomHistory");

    for (let j = 0; j < btnDeleteRoomHistory.length; j++) {
        btnDeleteRoomHistory[j].addEventListener("click", function() {
            let valueId = this.getAttribute("data-value");
            Swal.fire({
                title: 'Xác nhận',
                text: "Bạn chắc chắn xóa xét nghiệm này?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Hủy',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Xóa'
            }).then((result) => {
                if (result.isConfirmed) {
                    fetch(URL_COVID + "/quarantined-information/deleteRoomQuarantinedInformationByRoomQuarantinedInformationId?roomQuarantinedInformationId=" + valueId)
                        .then(res => res.json())
                        .then(data => {
                            if (data) {
                                renderUIRoomHistory(URL_COVID);
                            } else {
                                showError(null, "Không thể xóa phòng đang ở", null);
                            }
                        });
                }
            })
        });

        if (!ifHasAuthority(ROLE_ADMIN)) {
            btnDeleteRoomHistory[j].style.display = 'none';
        }
    }
}