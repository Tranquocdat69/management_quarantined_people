import { changeSubmitForm, elementById, showError, valueOf } from "../../../common_dashboard_login/js/function.js";
import { convertStringDateToDefaultDateTypeMySQL, convertStringToDate, isChoseDateGreaterThanToday, isDate, openDatePicker, renderRooms } from "../common_dashboard/function.js";
import { URL_COVID } from "../common_dashboard/variable.js";

var formCreateQuarantinedInformation;
var submitformCreateQuarantinedInformation;
var area;
var room;
var sourceOfContact;
var arrivalDateTemp;

window.onload = (function() {
    start();

    renderRooms(area.options[area.selectedIndex].value, room);

    area.addEventListener("change", () => {
        renderRooms(area.options[area.selectedIndex].value, room);
    });

    changeSubmitForm(formCreateQuarantinedInformation, submitformCreateQuarantinedInformation, validateForm)

    openDatePicker("#arrivalDateTemp");
})();

function start() {
    area = elementById("area");
    room = elementById("room");

    sourceOfContact = elementById("sourceOfContact");
    arrivalDateTemp = elementById("arrivalDateTemp");

    formCreateQuarantinedInformation = elementById("formCreateQuarantinedInformation");
    submitformCreateQuarantinedInformation = elementById("submitformCreateQuarantinedInformation");

    sourceOfContact.focus();
}

function validateForm(formId) {
    let valueSourceOfContact = valueOf(sourceOfContact);
    let valueArrivalDateTemp = valueOf(arrivalDateTemp);
    let isArrivalDateOk = false;


    if (valueSourceOfContact === "") {
        showError(sourceOfContact, "Nhập nguồn tiếp xúc mầm bệnh");
    } else if (valueArrivalDateTemp === "") {
        showError(arrivalDateTemp, "Nhập ngày vào khu cách ly");
    } else if (valueArrivalDateTemp != "") {
        if (!isDate(valueArrivalDateTemp)) {
            showError(arrivalDateTemp, null, "Ngày tháng năm không hợp lệ hoặc sai định dạng <br> VD: 01/01/2021")
            isArrivalDateOk = false;
        } else {
            if (isChoseDateGreaterThanToday(valueArrivalDateTemp)) {
                showError(arrivalDateTemp, "Không thể chọn ngày vào khu cách ly lớn hơn ngày hiện tại")
                isArrivalDateOk = false;
            } else {
                isArrivalDateOk = true;
            }
        }
    }

    if (isArrivalDateOk) {
        (function checkLimitBedNumberInRoom(param, callback) {
            fetch(URL_COVID + "/rooms/checkLimitBedNumberInRoom?roomId=" + param)
                .then(res => res.json())
                .then(data => {
                    if (data.bedNumber === data.totalPeopleInRoom) {
                        showError(room, null, "Phòng không còn giường trống <br> <b>Số người/Số giường: " + data.bedNumber + "/" + data.totalPeopleInRoom + "</b>");
                    } else {
                        callback(valueArrivalDateTemp, quarantinedPeopleId, checkArrivalDateGreaterThanLastDeparttureDate);
                    }
                });
        })(room.options[room.selectedIndex].value, checkExistsArrivalDate);

        function checkExistsArrivalDate(param1, param2, callback) {
            fetch(URL_COVID + "/quarantined-information/checkExistsArrivalDate?newArrivalDate=" + convertStringDateToDefaultDateTypeMySQL(param1) + "&quarantinedPeopleIdToCheck=" + param2)
                .then(res => res.json())
                .then(data => {
                    if (data) {
                        showError(arrivalDateTemp, "Ngày đến khu cách ly đã tồn tại");
                    } else {
                        callback(param1, param2);
                    }
                });
        };

        function checkArrivalDateGreaterThanLastDeparttureDate(param1, param2) {
            fetch(URL_COVID + "/quarantined-information/getLastDepartureDateByQuarantinedPeopleId?quarantinedPeopleId=" + param2)
                .then(res => res.json())
                .then(data => {
                    if (data.departureDate != null) {
                        let lastDepartureDate = convertStringToDate(data.departureDate);
                        let choseArrivalDate = convertStringToDate(param1);
                        if (choseArrivalDate < lastDepartureDate) {
                            showError(arrivalDateTemp, "Ngày đến khu cách ly phải lớn hơn hoặc bằng ngày: " + data.departureDate);
                        } else {
                            Swal.fire({
                                title: 'Xác nhận',
                                html: "Thêm người cách ly vào <br><b>" + room.options[room.selectedIndex].text + "</b><br><b>" + area.options[area.selectedIndex].text + "</b>",
                                icon: 'warning',
                                showCancelButton: true,
                                cancelButtonColor: '#d33',
                                cancelButtonText: 'Hủy',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Xác nhận'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    formId.submit();
                                }
                            });
                        }
                    } else {
                        Swal.fire({
                            title: 'Xác nhận',
                            html: "Thêm người cách ly vào <br><b>" + room.options[room.selectedIndex].text + "</b><br><b>" + area.options[area.selectedIndex].text + "</b>",
                            icon: 'warning',
                            showCancelButton: true,
                            cancelButtonColor: '#d33',
                            cancelButtonText: 'Hủy',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Xác nhận'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                formId.submit();
                            }
                        });
                    }
                })
        }

    }
}