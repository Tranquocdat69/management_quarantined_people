import { elementById, valueOf, changeSubmitForm, showError } from "../../../common_dashboard_login/js/function.js";
import { closeDatePicker, convertStringDateToDefaultDateTypeMySQL, convertStringToDate, isChoseDateGreaterThanToday, isDate, openDatePicker, renderRooms } from "../common_dashboard/function.js";
import { URL_COVID } from "../common_dashboard/variable.js";

var formEditQuarantinedInformation;
var submitformEditQuarantinedInformation;
var area;
var room;
var sourceOfContact;
var arrivalDateTemp;
var statusEditRoom;
var leaveDateTemp;
var levelInfection;
var id;
var valueId;

window.onload = (function() {
    start();

    getCurrentArea(currentRoomId, area);

    changeSubmitForm(formEditQuarantinedInformation, submitformEditQuarantinedInformation, validateForm);

    openDatePicker("#arrivalDateTemp");
    openDatePicker("#leaveDateTemp");

    statusEditRoom.addEventListener("change", function() {
        if (parseInt(statusEditRoom.value) === 2) {
            leaveDateTemp.removeAttribute("disabled");
            leaveDateTemp.removeAttribute("style");
        } else {
            leaveDateTemp.setAttribute("disabled", true);
            leaveDateTemp.value = "";
            leaveDateTemp.setAttribute("style", "cursor: no-drop");
        }
    });

    checkExistsRoomHistory(valueId);

})();

function start() {
    area = elementById("area");
    room = elementById("room");

    sourceOfContact = elementById("sourceOfContact");
    arrivalDateTemp = elementById("arrivalDateTemp");

    formEditQuarantinedInformation = elementById("formEditQuarantinedInformation");
    submitformEditQuarantinedInformation = elementById("submitformEditQuarantinedInformation");

    statusEditRoom = elementById("statusEditRoom");
    leaveDateTemp = elementById("leaveDateTemp");
    levelInfection = elementById("levelInfection");
    id = elementById("id");
    valueId = valueOf(id);

    arrivalDateTemp.value = currentArrivalDate;

}

function validateForm(formId) {
    let valueSourceOfContact = valueOf(sourceOfContact);
    let valueArrivalDateTemp = valueOf(arrivalDateTemp);
    let valueLeaveDateTemp = valueOf(leaveDateTemp);
    let isArrivalDateOk = false;
    let isUpdateStatusOk = false;
    let isChangeRoomStatusDateOk = false;

    if (parseInt(statusEditRoom.value) === 1) {
        if (parseInt(room.options[room.selectedIndex].value) === parseInt(currentRoomId)) {
            showError(room, "Chọn phòng khác phòng hiện tại để cập nhật phòng");
            isUpdateStatusOk = false;
        } else {
            isUpdateStatusOk = true;
        }
    } else {
        isUpdateStatusOk = true;
    }

    if (isUpdateStatusOk) {
        if (valueSourceOfContact === "") {
            showError(sourceOfContact, "Nhập nguồn tiếp xúc mầm bệnh");
        } else if (parseInt(statusEditRoom.value) === 2) {
            if (parseInt(room.options[room.selectedIndex].value) === parseInt(currentRoomId)) {
                showError(room, "Chọn phòng khác phòng hiện tại để chuyển phòng");
                isChangeRoomStatusDateOk = false;
            } else if (valueLeaveDateTemp === "") {
                showError(leaveDateTemp, "Nhập ngày chuyển phòng");
                isChangeRoomStatusDateOk = false;
            } else {
                if (valueArrivalDateTemp === "") {
                    showError(arrivalDateTemp, "Nhập ngày vào khu cách ly");
                    isChangeRoomStatusDateOk = false;
                } else {
                    let leaveDateRoomDate = convertStringToDate(valueLeaveDateTemp);
                    let arrivalDateDate = convertStringToDate(valueArrivalDateTemp);

                    // alert(leaveDateRoomDate.toISOString().slice(0, 10).replace(/-/g, "/").toString().split("/").reverse().join("/"))
                    // alert(arrivalDateDate.toISOString().slice(0, 10).replace(/-/g, "/").toString().split("/").reverse().join("/"))

                    if (leaveDateRoomDate < arrivalDateDate) {
                        showError(leaveDateTemp, "Ngày chuyển phòng phải lớn hơn hoặc bằng ngày đến");
                        isChangeRoomStatusDateOk = false;
                    } else {
                        fetch(URL_COVID + "/quarantined-information/getNumberOfDayQuarantinedByLevelInfectionId?levelInfectionId=" + levelInfection.value)
                            .then(res => res.json())
                            .then(data => {
                                arrivalDateDate.setDate(arrivalDateDate.getDate() + data);
                                if (leaveDateRoomDate >= arrivalDateDate) {
                                    arrivalDateDate.setDate(arrivalDateDate.getDate() + 1);
                                    let stringDepartureDate = arrivalDateDate.toISOString().slice(0, 10).replace(/-/g, "/").toString().split("/").reverse().join("/");
                                    showError(leaveDateTemp, null, "Ngày chuyển phòng phải nhỏ hơn ngày hoàn thành cách ly <br> Ngày hoàn thành cách ly: " + stringDepartureDate);
                                    isChangeRoomStatusDateOk = false;
                                } else {
                                    isChangeRoomStatusDateOk = true;
                                    nextStep();
                                }
                            });
                    }
                }
            }
        } else {
            isChangeRoomStatusDateOk = true;
        }
    }

    if (isChangeRoomStatusDateOk) {
        nextStep();
    }

    function nextStep() {
        if (isChangeRoomStatusDateOk) {
            if (valueArrivalDateTemp === "") {
                showError(arrivalDateTemp, "Nhập ngày vào khu cách ly");
            } else if (valueArrivalDateTemp != "") {
                if (!isDate(valueArrivalDateTemp)) {
                    showError(arrivalDateTemp, null, "Ngày tháng năm không hợp lệ hoặc sai định dạng <br> VD: 01/01/2021")
                    isArrivalDateOk = false;
                } else {
                    if (isChoseDateGreaterThanToday(valueArrivalDateTemp)) {
                        showError(arrivalDateTemp, "Không thể chọn ngày vào khu cách ly lớn hơn ngày hiện tại")
                        isArrivalDateOk = false;
                    } else {
                        isArrivalDateOk = true;
                    }
                }
            }
        }

        if (isArrivalDateOk) {
            if (parseInt(room.options[room.selectedIndex].value) !== parseInt(currentRoomId) && parseInt(statusEditRoom.value) !== 0) {
                fetch(URL_COVID + "/rooms/checkLimitBedNumberInRoomWhenEdit?newRoomId=" + room.options[room.selectedIndex].value + "&currentRoomId=" + currentRoomId)
                    .then(res => res.json())
                    .then(data => {
                        if (data.bedNumber === data.totalPeopleInRoom) {
                            showError(null, null, "Phòng không còn giường trống <br> <b>Số người/Số giường: " + data.bedNumber + "/" + data.totalPeopleInRoom + "</b>");
                        } else {
                            checkExistsArrivalDateWhenEdit(valueArrivalDateTemp, currentArrivalDate, quarantinedPeopleId, checkArrivalDateGreaterThanLastDeparttureDate, true);
                        }
                    });
            } else {
                checkExistsArrivalDateWhenEdit(valueArrivalDateTemp, currentArrivalDate, quarantinedPeopleId, checkArrivalDateGreaterThanLastDeparttureDate, false);
            }


            function checkExistsArrivalDateWhenEdit(param1, param2, param3, callback, param4) {
                fetch(URL_COVID + "/quarantined-information/checkExistsArrivalDateWhenEdit?newArrivalDate=" + convertStringDateToDefaultDateTypeMySQL(param1) + "&currentArrivalDate=" + convertStringDateToDefaultDateTypeMySQL(param2) + "&quarantinedPeopleIdToCheck=" + param3)
                    .then(res => res.json())
                    .then(data => {
                        if (data) {
                            showError(arrivalDateTemp, "Ngày đến khu cách ly đã tồn tại");
                        } else {
                            callback(param1, param3, param4);
                        }
                    })
            }

            function checkArrivalDateGreaterThanLastDeparttureDate(param1, param2, param3) {
                fetch(URL_COVID + "/quarantined-information/getLastDepartureDateByQuarantinedPeopleId?quarantinedPeopleId=" + param2)
                    .then(res => res.json())
                    .then(data => {
                        if (data.departureDate != null && data.departureDate != "") {
                            let lastDepartureDate = convertStringToDate(data.departureDate);
                            let choseArrivalDate = convertStringToDate(param1);
                            if (choseArrivalDate < lastDepartureDate) {
                                showError(arrivalDateTemp, "Ngày đến khu cách ly phải lớn hơn hoặc bằng ngày: " + data.departureDate);
                            } else {
                                if (param3) {
                                    Swal.fire({
                                        title: 'Xác nhận',
                                        html: statusEditRoom.options[statusEditRoom.selectedIndex].text + " của người cách ly vào <br><b>" + room.options[room.selectedIndex].text + "</b><br><b>" + area.options[area.selectedIndex].text + "</b>",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        cancelButtonColor: '#d33',
                                        cancelButtonText: 'Hủy',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Xác nhận'
                                    }).then((result) => {
                                        if (result.isConfirmed) {
                                            formId.submit();
                                        }
                                    });
                                } else {
                                    formId.submit();
                                }
                            }
                        } else {
                            if (param3) {
                                Swal.fire({
                                    title: 'Xác nhận',
                                    html: statusEditRoom.options[statusEditRoom.selectedIndex].text + " của người cách ly vào <br><b>" + room.options[room.selectedIndex].text + "</b><br><b>" + area.options[area.selectedIndex].text + "</b>",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    cancelButtonColor: '#d33',
                                    cancelButtonText: 'Hủy',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Xác nhận'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        formId.submit();
                                    }
                                });
                            } else {
                                formId.submit();
                            }
                        }
                    });
            }
        }
    }
}

function getCurrentArea(param, areaSelect) {
    fetch(URL_COVID + "/areasInQuarantinedInformation/getAreaByRoomId?roomId=" + param)
        .then(res => res.json())
        .then(data => {
            areaSelect.innerHTML = `<option value="${data.area_id}">${data.area_name}</option>`;
            areaSelect.value = data.area_id;
            renderRooms(data.area_id, room, currentRoomId);
        })
}

function checkExistsRoomHistory(id) {
    fetch(URL_COVID + "/quarantined-information/checkExistsRoomHistory?id=" + id)
        .then(res => res.json())
        .then(data => {
            if (data) {
                arrivalDateTemp.readOnly = true;
                arrivalDateTemp.setAttribute("style", "cursor: no-drop");

                closeDatePicker("#arrivalDateTemp");

                levelInfection.innerHTML = "";
                levelInfection.innerHTML = "<option value=" + currentLevelInfectionId + ">" + currentLevelInfectionName + "</option>"
                levelInfection.setAttribute("style", "cursor: no-drop");

                Swal.fire({
                    title: 'Thông báo',
                    html: "Bạn sẽ không thể cập nhật được cấp độ và ngày đến <br> Do thông tin cách ly này đã tồn tại lịch sử phòng",
                    icon: 'info',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok'
                });
            } else {
                checkExistsTest(id);
            }
        })
}

function checkExistsTest(id) {
    fetch(URL_COVID + "/quarantined-information/checkExistsTest?id=" + id)
        .then(res => res.json())
        .then(data => {
            if (data) {
                arrivalDateTemp.readOnly = true;
                arrivalDateTemp.setAttribute("style", "cursor: no-drop");

                closeDatePicker("#arrivalDateTemp");

                levelInfection.innerHTML = "";
                levelInfection.innerHTML = "<option value=" + currentLevelInfectionId + ">" + currentLevelInfectionName + "</option>"
                levelInfection.setAttribute("style", "cursor: no-drop");

                Swal.fire({
                    title: 'Thông báo',
                    html: "Bạn sẽ không thể cập nhật được cấp độ và ngày đến <br> Do thông tin cách ly này đã tồn tại xét nghiệm",
                    icon: 'info',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok'
                });
            } else {
                checkExistsApplicationForm(id);
            }
        })
}

function checkExistsApplicationForm(id) {
    fetch(URL_COVID + "/quarantined-information/checkExistsApplicationForm?id=" + id)
        .then(res => res.json())
        .then(data => {
            if (data) {
                arrivalDateTemp.readOnly = true;
                arrivalDateTemp.setAttribute("style", "cursor: no-drop");

                closeDatePicker("#arrivalDateTemp");

                levelInfection.innerHTML = "";
                levelInfection.innerHTML = "<option value=" + currentLevelInfectionId + ">" + currentLevelInfectionName + "</option>"
                levelInfection.setAttribute("style", "cursor: no-drop");

                Swal.fire({
                    title: 'Thông báo',
                    html: "Bạn sẽ không thể cập nhật được cấp độ và ngày đến <br> Do thông tin cách ly này đã tồn tại đơn",
                    icon: 'info',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok'
                });
            }
        })
}