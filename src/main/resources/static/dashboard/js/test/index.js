import { URL_COVID, configPagination, configSearch, ROLE_TESTER } from "../common_dashboard/variable.js"
import { ifHasAuthority, pagination, search } from "../common_dashboard/function.js"
import { elementByClassName, elementById, showError, valueOf } from "../../../common_dashboard_login/js/function.js";

var btnCreateTest;
var tableTest;
var errorTest;

var stompClient = null;
var searchInput;
var btnPositive;

window.onload = (function() {
    startTest();

    renderUITest(URL_COVID);

    connect();


    btnPositive.addEventListener("click", function() {
        Swal.fire({
            title: 'Xác nhận',
            text: "Cập nhật kết quả dương tính cho người cách ly này?",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            cancelButtonText: 'Hủy',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Xác nhận'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(URL_COVID + "/test/getTotalPositiveResultByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
                    .then(res => res.json())
                    .then(data => {
                        if (parseInt(data) === 2) {
                            updatePositiveResultForQuarantinedInformation(quarantinedInformationId)
                        } else {
                            if (parseInt(data) === -1) {
                                showError(null, "Không thể cập nhật nhật người cách ly thành dương tính")
                            } else {
                                showError(null, "Chưa có kết quả dương tính không thể cập nhật thành dương tính");
                            }
                        }
                    });
            }
        });

    });
})();

function updatePositiveResultForQuarantinedInformation() {
    fetch(URL_COVID + "/test/updatePositiveResultForQuarantinedInformation?quarantinedInformationId=" + quarantinedInformationId)
        .then(res => res.json())
        .then(data => {
            if (data) {
                renderUITest(URL_COVID);
            } else {
                showError(null, "Không thể cập nhật nhật người cách ly thành dương tính")
            }
        });
}

function checkQuarantinedInformationNotComplete(quarantinedPeopleId) {
    fetch(URL_COVID + "/quarantined-information/checkQuarantinedInformationNotComplete?quarantinedPeopleId=" + quarantinedPeopleId)
        .then(res => res.json())
        .then(data => {
            if (data) {
                btnCreateTest.style.display = 'none';
            } else {
                btnCreateTest.style.display = 'inline-block';
                btnCreateTest.addEventListener("click", function() {
                    getTotalNegativeResultByQuarantinedInformationId(quarantinedPeopleId);
                });
            }
        });
}

function getTotalNegativeResultByQuarantinedInformationId(quarantinedPeopleId) {
    fetch(URL_COVID + "/test/getTotalNegativeResultByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
        .then(res => res.json())
        .then(data => {
            if (parseInt(data) === 2) {
                showError(null, "Đã có kết quả âm tính không thể thêm xét nghiệm");
            } else {
                getTotalPositiveResultByQuarantinedInformationId(quarantinedPeopleId);
            }
        });
}

function getTotalPositiveResultByQuarantinedInformationId(quarantinedPeopleId) {
    fetch(URL_COVID + "/test/getTotalPositiveResultByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
        .then(res => res.json())
        .then(data => {
            if (parseInt(data) === 2) {
                showError(null, "Đã có kết quả dương tính không thể thêm xét nghiệm");
            } else {
                getTotalTestHaveQualifiedSampleStatus(quarantinedPeopleId);
            }
        });
}

function getTotalTestHaveQualifiedSampleStatus(quarantinedPeopleId) {
    fetch(URL_COVID + "/test/getTotalTestHaveQualifiedSampleStatusByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
        .then(res => res.json())
        .then(data => {
            if (parseInt(data) === 3) {
                showError(null, null, "Không thể thêm được xét nghiệm <br> <b>Tối đa 3 xét nghiệm với tình trạng mẫu: Đạt</b>")
            } else {
                window.location.href = URL_COVID + "/test/create-test/" + quarantinedPeopleId;
            }
        });
}

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function() {
        stompClient.subscribe('/topic/getTest/' + quarantinedInformationId, function() {
            renderUITest(URL_COVID);
        });
    });
}

function startTest() {
    // configPagination.currentPage = 1;
    // configPagination.recordPerPage = 1;
    // configPagination.elementPagination = elementById("paginationTest");

    btnCreateTest = elementById("btnCreateTest");
    tableTest = elementById("tableTest");
    errorTest = elementById("errorTest");
    searchInput = elementById("searchInput");
    btnPositive = elementById("btnPositive");

    // searchInput.addEventListener("keyup", function() {
    //     configSearch.baseUrl = URL_COVID; // là localhost:8080
    //     configSearch.urlSearch = "/rooms/SearchRoom?keyword="; // gán đường dẫn đến controller
    //     configSearch.keyword = valueOf(searchInput); // keyword khi người dùng nhập vào

    //     configSearch.paramCallback.elementTable = tableTest; //phần tử của table để hiển thị kết quả
    //     configSearch.callbackTable = renderTable; //hàm được sử dụng để render dữ liệu ra table tìm kiếm
    //     configSearch.callbackUI = renderUI; //hàm được sử dụng để render dữ liệu ra table khi người dùng k nhập gì ở thanh tìm kiếm

    //     configSearch.elementShowError = errorTest; // phần tử dùng để hiển thị lỗi
    //     configSearch.elementPagination = configPagination.elementPagination; // phần tử hiển thị phân trang

    //     configSearch.errorMessage = "Không có dữ liệu khớp với từ khóa :(((("; // lỗi khi hiển thị với ng dùng

    //     search(configSearch);
    // });
}

function renderUITest(url) {
    if (ifHasAuthority(ROLE_TESTER)) {
        fetch(URL_COVID + "/test/getTotalPositiveResultByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
            .then(res => res.json())
            .then(data => {
                if (parseInt(data) === 2) {
                    btnPositive.style.display = 'inline-block';
                } else {
                    btnPositive.style.display = 'none';
                }
            });

        checkQuarantinedInformationNotComplete(quarantinedPeopleId);
    }

    fetch(url + "/test/getAllTestByQuarantinedInformationIdDesc?quarantinedInformationId=" + quarantinedInformationId)
        .then(response => response.json())
        .then(data => {
            if (data.length > 0) {
                // configPagination.lengthArray = data.length;

                renderTableTest(tableTest, 0, data.length, data);
                //hàm xử lí dữ liệu khi chuyển trang
                // function changePageTest(page) {
                //     renderTableTest(tableTest, ((page - 1) * configPagination.recordPerPage), (page * configPagination.recordPerPage), data)
                // }

                // ae thêm cái này để khi xóa không bị lỗi trang lung tung :V
                // if (configPagination.currentPage > Math.ceil(configPagination.lengthArray / configPagination.recordPerPage)) {
                //     configPagination.currentPage -= 1;
                // }

                // changePageTest(configPagination.currentPage);

                // configPagination.callback = changePageTest;
                //hàm phân trang
                // configPagination: biến cấu hình phân trang
                // pagination(configPagination);

                errorTest.innerHTML = "";
            } else {
                tableTest.innerHTML = "";
                // configPagination.elementPagination.innerHTML = "";
                errorTest.innerHTML = "Hiện chưa có xét nghiệm trong lần cách ly này :(((("
            }
        });
}

// elementTable: phần tử hiển thị table
// startIndex: bắt đầu vòng for
// lengthArray: độ dài của mảng cần lặp
// data: biến lưu dữ liệu cần render ra table
function renderTableTest(elementTable, startIndex, lengthArray, data) {
    let baseTable = `<thead>
                <tr>
                    <th class="border-top-0">#</th>
                    <th class="border-top-0">Loại xét nghiệm</th>
                    <th class="border-top-0">Ngày lấy mẫu</th>
                    <th class="border-top-0">Ngày xét nghiệm</th>
                    <th class="border-top-0">Tình trạng mẫu</th>
                    <th class="border-top-0">Kết quả xét nghiệm</th>
                    
                </tr>
                </thead>
                <tbody>
                </tbody>`;

    let str = " ";
    for (let i = startIndex; i < lengthArray && i < data.length; i++) {
        str += `
                    <tr>
                        <td>
                        <a href="/test/detail-test/${data[i].testId}">
                        Lần ${i+1}</td>
                        </a>
                        <td>${data[i].testType.name}</td>
                        <td>${data[i].receiveSampleDate}</td>
                        <td>${data[i].testDate}</td>
                        <td>${data[i].sampleStatus ? "Đạt" : "Không đạt"}</td>
                        <td>${data[i].testResult ? "Dương tính" : "Âm tính"}</td>
                        <td>
                            <button data-value="${data[i].testId}" class="btn btn-danger text-white btnDeleteTest">Xóa</button>
                        </td>
                    </tr>
                        `;
    };

    baseTable += str;
    elementTable.innerHTML = baseTable;

    let btnDeleteTest = elementByClassName("btnDeleteTest");

    for (let j = 0; j < btnDeleteTest.length; j++) {
        btnDeleteTest[j].addEventListener("click", function() {
            let valueId = this.getAttribute("data-value");
            Swal.fire({
                title: 'Xác nhận',
                text: "Bạn chắc chắn xóa xét nghiệm này?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Hủy',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Xóa'
            }).then((result) => {
                if (result.isConfirmed) {
                    fetch(URL_COVID + "/test/deleteTestById?id=" + valueId + "&quarantinedInformationId=" + quarantinedInformationId)
                        .then(res => res.json())
                        .then(data => {
                            if (data) {
                                renderUITest(URL_COVID);
                            } else {
                                showError(null, "Không thể xóa xét nghiệm không tồn tại", null);
                            }
                        });
                }
            })
        });

        if (!ifHasAuthority(ROLE_TESTER)) {
            btnDeleteTest[j].style.display = 'none';
        }
    }

}