import { elementById } from "../../../common_dashboard_login/js/function.js";
import { exportPDF } from "../common_dashboard/function.js";

var btnExportPDF;
var btnBack;
var content;

window.onload = (function() {
    start();

    btnExportPDF.addEventListener("click", function() {
        exportPDF("Xét nghiệm", content);
    });

    btnBack.addEventListener("click", function() {
        window.history.back();
    })
})();

function start() {
    btnExportPDF = elementById("btnExportPDF");
    btnBack = elementById("btnBack");
    content = elementById("content");
}