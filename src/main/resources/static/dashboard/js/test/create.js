import { URL_COVID } from "../common_dashboard/variable.js";
import { changeSubmitForm, elementById, showError, valueOf } from "../../../common_dashboard_login/js/function.js";
import { convertDefaultDateTypeMySQLToStringDate, convertStringDateToDefaultDateTypeMySQL, convertStringToDate, isChoseDateGreaterThanToday, isDate, openDatePicker } from "../common_dashboard/function.js";

var testType;
var diagnose;
var sample;
var sampler;
var receiveSampleDateTemp;
var sampleStatus;
var tester;
var testDateTemp;
var testResult;
var btnBack;

window.onload = (function() {
    start();

    openDatePicker("#testDateTemp");
    openDatePicker("#receiveSampleDateTemp");

    changeSubmitForm(formCreateTest, submitBtn, validateForm);

    btnBack.addEventListener("click", function() {
        window.history.back();
    });

    $('#receiveSampleDateTemp').datepicker('setDate', convertStringToDate(convertDefaultDateTypeMySQLToStringDate(arrivalDate)));
    $('#testDateTemp').datepicker('setDate', convertStringToDate(convertDefaultDateTypeMySQLToStringDate(departureDate)));
})();

function start() {
    testType = elementById("testType");
    diagnose = elementById("diagnose");
    sample = elementById("sample");
    sampler = elementById("sampler");
    receiveSampleDateTemp = elementById("receiveSampleDateTemp");
    sampleStatus = elementById("sampleStatus");
    tester = elementById("tester");
    testDateTemp = elementById("testDateTemp");
    testResult = elementById("testResult");
    btnBack = elementById("btnBack");
}

function validateForm(formId) {
    let valueTestType = testType.options[testType.selectedIndex].value;
    let valueDiagnose = valueOf(diagnose);
    let valueSample = valueOf(sample);
    let valueSampler = valueOf(sampler);
    let valueReceiveSampleDateTemp = valueOf(receiveSampleDateTemp);
    let valueSampleStatus = sampleStatus.options[sampleStatus.selectedIndex].value;
    let valueTester = valueOf(tester);
    let valueTestDateTemp = valueOf(testDateTemp);
    let valueTestResult = testResult.options[testResult.selectedIndex].value;

    if (valueTestType == null || valueTestType == "") {
        showError(testType, "Chọn loại xét nghiệm");
    } else if (valueDiagnose === "") {
        showError(diagnose, "Nhập chẩn đoán người xét nghiệm");
    } else if (valueSample === "") {
        showError(sample, "Nhập bệnh phẩm thu nhập");
    } else if (valueSample.length > 255) {
        showError(sample, "Độ dài kí tự bệnh phẩm thu nhập tối đa là 255 kí tự");
    } else if (valueSampler === "") {
        showError(sampler, "Nhập người lấy mẫu");
    } else if (valueSampler.length > 255) {
        showError(sampler, "Độ dài kí tự người lấy mẫu tối đa là 255 kí tự");
    } else if (valueReceiveSampleDateTemp === "") {
        showError(receiveSampleDateTemp, "Chọn ngày lấy mẫu");
    } else if (!isDate(valueReceiveSampleDateTemp)) {
        showError(receiveSampleDateTemp, null, "Ngày tháng năm không hợp lệ hoặc sai định dạng <br> VD: 01/01/2021")
    } else if (isChoseDateGreaterThanToday(valueReceiveSampleDateTemp)) {
        showError(receiveSampleDateTemp, "Không thể chọn ngày lấy mẫu lớn hơn ngày hiện tại")
    } else if (valueSampleStatus == null || valueSampleStatus == "") {
        showError(sampleStatus, "Chọn tình trạng mẫu");
    } else if (valueTester == "") {
        showError(tester, "Nhập người xét nghiệm");
    } else if (valueTester.length > 255) {
        showError(tester, "Độ dài kí tự người xét nghiệm tối đa là 255 kí tự");
    } else if (valueTestDateTemp === "") {
        showError(testDateTemp, "Chọn ngày xét nghiệm");
    } else if (!isDate(valueTestDateTemp)) {
        showError(testDateTemp, null, "Ngày tháng năm không hợp lệ hoặc sai định dạng <br> VD: 01/01/2021")
    } else if (isChoseDateGreaterThanToday(valueTestDateTemp)) {
        showError(testDateTemp, "Không thể chọn ngày xét nghiệm lớn hơn ngày hiện tại")
    } else if (convertStringToDate(valueTestDateTemp) < convertStringToDate(valueReceiveSampleDateTemp)) {
        showError(testDateTemp, "Không thể chọn ngày xét nghiệm nhỏ hơn ngày lấy mẫu")
    } else if (valueTestResult == null || valueTestResult == "") {
        showError(testResult, "Chọn kết quả xét nghiệm");
    } else {
        fetch(URL_COVID + "/quarantined-information/getQuarantinedInformationyById?id=" + quarantinedInformationId)
            .then(res => res.json())
            .then(data => {
                let arrivalDate = convertStringToDate(data.arrivalDate);
                let departureDate = convertStringToDate(data.departureDate);
                let testDateTempDate = convertStringToDate(valueTestDateTemp);
                let receiveSampleDateTempDate = convertStringToDate(valueReceiveSampleDateTemp);

                if (receiveSampleDateTempDate < arrivalDate) {
                    showError(receiveSampleDateTemp, null, "Ngày lấy mẫu phải lớn hơn hoặc bằng ngày đến khu cách ly <br> <b>Ngày đến khu cách ly: " + data.arrivalDate + "</b>");
                } else if (testDateTempDate >= departureDate) {
                    showError(testDateTemp, null, "Ngày xét nghiệm phải nhỏ hơn ngày hoàn thành cách ly <br> <b>Ngày hoàn thành cách ly: " + data.departureDate + "</b>");
                } else {
                    checkDateExistsInTest(convertStringDateToDefaultDateTypeMySQL(valueReceiveSampleDateTemp), convertStringDateToDefaultDateTypeMySQL(valueTestDateTemp), quarantinedInformationId, quarantinedPeopleId);
                }
            });

        function checkDateExistsInTest(receiveSampleDate, testDate, quarantinedInformationId, quarantinedPeopleId) {
            fetch(URL_COVID + "/test/checkDateExistsInTestByQuarantinedInformationId?receiveSampleDate=" + receiveSampleDate + "&testDate=" + testDate + "&quarantinedInformationId=" + quarantinedInformationId)
                .then(res => res.json())
                .then(data => {
                    if (data) {
                        showError(null, null, "Đã tồn tại ngày lấy mẫu và ngày xét nghiệm <br> Với tình trạng mẫu: Đạt")
                    } else {
                        getTotalNegativeResultByQuarantinedInformationId(quarantinedPeopleId);
                    }
                });
        }

        function getTotalNegativeResultByQuarantinedInformationId(quarantinedPeopleId) {
            fetch(URL_COVID + "/test/getTotalNegativeResultByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
                .then(res => res.json())
                .then(data => {
                    if (parseInt(data) === 2) {
                        showError(null, "Đã có kết quả âm tính không thể thêm xét nghiệm");
                    } else {
                        getTotalPositiveResultByQuarantinedInformationId(quarantinedPeopleId);
                    }
                });
        }

        function getTotalPositiveResultByQuarantinedInformationId(quarantinedPeopleId) {
            fetch(URL_COVID + "/test/getTotalPositiveResultByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
                .then(res => res.json())
                .then(data => {
                    if (parseInt(data) === 2) {
                        showError(null, "Đã có kết quả dương tính không thể thêm xét nghiệm");
                    } else {
                        getTotalTestHaveQualifiedSampleStatus(quarantinedPeopleId);
                    }
                });
        }

        function getTotalTestHaveQualifiedSampleStatus(quarantinedPeopleId) {
            fetch(URL_COVID + "/test/getTotalTestHaveQualifiedSampleStatusByQuarantinedInformationId?quarantinedPeopleId=" + quarantinedPeopleId)
                .then(res => res.json())
                .then(data => {
                    if (parseInt(data) === 3) {
                        showError(null, null, "Không thể thêm được xét nghiệm <br> <b>Tối đa 3 xét nghiệm với tình trạng mẫu: Đạt</b>")
                    } else {
                        Swal.fire({
                            title: 'Xác nhận',
                            html: "Thêm thông tin xét nghiệm vào loại xét nghiệm: <br> <b>" + testType.options[testType.selectedIndex].text + "</b>",
                            icon: 'warning',
                            showCancelButton: true,
                            cancelButtonColor: '#d33',
                            cancelButtonText: 'Hủy',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Xác nhận'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                formId.submit();
                            }
                        });
                    }
                });
        }
    }
}