import { changeSubmitForm, elementById } from "../../../common_dashboard_login/js/function.js";
import { URL_COVID, URL_IMAGE, URL_IMAGE_DEFAULT } from "../common_dashboard/variable.js";
import { setAvatar } from "../common_dashboard/function.js";

var loggedUserAvatar;
var loggedUsername;
var formLogout;
var btnLogout;

var stompClient = null;
var checkReload = false;

window.onload = (function() {
    start();

    renderUI(URL_COVID, checkReload);

    changeSubmitForm(formLogout, btnLogout, validate)

    connect();
})();

function start() {
    loggedUserAvatar = elementById("loggedUserAvatar");
    loggedUsername = elementById("loggedUsername");
    formLogout = elementById("formLogout");
    btnLogout = elementById("btnLogout");
}

function renderUI(url) {
    fetch(url + "/current-user/getCurrentLoggedUser")
        .then(res => res.json())
        .then(data => {
            loggedUsername.textContent = data.username;
            setAvatar(loggedUserAvatar, data.avatar, URL_IMAGE, URL_IMAGE_DEFAULT);
        })
}

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function() {
        stompClient.subscribe('/topic/getUser/' + loggedUsername.textContent, function() {
            renderUI(URL_COVID);
        });
        stompClient.subscribe('/topic/forceLogout/' + loggedUsername.textContent, function() {
            sessionStorage.clear();
            localStorage.clear();

            formLogout.submit();
        });
    });
}

function validate(formId) {
    Swal.fire({
        title: 'Xác nhận',
        text: "Bạn muốn đăng xuất chứ ?",
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Hủy',
        confirmButtonText: 'Đăng xuất'
    }).then((result) => {
        if (result.isConfirmed) {
            sessionStorage.clear();
            localStorage.clear();

            formId.submit();
        }
    })
}