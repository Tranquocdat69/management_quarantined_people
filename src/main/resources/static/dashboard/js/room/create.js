 import { URL_COVID } from "../common_dashboard/variable.js";
import { showError, valueOf, checkRegex, elementById, elementByName, changeSubmitForm } from "../../../common_dashboard_login/js/function.js";


var formCreateRoom;
var submitBtn;
var roomName;
var note;
var bedNumber;
var areaSelect;

window.onload=(function(){
    start();

    changeSubmitForm(formCreateRoom,submitBtn,validateForm);
})();
function start(){
    formCreateRoom = elementById("formCreateRoom");
    submitBtn = elementById("submitBtn");
    roomName= elementById("roomName");
    bedNumber = elementById("bedNumber")
    note= elementById("note");
    areaSelect = elementById("area");
}
function validateForm(formId){

    let valueRoomName = valueOf(roomName);
    let valueBedNumber = valueOf(bedNumber);
    let valueNote = valueOf(note);
    let valueAreaId = areaSelect.options[areaSelect.selectedIndex].value;

    if(valueRoomName===""){
        showError(roomName,'Nhập tên phòng');
    }else if(valueRoomName.length >100){
        showError(roomName,'Tên phòng tối đa 100 kí tự');
    }
    else if (valueBedNumber === "") {
        showError(bedNumber, 'Nhập số giường');
    } else if (valueBedNumber > 10 ||valueBedNumber < 0) {
        showError(bedNumber, 'Số giường tối đa 10 giường và lớn hơn 0');
    }else{
        fetch(URL_COVID + "/rooms/SearchExistRoom?newRoomName="+valueRoomName+"&areaId="+valueAreaId)
        .then(res => res.json())
        .then(data => {
            if (data) {
                showError(roomName,"Tên phòng đã tồn tại trong khu vực");
            }else{
                formId.submit();
            }
        })
    }
}