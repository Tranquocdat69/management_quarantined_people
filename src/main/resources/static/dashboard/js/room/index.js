import { URL_COVID, configPagination, configSearch, ROLE_ADDER } from "../common_dashboard/variable.js"
import { ifHasAuthority, pagination, search } from "../common_dashboard/function.js"
import { elementByClassName, elementById, showError, valueOf } from "../../../common_dashboard_login/js/function.js";

var elementOfTable;
var elementOfShowError;

var stompClient = null;
var searchInput;

window.onload = (function() {

    start();

    renderUI(URL_COVID);

    connect();
})();

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function() {
        stompClient.subscribe('/topic/getRoom', function() {
            renderUI(URL_COVID);
        });
    });
}

function start() {
    configPagination.currentPage = 1;
    configPagination.recordPerPage = 2;
    configPagination.elementPagination = elementById("pagination");
    searchInput = elementById("searchInput");
    elementOfTable = elementById("userRoom");
    elementOfShowError = elementById("error");

    searchInput.addEventListener("keyup", function() {
        configSearch.baseUrl = URL_COVID; // là localhost:8080
        configSearch.urlSearch = "/rooms/SearchRoom?keyword="; // gán đường dẫn đến controller
        configSearch.keyword = valueOf(searchInput); // keyword khi người dùng nhập vào

        configSearch.paramCallback.elementTable = elementOfTable; //phần tử của table để hiển thị kết quả
        configSearch.callbackTable = renderTable; //hàm được sử dụng để render dữ liệu ra table tìm kiếm
        configSearch.callbackUI = renderUI; //hàm được sử dụng để render dữ liệu ra table khi người dùng k nhập gì ở thanh tìm kiếm

        configSearch.elementShowError = elementOfShowError; // phần tử dùng để hiển thị lỗi
        configSearch.elementPagination = configPagination.elementPagination; // phần tử hiển thị phân trang

        configSearch.errorMessage = "Không có dữ liệu khớp với từ khóa :(((("; // lỗi khi hiển thị với ng dùng

        search(configSearch);
    });
}

function renderUI(url) {
    fetch(url + "/rooms/getAllRoom")
        .then(response => response.json())
        .then(data => {
            if (data.length > 0) {
                configPagination.lengthArray = data.length;

                //hàm xử lí dữ liệu khi chuyển trang
                function changePage(page) {
                    renderTable(elementOfTable, ((page - 1) * configPagination.recordPerPage), (page * configPagination.recordPerPage), data)
                }

                // ae thêm cái này để khi xóa không bị lỗi trang lung tung :V
                if (configPagination.currentPage > Math.ceil(configPagination.lengthArray / configPagination.recordPerPage)) {
                    configPagination.currentPage -= 1;
                }

                changePage(configPagination.currentPage);

                configPagination.callback = changePage;
                //hàm phân trang
                // configPagination: biến cấu hình phân trang
                pagination(configPagination);

                elementOfShowError.innerHTML = "";
            } else {
                elementOfTable.innerHTML = "";
                configPagination.elementPagination.innerHTML = "";
                elementOfShowError.innerHTML = "Hiện chưa có thêm người dùng trong hệ thống :(((("
            }
        });
}

// elementTable: phần tử hiển thị table
// startIndex: bắt đầu vòng for
// lengthArray: độ dài của mảng cần lặp
// data: biến lưu dữ liệu cần render ra table
function renderTable(elementTable, startIndex, lengthArray, data) {
    let baseTable = `<thead>
                <tr>
                    <th class="border-top-0">#</th>
                    <th class="border-top-0">Tên phòng</th>
                    <th class="border-top-0">Khu vực</th>
                    <th class="border-top-0">Số lượng người đang ở</th>
                    <th class="border-top-0">Số lượng giường</th>
                </tr>
                </thead>
                <tbody>
                </tbody>`;

    let str = " ";
    for (let i = startIndex; i < lengthArray && i < data.length; i++) {
        str += `
                    <tr>
                        <td>${i+1}</td>
                        <td>${data[i].roomName}</td>
                        
                        <td>${data[i].areaName}</td>
                        <td>${data[i].totalPeopleInRoom}</td>
                        <td>${data[i].bedNumber}</td>
                        <td>
                            <a href="/rooms/edit/${data[i].roomId}" class="btn btn-info text-white btnEdit">Sửa</a>
                            <button data-value="${data[i].roomId}" class="btn btn-danger text-white btnDelete">Xóa</button>
                        </td>
                    </tr>
                        `;

    };

    baseTable += str;
    elementTable.innerHTML = baseTable;

    let btnDelete = elementByClassName("btnDelete");
    let btnEdit = elementByClassName("btnEdit");

    for (let j = 0; j < btnDelete.length; j++) {
        btnDelete[j].addEventListener("click", function() {
            let valueId = this.getAttribute("data-value");
            Swal.fire({
                title: 'Xác nhận',
                text: "Bạn chắc chắn xóa ?",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Hủy',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Xóa'
            }).then((result) => {
                if (result.isConfirmed) {
                    fetch(URL_COVID + "/rooms/delete?room_id=" + valueId)
                        .then(res => res.json())
                        .then(data => {
                            if (data) {
                                renderUI(URL_COVID);
                            } else {
                                showError(null, "Không thể xóa khi đã/đang có người trong phòng", null);
                            }
                        });
                }
            })
        });
        if (!ifHasAuthority(ROLE_ADDER)) {
            btnDelete[j].style.display = 'none';
            btnEdit[j].style.display = 'none';
        }
    }
}