import { URL_COVID } from "../common_dashboard/variable.js";
import { showError, valueOf, checkRegex, elementById, elementByName, changeSubmitForm } from "../../../common_dashboard_login/js/function.js";


var formEditRoom;
var submitBtn;
var roomName;
var bedNumber;
var areaAddress;
var note;
var currentEditRoomName;
var areaId;
var roomId;
var currentAreaId;
window.onload=(function(){
    start();

    changeSubmitForm(formEditRoom,submitBtn,validateForm);
})();

function start(){
    formEditRoom = elementById("formEditRoom");
    submitBtn = elementById("submitBtn");
    roomName= elementById("roomName");
    bedNumber = elementById("bedNumber")
    areaAddress= elementById("areaAddress");
    note = elementById("note");
    roomId = elementById("roomId");

    currentAreaId = areaAddress.options[areaAddress.selectedIndex].value;
    currentEditRoomName= valueOf(roomName);

    areaId = elementById("areaAddress");
}


function validateForm(formId){
    let valueRoomId = valueOf(roomId);
    let valueRoomName = valueOf(roomName);
    let valueBedNumber = valueOf(bedNumber);
    let valueAreaAddress = valueOf(areaAddress);
    let valueAreaId = areaId.options[areaId.selectedIndex].value;

    if(valueRoomName===""){
        showError(roomName,'Nhập tên phòng');
    }else if(valueRoomName.length >100){
        showError(roomName,'Tên phòng tối đa 100 kí tự');
    }else if (valueBedNumber === "") {
        showError(bedNumber, 'Nhập số giường');
    } else if (valueBedNumber > 10 || valueBedNumber<=0  ) {
        showError(bedNumber, 'Số giường tối đa 10 giường và phải lớn hơn 0');
    }else{
        fetch(URL_COVID + "/rooms/checkLimitBedNumberInRoom?roomId=" + valueRoomId)
        .then(res => res.json())
        .then(data => {
            if (data.totalPeopleInRoom > valueBedNumber) {
                // alert('That Bai');
                showError(null, null, "Không thể cập nhật số giường  <br> <b>Số người đang trong phòng: "+ data.totalPeopleInRoom+ "</b> ");
            } else {
                if (valueAreaId != currentAreaId) {
                    fetch(URL_COVID + "/rooms/searchEditExistRoomDiffArea?areaId="+valueAreaId+ "&newRoomName="+valueRoomName)
                    .then(res => res.json())
                    .then(data => {
                        if (data) {
                            showError(roomName,"Tên phòng đã tồn tại trong khu vực");
                        }else{
                            formId.submit();
                        }
                    });
                }else{
                    fetch(URL_COVID + "/rooms/SearchEditExistRoom?currentEditRoomName="+currentEditRoomName+ "&newEditRoomName="+valueRoomName +"&areaId="+valueAreaId)
                    .then(res => res.json())
                    .then(data => {
                        if (data) {
                            showError(roomName,"Tên phòng đã tồn tại trong khu vực");
                        }else{
                            formId.submit();
                        }
                    })
                }
            }
        });
        
        
    }
    
}