import { URL_STATS_COVID_VIET_NAM, URL_COVID } from "./common_dashboard/variable.js";
import { elementById } from "../../common_dashboard_login/js/function.js";
import { convertDefaultDateTypeMySQLToStringDate, numberWithCommas } from "./common_dashboard/function.js";

var population;
var confirmed;
var deaths;
var ctx;
var dataCovidVn;
var confirmedObjectToArray;
var deathObjectToArray;
var numberDays;
var currentNumberDay;
var chart;
var quarantining;
var quarantined;
var positive;

window.onload = (async function() {

    loadDataApplication(URL_COVID, 0);
    loadDataApplication(URL_COVID, 1);
    loadDataApplication(URL_COVID, 2);

    beforeAllFunctions(URL_STATS_COVID_VIET_NAM);

    // setInterval(() => {
    //     // if (localStorage.getItem('data_covid_vn')) {
    //     //     localStorage.removeItem("data_covid_vn");
    //     // }
    //     // beforeAllFunctions(URL_STATS_COVID_VIET_NAM);
    // }, 3000);
})();

function beforeAllFunctions(url) {
    fetch(url)
        .then(res => res.json())
        .then(data => {
            start(data);

            renderUI(URL_COVID);

            currentNumberDay.innerHTML = numberDays.options[numberDays.selectedIndex].value;

            numberDays.addEventListener("change", function() {
                currentNumberDay.innerHTML = numberDays.options[numberDays.selectedIndex].value;
                renderChart(numberDays.options[numberDays.selectedIndex].value);
            });
            // localStorage.setItem("data_covid_vn", JSON.stringify(data));
        });
}

function start(dataCovidVn) {

    ctx = elementById('myChart').getContext('2d');
    population = elementById("population");
    confirmed = elementById("confirmed");
    deaths = elementById("deaths");
    numberDays = elementById("numberDays");
    currentNumberDay = elementById("currentNumberDay");

    // dataCovidVn = JSON.parse(localStorage.getItem('data_covid_vn'));
    confirmedObjectToArray = Object.keys(dataCovidVn.location.timelines.confirmed.timeline).map((key) => [key, dataCovidVn.location.timelines.confirmed.timeline[key]]);
    deathObjectToArray = Object.keys(dataCovidVn.location.timelines.deaths.timeline).map((key) => [key, dataCovidVn.location.timelines.deaths.timeline[key]]);

    population.innerHTML = numberWithCommas(dataCovidVn.location.country_population) + " người";
    confirmed.innerHTML = numberWithCommas(dataCovidVn.location.latest.confirmed) + " ca";
    deaths.innerHTML = numberWithCommas(dataCovidVn.location.latest.deaths) + " ca";
}

function renderUI(url) {
    renderChart(numberDays.options[numberDays.selectedIndex].value);
}

function renderChart(numberDays) {
    if (chart) {
        chart.destroy();
    }

    let newArrayLabel = [];

    let newArrayDataConfirmed = [];
    let newArrayBackgroundColorConfirm = [];

    let newArrayDataDeaths = [];
    let newArrayBackgroundColorDeaths = [];

    for (let i = confirmedObjectToArray.length - numberDays; i < confirmedObjectToArray.length; i++) {
        newArrayLabel.push(convertDefaultDateTypeMySQLToStringDate(confirmedObjectToArray[i][0].substr(0, 10)));

        newArrayDataConfirmed.push(confirmedObjectToArray[i - 1] ? confirmedObjectToArray[i][1] - confirmedObjectToArray[i - 1][1] : "");
        newArrayBackgroundColorConfirm.push('rgb(252, 113, 20)');

        newArrayDataDeaths.push(deathObjectToArray[i - 1] ? deathObjectToArray[i][1] - deathObjectToArray[i - 1][1] : "");
        newArrayBackgroundColorDeaths.push('rgb(247, 40, 40)');
    }

    chart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: newArrayLabel,
            datasets: [{
                    label: 'Dương tính',
                    data: newArrayDataConfirmed,
                    backgroundColor: newArrayBackgroundColorConfirm,
                    borderWidth: 1
                },
                {
                    label: 'Tử vong',
                    data: newArrayDataDeaths,
                    backgroundColor: newArrayBackgroundColorDeaths,
                    borderWidth: 1
                }
            ]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
}

function loadDataApplication(url, status) {
    quarantining = elementById("quarantining");
    quarantined = elementById("quarantined");
    positive = elementById("positive");

    fetch(url + "/quarantined-information/getDataApplication?status=" + status)
    .then(res => res.json())
    .then(data => {
        if (data) {
            if (status == 0) {
                quarantining.innerHTML = data;
            } else if (status == 1) {
                quarantined.innerHTML = data;
            } else {
                positive.innerHTML = data;
            }
        } else {
            if (status == 0) {
                quarantining.innerHTML = "0";
            } else if (status == 1) {
                quarantined.innerHTML = "0";
            } else {
                positive.innerHTML = "0";
            }
        }
    });
}