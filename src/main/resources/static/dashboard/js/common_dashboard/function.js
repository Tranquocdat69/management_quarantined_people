    import { elementById, elementByClassName } from "../../../common_dashboard_login/js/function.js";
    import { REGEX_DATE, TRANSLATE_DATE, URL_COVID } from "./variable.js";

    export function previewImageBeforeUpload(chooseImage, previewImage, defaultImage) {
        chooseImage.addEventListener("click", function() {
            previewImage.src = defaultImage;
        })
        chooseImage.addEventListener("change", function() {
            const [file] = chooseImage.files
            if (file) {
                previewImage.src = URL.createObjectURL(file)
            }
        });
    }

    export function setAvatar(imageToShow, nameImage, url, defaultUrl) {
        if (nameImage === "" || nameImage === null) {
            imageToShow.setAttribute("src", defaultUrl);
        } else {
            imageToShow.setAttribute("src", url + nameImage);
        }
    }

    export function pagination(config) {

        let strLi = `<li class="page-item disabled" id="li_prev">
        <a class="page-link" href="#" id="btn_prev">Trước</a>
        </li>`;
        for (let i = 0; i < numPages(); i++) {
            if (i + 1 === 1) {
                strLi += `<li class="page-item active" id="li${i+1}">
                <a class="page-link current-page" href="#" data-value="${i+1}">${i+1}</a>
                </li>`;
                continue;
            }
            strLi += `<li class="page-item" id="li${i+1}">
            <a class="page-link current-page" href="#" data-value="${i+1}">${i+1}</a>
            </li>`;

        }
        strLi += `<li class="page-item" id="li_next">
        <a class="page-link" href="#" id="btn_next">Tiếp</a>
        </li>`;

        config.elementPagination.innerHTML = strLi;

        for (let j = 0; j < numPages(); j++) {
            elementByClassName("current-page")[j].addEventListener("click", function() {
                config.currentPage = this.getAttribute("data-value");
                elementById("li_next").classList.remove("disabled");
                elementById("li_prev").classList.remove("disabled");
                if (config.currentPage == numPages()) {
                    elementById("li_next").classList.add("disabled");
                }
                if (config.currentPage == 1) {
                    elementById("li_prev").classList.add("disabled");
                }

                elementByClassName("page-item active")[0].classList.remove("active");
                elementById("li" + config.currentPage).classList.add("active");

                config.callback(config.currentPage);

            });
        }

        let btn_next = elementById("btn_next");
        let btn_prev = elementById("btn_prev");

        elementById("li_next").classList.remove("disabled");
        elementById("li_prev").classList.remove("disabled");
        if (config.currentPage == numPages()) {
            elementById("li_next").classList.add("disabled");
        }
        if (config.currentPage == 1) {
            elementById("li_prev").classList.add("disabled");
        }

        elementByClassName("page-item active")[0].classList.remove("active");
        elementById("li" + config.currentPage).classList.add("active");

        btn_next.addEventListener("click", function() {
            if (config.currentPage < numPages()) {
                config.currentPage++;
                elementByClassName("page-item active")[0].classList.remove("active");
                elementById("li" + config.currentPage).classList.add("active");
                elementById("li_prev").classList.remove("disabled");
                config.callback(config.currentPage);
            }
            if (config.currentPage === numPages()) {
                elementById("li_next").classList.add("disabled");
            }
        });

        btn_prev.addEventListener("click", function() {
            if (config.currentPage > 1) {
                config.currentPage--;
                elementByClassName("page-item active")[0].classList.remove("active");
                elementById("li" + config.currentPage).classList.add("active");
                elementById("li_next").classList.remove("disabled");

                config.callback(config.currentPage);
            }
            if (config.currentPage === 1) {
                elementById("li_prev").classList.add("disabled");
            }
        })

        function numPages() {
            return Math.ceil(config.lengthArray / config.recordPerPage);
        }
    }

    export function search(configSearch) {
        if (configSearch.keyword !== "") {
            fetch(configSearch.baseUrl + configSearch.urlSearch + configSearch.keyword)
                .then(res => res.json())
                .then(data => {
                    if (data.length > 0) {

                        configSearch.callbackTable(configSearch.paramCallback.elementTable, 0, data.length, data);

                        configSearch.elementShowError.innerHTML = "";

                        if (configSearch.elementPagination != null) {
                            configSearch.elementPagination.setAttribute("style", "display:none");
                        }
                    } else {
                        configSearch.paramCallback.elementTable.innerHTML = "";
                        configSearch.elementShowError.innerHTML = configSearch.errorMessage;

                        if (configSearch.elementPagination != null) {
                            configSearch.elementPagination.setAttribute("style", "display:none");
                        }
                    }
                });
        } else {
            configSearch.elementShowError.innerHTML = "";

            if (configSearch.elementPagination != null) {
                configSearch.elementPagination.removeAttribute("style");
            }
            configSearch.callbackUI(configSearch.baseUrl);
        }
    }

    export function isDate(str) {
        if (str.match(REGEX_DATE)) {
            var parms = str.split(/[\.\-\/]/);
            var yyyy = parseInt(parms[2], 10);
            var mm = parseInt(parms[1], 10);
            var dd = parseInt(parms[0], 10);
            var date = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
            return mm === (date.getMonth() + 1) && dd === date.getDate() && yyyy === date.getFullYear();
        }

        return false;
    }

    export function openDatePicker(id) {
        TRANSLATE_DATE(jQuery);
        $(id).datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            endDate: '+1d',
            todayHighlight: true,
            language: 'vi'
        });
    }

    export function closeDatePicker(id) {
        $(id).datepicker('remove');
    }

    export function renderProvinces(url, provinceSelect) {
        fetch(url)
            .then(response => response.json())
            .then(
                data => {
                    let str = `<option selected>Tỉnh/Thành phố</option>`;
                    for (let i = 0; i < data.length; i++) {
                        str += `<option data-code-province="${data[i].code}" value="${data[i].name}">${data[i].name}</option>`;
                    }
                    provinceSelect.innerHTML = str;
                }
            );
    }

    export function renderDistricts(url, provinceSelect, districtSelect, wardSelect) {
        wardSelect.innerHTML = " ";
        wardSelect.innerHTML = "<option selected>Phường/Xã</option>";

        let selectedProvince = provinceSelect.options[provinceSelect.selectedIndex].getAttribute("data-code-province");
        fetch(url)
            .then(response => response.json())
            .then(
                data => {
                    let str = `<option selected>Quận/Huyện</option>`;
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].province_code == selectedProvince) {
                            str += `<option data-code-district="${data[i].code}" value="${data[i].name}">${data[i].name}</option>`;
                        }
                    }
                    districtSelect.innerHTML = str;

                }
            );
    };

    export function renderWards(url, districtSelect, wardSelect) {
        let selectedDistrict = districtSelect.options[districtSelect.selectedIndex].getAttribute("data-code-district");
        fetch(url)
            .then(response => response.json())
            .then(
                data => {
                    let str = `<option selected>Phường/Xã</option>`;
                    for (let i = 0; i < data.length; i++) {
                        if (data[i].district_code == selectedDistrict) {
                            str += `<option data-ward="${data[i].code}" value="${data[i].name}">${data[i].name}</option>`;
                        }
                    }
                    wardSelect.innerHTML = str;
                }
            );

    }

    export function renderRooms(areaId, roomSelect, currentRoomId = null) {
        fetch(URL_COVID + "/rooms/getRoomsByAreaId?areaId=" + areaId)
            .then(res => res.json())
            .then(data => {
                if (data.length > 0) {
                    let str = "";
                    for (let i = 0; i < data.length; i++) {
                        if (parseInt(data[i].room_id) === currentRoomId) {
                            str += `<option selected value="${data[i].room_id}">${data[i].room_name}</option>`;
                        } else {
                            str += `<option value="${data[i].room_id}">${data[i].room_name}</option>`;
                        }
                    }
                    roomSelect.innerHTML = str;
                } else {
                    roomSelect.innerHTML = "";
                }
            });
    }

    export function convertStringDateToDefaultDateTypeMySQL(s) {
        return s.split("/").reverse().join("-");
    };

    export function convertDefaultDateTypeMySQLToStringDate(s) {
        return s.split("-").reverse().join("/");
    };

    export function isChoseDateGreaterThanToday(dateString) {
        let choseDate = convertStringToDate(dateString);
        var today = new Date();

        if (choseDate > today) {
            return true;
        }
        return false;
    }

    export function convertStringToDate(dateString) {
        let dateParts = dateString.split("/");
        let convertedDate = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);

        return convertedDate;
    }

    export function ifHasAuthority(roleToBeAccessed) {
        return roles.some(function(val) {
            return roleToBeAccessed.includes(val);
        });
    }

    export function exportPDF(filenameWhenExport, elementToExport) {
        const opt = {
            filename: filenameWhenExport,
        };

        html2pdf().set(opt).from(elementToExport).save();
    }

    export function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }