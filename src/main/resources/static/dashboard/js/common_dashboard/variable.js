export const URL_COVID = "http://localhost:8080"
export const URL_GET_PROVINCE = "https://provinces.open-api.vn/api/p/";
export const URL_GET_DISTRICT = "https://provinces.open-api.vn/api/d/";
export const URL_GET_WARD = "https://provinces.open-api.vn/api/w/";
export const URL_STATS_COVID_VIET_NAM = "https://coronavirus-tracker-api.herokuapp.com/v2/locations/274";

export const URL_IMAGE_DEFAULT = "/dashboard/plugins/images/noimage.jpg";
export const URL_IMAGE = "/dashboard/plugins/images/uploads/";

export const REGEX_DATE = /^\d{2}[/]\d{2}[/]\d{4}$/;

// biến lưu thông tin cấu hình phân trang, ae cóp π nhé đừng sửa biến bên trong là được 
export var configPagination = {
    currentPage: 0,
    recordPerPage: 0,
    lengthArray: 0,
    elementPagination: null,
    callback: function() {}
};

// biến lưu thông tin cấu hình tìm kiếm, ae cóp π nhé đừng sửa biến bên trong là được
export var configSearch = {
    baseUrl: "",
    urlSearch: "",
    keyword: "",
    callbackTable: function() {},
    callbackUI: function() {},
    paramCallback: {
        elementTable: null
    },
    elementShowError: null,
    elementPagination: null,
    errorMessage: ""
};


export const TRANSLATE_DATE = function($) {
    $.fn.datepicker.dates['vi'] = {
        days: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
        daysShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
        daysMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        months: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        monthsShort: ["Th 1", "Th 2", "Th 3", "Th 4", "Th 5", "Th 6", "Th 7", "Th 8", "Th 9", "Th 10", "Th 11", "Th 12"],
        today: "Hôm nay",
        suffix: [],
        meridiem: []
    };
};

export const ROLE_ADMIN = ["ADMIN"];

export const ROLE_ADDER = ["ADMIN", "ADDER"];

export const ROLE_TESTER = ["ADMIN", "TESTER"];

export const ROLE_HANDLER = ["ADMIN", "HANDLER"];