-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: db_covid
-- ------------------------------------------------------
-- Server version	8.0.26
--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ADMIN'),(2,'ADDER'),(3,'HANDLER'),(4,'TESTER');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@gmail.com','$2a$10$knnXpQsscDJlSjkG1RPYKOWfiHe1rNENV.74ukQrZ75NLRKZo6WOy','Administrator','1900819899','',0,NULL,'2021-09-11','2021-09-27');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `users_roles`
--

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` VALUES (1,1);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `level_infection`
--

LOCK TABLES `level_infection` WRITE;
/*!40000 ALTER TABLE `level_infection` DISABLE KEYS */;
INSERT INTO `level_infection` VALUES (1,'F1',21),(2,'F2',14),(3,'F3',7),(4,'Từ vùng dịch',14);
/*!40000 ALTER TABLE `level_infection` ENABLE KEYS */;
UNLOCK TABLES;

